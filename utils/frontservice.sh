#!/bin/bash

cat > eestats.${1}.frontend.service <<EOF
[Unit]
Description=Evaluation Engine Statistics Frontend $1
After=docker-latest.service
Requires=docker-latest.service

[Service]
TimeoutStartSec=0
Restart=always
ExecStartPre=-/usr/bin/docker stop frontend_${1}
ExecStartPre=-/usr/bin/docker rm frontend_${1}
ExecStart=/usr/bin/docker run --rm --name frontend_${1} -p ${2}:80 -e EE_ENVIRONMENT=${1} eestats/frontend:${1}
ExecStop=/usr/bin/docker kill frontend_${1}

[Install]
WantedBy=multi-user.target
EOF
