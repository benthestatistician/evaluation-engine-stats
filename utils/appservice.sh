#!/bin/bash

NAME=app_${1}_${2}
cat > eestats.$NAME.service <<EOF
[Unit]
Description=Evaluation Engine Statistics App: $NAME
After=eestats.${1}.frontend.service
Requires=eestats.${1}.frontend.service

[Service]
TimeoutStartSec=0
Restart=always
ExecStartPre=-/usr/bin/docker stop $NAME
ExecStartPre=-/usr/bin/docker rm $NAME
ExecStart=/usr/bin/docker run --rm --name $NAME --link frontend_${1}:frontend \\
       -e EE_ENVIRONMENT=${1} \\
       -e EE_DB_USER=\${EE_DB_USER} \\
       -e EE_DB_PASS=\${EE_DB_PASS} \\
       -v /srv/stats/${1}/:/srv/stats/ \\
       eestats/app:${1}
ExecStop=/usr/bin/docker kill $NAME

[Install]
WantedBy=multi-user.target
EOF
