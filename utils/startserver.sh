#!/bin/bash
n=$1

docker stop frontend_$EE_ENVIRONMENT
sleep 1
docker run -d --rm --name frontend_$EE_ENVIRONMENT \
       -p $EE_PORT:80 \
       -e EE_ENVIRONMENT=$EE_ENVIRONMENT \
       eestats/frontend:$EE_TAG

for ((i = 1; i <= $n; i++))
do
    docker stop app_"$EE_ENVIRONMENT"_$i
    sleep 1
    docker run -d -t --rm --name app_"$EE_ENVIRONMENT"_$i --link frontend_$EE_ENVIRONMENT:frontend \
       -e EE_ENVIRONMENT=$EE_ENVIRONMENT \
       -e EE_DB_USER=$EE_DB_USER \
       -e EE_DB_PASS=$EE_DB_PASS \
       eestats/app:$EE_TAG
done

