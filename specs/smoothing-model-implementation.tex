\documentclass[11pt]{article}
\input{ee-planning-preamble}
\newcounter{saveenum}
%\author{BH, AM, \ldots}
\date{\today} 
\title{Smoothing of scores to be used as pretests:\\ version based on empirical Bayes + EM}
\begin{document}
\maketitle

\section{Concept summary}

Whenever suitable pretest data are available, the Evaluation Engine uses them to adjust outcome comparisons. Two common limitations of pretest score data apply here also:
\begin{enumerate}
\item some subjects for whom outcome and treatment information is available will lack pretest scores (\textit{missing data}); and
\item pretest scores are, at best, sums of students' true prior ability and a random disturbance (\textit{measurement error}). \setcounter{saveenum}{\value{enumi}}\end{enumerate}

In the propensity matching literature, neither of these are usually treated as important limitations. However, both issues are ordinarily felt call for special accommodations in analyses comparing intervention and control groups that use regression methods\footnote{E.g., Trochim (2006), ``Nonequivalent groups analysis,'' \textit{Research Methods Knowledge Base}, \url{http://www.socialresearchmethods.net/kb/statnegd.php}.}. Such methods are more broadly known than propensity score methods, and besides we are likely to use them in a subsidiary role in the Evaluation Engine, to address imbalances that may remain after propensity score matching and to reduce standard errors. Some steps to address missing data in the pretests and measurement error in the pretests may help bolster the credibility of the overall analysis.

This note outlines some measures that help address these concerns. In particular, the creation of smoothed scores. The smoothed scores are generated off-line and in advance, and the models that give place to them are relatively concrete and easy to understand. The process of creating and fitting the smoothing models produces, as an additional by-product of use to the Engine, ``\textit{school effects}'' characterizing schools' effectiveness relative to other schools in the same state and/or relative to other schools in the same state serving a similar mixture of students, which can be used to inform the matching (and that would not otherwise be available). These effects are also described herein. 
% To be added: usefulness with the vertically linked test scores to be added, if any. 
% The growth rates initially envisioned are not being produced given the nature of the models being used. 

%\textbf{Assumptions.} The remainder of the note assumes, with rare exceptions, missingness occurs \textit{only} for assessment measures, not demographic or other background measures. 
%(This based on Bob F's summary of administrative data from ``unnamed state,'' from 10 Jan 2013 meeting.)

\section{Structure of the smoothing model}
\label{sec:structure-smoothing-model}

Let $y_{ij}$ denote the test score of interest for student $i$ in school $j$. We model the basic outcome $y_{ij}$ as a function of individual and school-level covariates. That is, we specify a mixed model, and specifically a two-level hierarchical linear model, with students (at level~1) nested within schools (at level~2). We write the student-level model as
\begin{equation}
  {y}_{ij} =
    {\beta}_{0j}
    +
    \sum_{p=1}^{P}
    {\beta}_{pj} 
    \left(
      x_{pij} - \bar{x}_{p \cdot{} j}
    \right)
    +
    {e}_{ij}
    , \quad {e}_{ij} \sim N ( 0, \sigma^2),
\label{eq:level-1-basic-smoothing-model} 
\end{equation}

\noindent and the school-level model as
\begin{equation}
  {\beta}_{0j} =
    {\gamma}_{0}
    + 
    \sum_{p=1}^{P} {\gamma}_{p} \bar{x}_{p \cdot{} j}
    +
    {u}_{j}
    , \quad {u}_{j} \sim N ( 0, \tau).
\label{eq:level-2-basic-smoothing-model} 
\end{equation}

\noindent Replacing \eqref{eq:level-2-basic-smoothing-model} into \eqref{eq:level-1-basic-smoothing-model} leads to the mixed model 
\begin{equation}
  {y}_{ij} =
    {\gamma}_{0}
    + 
    \sum_{p=1}^{P} {\gamma}_{p} \bar{x}_{p \cdot{} j}
    +
    \sum_{p=1}^{P}
    {\beta}_{pj} 
    \left(
      x_{pij} - \bar{x}_{p \cdot{} j}
    \right)
    +
    {u}_{j}
    +
    {e}_{ij}
    .
\label{eq:basic-smoothing-model} 
\end{equation}

\noindent Here, 
$x_{pij}$ denotes the value of the $p^{th}$ covariate for student $i$ of classroom $j$,
$\bar{x}_{p \cdot{} j}$ denotes the mean of $x_{pij}$ for classroom $j$,
${\gamma}_{0}$ denotes the overall intercept, 
${\gamma}_{p}$ denotes the effect of the $p^{th}$ school-level covariate, 
${\beta}_{pj}$ denotes the effect of the $p^{th}$ individual-level covariate, 
${e}_{ij}$ is a student-specific disturbance, and
${u}_{j}$ is a school-specific disturbance.
In Stata lingo, \eqref{eq:basic-smoothing-model} has a single random effect (the school-level random effect, ${u}_{j}$), and a residual (the student-level random effect, ${e}_{ij}$).

The following is worth noting about \eqref{eq:basic-smoothing-model}:
\begin{itemize}
  \item all the student-level covariates are centered around their school mean (i.e., $x_{pij} - \bar{x}_{p \cdot{} j}$), 
  \item for each individual-level covariate the school mean is also included as a covariate,
  \item school-level covariates arise only from aggregating student-level information,
  and
  \item there are no random slopes (i.e, random intercepts only).
\end{itemize}
These modeling choices are due partly to keep the model relatively straightforward but also because of certain data characteristics. If data availability changes (e.g., additional covariates become available), or additional states are considered, it might be worth reconsidering some of these choices. 

\section{Covariates}

The following variables are being used to obtain the covariates included in \eqref{eq:basic-smoothing-model}:
\begin{itemize}
  \item gender (originally dummy-coded female $1$ and male $0$), 
  \item race-ethnicity (dummy-coded indicators for Asian, Black, Hispanic and National, with White as the reference level), 
  \item limited English proficiency (dummy-coded indicator), 
  and
  \item special education (dummy-coded indicator).
\end{itemize}

\noindent As of this writing, covariates are the same for each agency we've worked with. Covariates are included as described in \S~\ref{sec:structure-smoothing-model}. 

\section{Data management}

The process of generating the smoothed scores entails the following broad steps:
\begin{enumerate}
  \item Pre-smoothing data processing,  
  \item Model fitting, 
  \item Generation of smoothed scores, and
  \item Post smoothing data processing. 
\end{enumerate}

\noindent Breaking the process into these steps facilitates preparing and running the code which as of the writing of this note is in Stata. Each step is briefly described below, but please refer to the actual code for more details. 

\subsection{Pre-smoothing data processing}

This step takes the raw data and outputs a temporary ``regression-ready'' data set. This data set is temporary in the sense that it is an intermediate product of the entire process (though technically it has not been coded as a temporary data set). The actions completed in this step include reading the raw data, generating unique school identifiers based on the State-provided district and school identifiers, sub-setting the cases and variables to be used, preparing the covariates for the regression models, and reshaping the data set to long format. 

Data are reshaped to ease model fitting. In the raw data set, each case represents a student and is uniquely identified by variable \textit{studyid} (or variable \textit{hashedid}). In the reshaped-long data set, each case represents a student-by-year combination and thus cases are no longer uniquely identified by \textit{studyid}, but rather by \textit{studyid} and \textit{year}. The entire process may have been achieved without reshaping the data, but the reshaping herein described significantly simplifies the subsequent steps.

\subsection{Model fitting}

This step takes the ``regression-ready'' data set and repeatedly fits  \eqref{eq:basic-smoothing-model} to specific subsets of this data set. 
Specifically, a model fit is obtained for each subset of cases that share subject, grade and year. In other words, a model fit is obtained for each cross-sectional data subset specific to a grade and subject. 
For example, with a regression-ready'' data set containing 
3 years of data (2010, 2011 2012), 
for 6 grades (3, 4, 5, 6, 7, 8), 
and for 2 subjects (\textit{glmath} and \textit{readng}), 
a total of $3 \times 6 \times 2 = 36$ models are fit. 
Fitting all these models can be time-consuming, particularly for larger data sets. 

The estimates of each model are stored in the Stata working session and can be optionally stored as separate files . 
For each model fit, the student and school-level residuals are saved. At this point though, only the school-level residuals are being carried over. 
Moreover, for each model fit, student and school-level residual plots can be generated and saved if desired. 

\subsection{Generation of smoothed scores}

Smoothed scores are generated after all the models are fit. Similar to model fitting, each set of smoothed scores is specific to a year, grade and subject. In fact, each set of smoothed scores uses a single model fit. However, students of a given year, grade and subject may have \textit{multiple} sets of smoothed scores. In fact, smoothed scores are specific to a grade, a subject, a base year and a lag (in years). 

Specifically, for a given subject, the smoothed scores for students in grade G in year Y are generated using the model fit for grade G-lag in year Y-lag, where the lag can be 0, 1 or 2. The model used is then a ``lagged'' model (e.g., from the previous grade in the previous year) and the smoothed scores may be seen as out-of-sample predictions. In summary, all students in grade G in year Y should end up with ``lagged'' scores, as long as there is a model fit to be used. Also, students in a given grade and year may have multiple ``lagged'' scores, so cohorts may use multiple model fits and model fits may be used multiple times.\footnote{In Stata, the command \textit{predict, fitted} requires the original data used in estimation be in memory. This is the main reason data are handled in a reshaped-long format.}

Two types (versions) of smoothed scores are generated, but only 1 is carried over. The first type uses the school-level random effect, if available. The second type uses only the fixed effects so a smoothed score is obtained even for cases missing a school random effect. The smoothed score that is being carried forward is a combination of these two, with the second type being used only when the first type is missing.

Lagged smoothed scores for year Y when the last year of grade classification available is year Y-1 are treated separately. Since the grade classification in year Y is not (yet) available, the grade classification of year Y-1 is used instead. The lagged-1-year scores are then based on the model fit for grade G-1 in year Y-1. That is, in this case, the year of grade classification and the year of model fit are the same. One downside of this proxy is that students in grade G in year Y who were not in grade G-1 in year Y-1 will not have a smoothed score (i.e., a missing value).

\subsection{Post-smoothing data processing}

This step takes the long-form data set containing the smoothed scores, truncates any out-of-range smoothed scores, calculates the school means, 
%generates the context variables, 
and creates the student-level and school-level output files. 
%The context variables are the school-level random effects for a given year and grade. 
The creation of the student-level output files involves collapsing the data back to the original (wide) format. 
The creation of the school-level output files involves collapsing the data across schools and years. 

\section{Output files and variables}

The focus in this section is on the Stata data files that are generated after running the entire code and on the variables contained therein. The details related to the naming and management of the .do files are included at the top of the file \textit{smoothing-master.do}. That file also contains all the preliminary settings, some known limitations of the code, and some other general comments related to code execution. 

Two Stata data (.dta) files are generated after running the code, a student-level file and a school-level file. 
The student-level file is named \textit{st}\textit{year}\_smhstu\_\textit{date} and the school-level file is named \textit{st}\textit{year}\_smhsch\_\textit{date}. 
The prefix \textit{st} denotes the 2-letter State acronym, followed by the year, and the suffix \textit{date} denotes the date when the file was generated (a date stamp). The \textit{st} after the 2-letter State acronym is used to relate these data sets to the raw data file being used. 

Cases in the student-level file represent students and are uniquely identified by the variable \textit{studyid}. Cases in the school-level file represent schools and are uniquely identified by the variable \textit{schid}. This variable \textit{schid} is created by concatenating the State-provided district and school identifiers, which are included in the raw data set. A \textit{schid} value (i.e. a concatenated school ID) is obtained only if both the State-provided district and school identifiers are available (not missing) and not coded as missing. Those identifiers are provided as year-specific variables in the raw data and are included as such in the final school-level file. However the variable \textit{schid} is not year specific. Rather, \textit{schid} represents any valid concatenation of the State-provided district and school identifiers across years. 

Variables in both files may be provided in `unstacked' and `stacked' versions. A stacked variable contains all the valid values of the unstacked grade-specific variable for a given subject, variable type, lag, and year.
Unstacked variables follow the naming convention\\
\centerline{\textit{subject}\_\textit{vartype}\_\textit{gl}\#\_\textit{lag}\_\textit{year}drv}
and stacked variables following the naming convention\\
\centerline{\textit{subject}\_\textit{vartype}\_\textit{lag}\_\textit{year}drv}
where 
\begin{itemize}
  \item[] \textit{subject} represents the subject (e.g. \textit{glmath} or \textit{readng});  
  \item[] \textit{vartype} represents the variable type, with \textit{smhstu} standing for student-level smoothed score, \textit{smhsch} standing for school-level smoothed score, and \textit{schref} standing for school effect;
  \item[] \textit{gl}\# represents the grade level; 
  \item[] \textit{lag} represents the lag in years, with \textit{p0} standing for no lag (i.e., current year), \textit{m1} standing for a 1-year lag, and \textit{m2} standing for a 2-year lag; and
  \item[] \textit{year} represents the year. 
\end{itemize}
The drv suffix denotes these are derived variables. 
Note stacked variables are specific to a year and thus for each student, to a grade level. Therefore, for a given subject, variable type, lag, and year, the stacked variables contain all (and only) the values of the unstacked variable across all grades. The school-level file only contains variables of the type \textit{smhsch}. 

\end{document}
