\documentclass[11pt]{article}
\input{./ee-planning-preamble}
\bibliographystyle{asa}

\newcounter{saveenum}
\author{BH, \ldots}
\date{\currenttime, \today} 

\begin{document}

\section*{Flexible relative density based calipers}

\subsection*{Inputs and outputs}

\subsubsection*{Inputs to the routine}

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  encoding of treatment/control distinction
\item
  an inclusive propensity score (PS)
\item
  caliper value for inclusive PS
\item
  a ``main'' PS
\item
  optional ``across factor''
\item
  optional ``within factor''
\end{itemize}

\textbf{Notes}. The inclusive PS is expressed on the logit scale, and
comes with an accompanying caliper constraint, called the global PS
caliper. When we think of the inclusive and main PSes as defining a
rectangle in two dimensions, the inclusive PS will sit on the x axis
while the main PS inhabits the y axis.

Participant/potential control pairs that are separated on the inclusive
PS by less than the global PS caliper are called \emph{match-eligible}.

The main PS comes without a specific caliper, our task being to
determine a caliper on the main PS for each member of the treatment
group. For treatment subjects with an abundance of matches, this caliper
should be wide enough to leave the treatment with \texttt{max.controls}
or more matches, but not a lot wider. For treatment subjects with
relatively few matches, it may be $\infty$. Whether the main PS should
ideally be expressed on the logit or probability scale isn't yet clear
to me.

An ``\textbf{across factor}'' is a variable defining categories within
which later matches will be discouraged or forbidden. When users specify
that intra-district matching is forbidden, the function is given the
district variable as an across factor. On the other hand a
\textbf{within factor} is a variable defining categories within which
matching is required to occur. When users specify that intra-district
matching is required, the function is given the district variable as a
within factor.

Call the function implementing this spec \texttt{relativeDensity2D}. It
handles factors simply by splitting the propensity scores and the
treatment and control groups along the within factor, then calling a
routine \texttt{relativeDensity2D\_internal} separately on each of the
resulting subproblems. (The same inclusive PS caliper is used in each
call.) Accordingly, what follows describes
\texttt{relativeDensity2D\_internal}, which takes each of the inputs
just described except for within factors.

\subsection*{Return value of the routine}

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  For each participant $i$, a caliper $c_i$ on $y$ (the main PS),
  possibly $\infty$.
\item
  For each participant $i$, a lowball estimate of number of matches
  under the simplified matching model with calipers and additional
  constraints (see below) 
\end{itemize}

\subsection*{A simplified matching model}

The simplified model assumes that matches are made in the following way.
Each member of the control group that's not too far from the participant
group is matched to a member of the treatment group, with ``not too far
from the treatment group'' being operationalized in terms of an estimate
of its density. Specifically, define bandwidths $s_x$, $s_y$ by \[
s_x \equiv s_y \equiv .5 (\mbox{global inclusive caliper}) ,
\] continuing in the assumption that main as well as inclusive PSes are
to be expressed on the logit scale.\footnote{If we were later to revisit
  the decision to express the main PS on the logit rather than
  probability scale, then we might as a caliper use 1/8 of a global PS
  caliper, half of that caliper times 1/4, the derivative of the inverse
  logit transform at $\theta = 0$.} let the $K$ participants' inclusive
and main PSes be $(x_1, y_1), \ldots, (x_K, y_K)$, and let $(x, y)$ be
corresponding PS values for a potential control. Writing $\phi(\cdot)$
for the standard Normal density, the potential control is matched to
some participant if and only if
\begin{equation}
  \label{simpmatch-participates} \sum_{k=1}^K \phi((x_k-x)/s_x) \phi( (y_k - y)/s_y)  \geq \phi(2s_x/s_x) * \phi(2s_y/s_y),
\end{equation}
or $\phi(2)^2 \approx .003$\footnote{This value aims to make it the case
  that controls without counterparts within caliper distance are left
  out, but borderline cases get matched. I'm expecting the short tails
  of the Normal distribution to compensate for the fact that the
  decision as to whether a control finds a match is made in terms of a
  sum rather than a maximum of his distances from the treatment group.
  Expressing the criterion in terms of this sum, a scaled density
  estimate, should permit some computational economy, as well as
  numerically stabilizing probabilities in \eqref{simpmatch-prob}}.

The selection of a participant match is probabilistic, with greater
probability attaching to matches to the closest participants. If it is
matched, the probability of its being matched to participant $i$ is
\begin{equation}
    \label{simpmatch-prob} \frac{\phi((x_i-x)/s_x) \phi( (y_i - y)/s_y)}{\sum_{k=1}^K \phi((x_k-x)/s_x) \phi( (y_k - y)/s_y) } . 
\end{equation}

The attraction of the simplified approach is the ease of calculating
these probabilities, not the matches it makes. In contrast to the
matches we'd like to make, it leaves more treatment subjects unmatched
than is necessary, while associating others with many more controls than
is helpful. By the same token, however, expected value calculations
based on it will help us to identify difficult to match subjects as well
as subjects with many more matches than we have use for.

To incorporate constraints expressed by across factors into the
simplified approach, the sums in \eqref{simpmatch-participates},  \eqref{simpmatch-prob}, and similar expressions are
restricted to range over subsets of the participant group defined in
terms of the across factors. For example, suppose there is one across
factor, \texttt{school} say. To determine whether control $j$ with
coordinates $(x,y) = (x_j, y_j)$ is matched, \eqref{simpmatch-participates} is calculated by
summing only over treatment subjects $1, \ldots K$ associated with a
school \emph{other than} $j$'s school. Then probabilities of matching to
any one of these participants are calculated using \eqref{simpmatch-prob}, with indices $i$
and $k$ both understood to range only over treatments from different
levels of the across factor than the control $j$.

Separately for each level $a$ of the across factor $A$, we can apply our
2D density estimation routine to the two propensity scores of the $K_a$
participants with $A$-values other than $a$ to generate estimates of the
function \[
t_a: (x, y) \mapsto \frac{1}{K_a s_x s_y}\sum_{k: A_k \neq a} \phi((x_k-x)/s_x) \phi( (y_k - y)/s_y) .\]

The we estimate the probability of participant $i$ being matched to
control $j$, with $A_j = a$, by \[
w^{(i)}_j = \left\{ 
\begin{array}{cl} 
\frac{\phi((x_i-x_j)/s_x) \phi( (y_i - y_j)/s_y)}{K_a s_x s_y \hat{t}_a(x_j, y_j)},& \hat{t}_a(x_j, y_j) \geq \phi(2)^2 \,\& A_i \neq a\\
0, &  \mathrm{otherwise}.\\
\end{array} 
\right.
\]

\textbf{Notes regarding computational implementation.}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Computation of the functions $\{ \hat{t}_a : a \}$, is performed on
  values describing the ensemble of treatment group members. The
  computations to be described in what follows don't need to look at the
  treatment group as a group, except through $\{ \hat{t}_a : a \}$.
\item
  If there's an across factor involved, then later calculations aimed at
  determining the density of treatments at a point won't be able to use
  the functions $\{ \hat{t}_a : a \}$ but will instead need a separate
  determination of overall treatment density (without regard to across
  factors). In consequence, the functions $\{ \hat{t}_a : a \}$ only
  ever need to be evaluated at control points $(x_j, y_j)$.
\item
  Furthermore, we only ever need evaluations of the form
  $\hat{t}_{A_j}(x_j, y_j)$; no values of the form
  $\hat{t}_{a'}(x_j, y_j)$, $a' \neq A_j$, are used. So we might as well
  store only the vector $\left(\hat{t}_{A_j}(x_j, y_j): Z_j = 0\right)$,
  not also the function estimates $\{ \hat{t}_a : a \}$.
\end{enumerate}

\subsection*{Participant-specific caliper calculations}

Now we characterize the distribution of each participants' matching
distances under the simplified model, as an intermediate step to
selecting participant-specific caliper values on the main PS.

First, evaluate $\hat{t}_{A_j}(x_j,y_j)$ for each control $j$. The next
steps loop over each member of the treatment group, culminating in
treatment-specific caliper widths and expected values, per the
simplified model, of the number of controls to be matched to that
treatment. For a given participant $i$,

\newcommand{\currcal}{\ensuremath{\tilde{c}^{(i)}}}
\newcommand{\currnchat}{\ensuremath{\tilde{\hat{n}}_c}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\itemsep1pt\parskip0pt\parsep0pt
\item
  For each control $j=1, \ldots, J$, calculate $w^{(i)}_j$ (as above)
  and $\delta^{(i)}_j = |y_i - y_j|$.
\item
  Write $\Omega_i = \sum_{j: A_j\neq A_i} w^{(i)}_j$. Letting
  $\Delta^{(i)}$ be a random pick from $(\delta^{(i)}_j : A_j\neq A_i) $
  with probabilities proportional to $(w^{(i)}_j : j) $, calculate
  $\mathbf{E}\Delta^{(i)} = \Omega_i^{-1}\sum_{j:A_j\neq A_i} w^{(i)}_j \delta^{(i)}_j$.
  (In the event that $\Omega_i \leq$ \texttt{.Machine\$double.eps}, set
  $\Delta^{(i)} \equiv 0$.)
\item
  Set $\currcal$, the working value of the caliper for $i$, to
  $\mathbf{E}\Delta^{(i)}$. Also calculate
  $\currnchat = \sum \{w^{(i)}_j: A_j\neq A_i, \delta^{(i)}_j \leq \currcal\}$,
  the expected number of controls matched to $i$ (under the simplified
  matching model) at main PS distances of $\currcal$ or less.
\item
  If $\currnchat < 1$, proceed as follows. If also $\Omega_i \leq 1$,
  return \texttt{Inf} as participant $i$'s main PS caliper.
  ($\Omega_i \leq 1$ indicates that $i$ will be difficult to match even
  without calipers. Therefore we impose no main PS caliper on matches to
  $i$; in addition, we'll invoke a separate routine to determine whethe
  its inclusive PS caliper can be widened. {[}Eventually: as of this
  writing, no such routine exists.{]})
\item
  If $\currnchat < 1$ while
  $\Omega_i = \sum_{j:A_j\neq A_i} w^{(i)}_j >1$, we take this as an
  indication that in $i$'s case $\currcal$ would be too small a main PS
  caliper, but that a caliper larger than it would leave sufficient
  matching possibilities, ie raise $\currnchat$ to 1 or more. To find
  such a caliper without an expensive sort of
  $(\delta^{(i)}_j : Z_j=0, A_j\neq A_i) $, we use an approximation to
  the distribution of those separations based on moments of $\Delta_i$.
  Specifically, we reason from Chebyshev's inequality to find a
  coefficient $d$ for which it can be guaranteed that
  $\mathrm{Pr} (\Delta_i \leq \mathrm{E}(\Delta_i) + d \mathrm{Var}^{1/2}(\Delta_i)) > 1/\Omega_i = 1 - (\Omega_i - 1)/\Omega_i$\footnote{The
    quantity $(\Omega_i - 1)/\Omega_i$ represents the chance that
    $\Delta_i$ falls at or above its $(1/\Omega_i)*100$\%-th percentile.
    The goal is to update $\currcal$ to a value at or above that
    percentile, thus bringing
    $\currnchat = \sum \{w^{(i)}_j: A_j\neq A_i, \delta^{(i)}_j \leq \currcal\}$
    to 1 or more. The inequality serves to furnish an upper bound for
    this quantile of the $\Delta_i$ distribution.}; calculate
  $\mathrm{Var}(\Delta_i)$; and then update $\currcal$ to
  $\mathrm{E}(\Delta) + d \mathrm{Var}^{1/2}(\Delta)$. The Chebyshev
  calculation\footnote{Of course
    $\mathrm{Pr} (\Delta < \mathrm{E}(\Delta) + d \mathrm{Var}^{1/2}(\Delta)) > \mathrm{Pr} (|\Delta - \mathrm{E}(\Delta)| < d \mathrm{Var}^{1/2}(\Delta)) $.
    By Chebyshev's inequality
    $\mathrm{Pr} (|\Delta - \mathrm{E}(\Delta)| < c \mathrm{Var}^{1/2}(\Delta)) > 1 - d^{-2}.$
    Solving $1-d^{-2} = 1/\Omega$ yields $d = \sqrt{1 + 1/(\Omega -1)}$.}
  leads to a $d$ coefficient of $\sqrt{1 + 1/(\Omega_i-1)}$. Once this
  update to $\currcal$ has propagated to $\currnchat$, then necessarily
  $\currnchat >1$, and we are put into one of the two situations
  described immediately below.
\item
  If $1 \leq \currnchat \leq $ \texttt{max.controls}, then simply use
  $\currcal$ as a caliper in $y$ (the main PS) for participant $i$, with
  the corresponding
  $\currnchat = \sum \{w^{(i)}_j: A_j\neq A_i, \delta^{(i)}_j \leq \currcal\}$.
\item
  If \texttt{max.controls} ${} <  \currnchat $, then we have a treatment
  who'd get more matches than we need under the simplified matching
  strategy, even after a $y$-caliper of $\mathrm{E} \Delta^{(i)}$ has
  been imposed. We need to select a $y$-caliper
  $c < \mathrm{E}(\Delta) $ that ``trims the fat'' while still making it
  the case, hopefully, that
  $\sum \{ w^{(i)}_j: \delta^{(i)}_j \leq c,\, A_i \neq A_j \} \geq $
  \texttt{max.controls}.
\end{enumerate}

\subsubsection*{Fat-trimming}

In case $\sum \{ w^{(i)}_j: \delta^{(i)}_j \leq \currcal\} > $
\texttt{max.controls}, first determine
$c^{(i)}_{\mathrm{l}} = \min \{ j :\delta^{(i)}_j \leq c,\, A_i \neq A_j \}$
and $\sum \{ w^{(i)}_j: \delta^{(i)}_j \leq c^{(i)}_{\mathrm{l}} \}$. If
this sum meets or exceeds \texttt{max.controls}, use
$c^{(i)}_{\mathrm{l}}$ as a caliper for participant $i$. If not, then we
proceed with additional calculations aimed at determining caliper $c$
between $c^{(i)}_{\mathrm{l}}$ and
$c^{(i)}_{\mathrm{u}} \equiv \sum \{ w^{(i)}_j: \delta^{(i)}_j \leq \currcal\}$.

The additional calculation is of the sum

\begin{eqnarray*}
\hat{d}_i &\equiv& \frac{1}{ s_x (\currcal/2)} \sum_{ j: A_j \neq A_i, \delta^{(i)}_j \leq c^{(i)}_{\mathrm{u}} }
w^{(i)}_j \phi\left(\frac{x_i-x_j}{s_x}\right) \phi\left( \frac{y_i - y_j}{\currcal/2}\right) \\
&=& 
\frac{1}{ s_x (\currcal/2)} \sum_{ j: A_j \neq A_i, \delta^{(i)}_j \leq c^{(i)}_{\mathrm{u}} }
w^{(i)}_j \phi\left(\frac{x_i-x_j}{s_x}\right) \phi\left( \frac{\delta^{(i)}_j}{\currcal/2}\right) , 
\end{eqnarray*}

interpreted as an estimate of the density of controls at the point
$(x_i, y_i)$, after downweighting each control in proportion to the
likelihood that it would be matched to $i$ under the simplified matching
model. If this weighted density were constant in the vecinity of
$(x_i, y_i)$ and equal to $d$, then a $y$-caliper $c$ just wide enough
to include \texttt{max.controls} controls, after downweighting according
to $w^{(i)}_j$, within a rectangle centered at $(x_i, y_i)$ and with
half-widths $2s_x$ and $c$, satisfies $(4s_x) * (2c) * d =$
\texttt{max.controls}. So set $c^{(i)}_{\mathrm{m}} =  $
\texttt{max.controls} ${}/(8s_x \hat{d})$, and return \[
c^{(i)} := \max( c^{(i)}_{\mathrm{l}}, \min(c^{(i)}_{\mathrm{m}}, c^{(i)}_{\mathrm{u}}) ) .
\]

\subsubsection*{Last calc}

In each case, once a caliper $c_i$ has been selected, calculate and
return the sum
$\sum \{ w^{(i)}_j: \delta^{(i)}_j \leq c_i,\, A_i \neq A_j \}$,
interpreted as an estimate of the number of controls that $i$ would be
matched to under a simplified matching routine with restrictions and
calipers.
\end{document}
