\documentclass[11pt]{article}
\input{./ee-planning-preamble}
\bibliographystyle{asa}

\newcounter{saveenum}
\author{BH, \ldots}
\date{\currenttime, \today} 
\title{Variance approximations for matched differences estimators}
\newcommand{\EE}{\mathbf{E}}
\newcommand{\PP}{\mathbf{P}}
\newcommand{\var}{V}
\newcommand{\cov}{C}
\newcommand{\combdiff}[1]%
{\ensuremath{\Delta_{{z}}[#1]}}
\newcommand{\Combdiff}[1]%
{\ensuremath{\Delta_{{Z}}[#1]}}
\newcommand{\matchdiff}[1]%
{\ensuremath{\Delta[#1]}}

\title{The Evaluation Engine's tests for presence of intervention effects:\\ covariate adjustment and omitted variable sensitivity}
\author{Ben Hansen}

\begin{document}
\maketitle

The Evaluation Engine tests for the presence of intervention effects using asymptotic approximations to the permutation tests.  These permutation tests will often be covariance-adjusted, utilizing ordinary means of covariance adjustment but in a manner adapted to their combination with permutation tests \citep{rosenbaum:2002a}, and they will always be accompanied with omitted variable sensitivity analyses, using a novel adaptation to permutation tests of methods relating to those of \citet{rosenbaum:1986a}, \citet{imbens2003sea} and particularly \citet{hosmanetal2010}.  In the background, of course, is a painstaking propensity score match, the primary adjustment for confounding variables; the covariance adjustments discussed here may be seen as back-up confounder controls, and as stabilizers that promote efficiency and perhaps reduce omitted variable sensitivity \citep{rosenbaum:heterogeneity:2005}.

Advantages of the EE's specific way of knitting together techniques include its usage of distribution-free techniques and the relative ease of assessing inferences' robustness to covariate omission.  Covariance adjustments are made using robust (in the ordinary sense of e.g. \citet{huber1981robust}) near-relatives of common regression techniques, the robustness offering insurance of fair performance even when distributional expectations fail and their near relation to ordinary regression methods enabling existing information about variance components in educational data to contribute to sensitivity analysis.  Nonrandomized studies analyzed using the EE are, like all observational studies, vulnerable to selection bias as well as to chance variability. The EE takes particular care to mitigate this vulnerability, with its propensity and other adjustments then it takes the additional step of offering uncommonly accessible uncertainty assessments, assessments that put errors due to chance and errors due to variable omission on equal footing.


\section{Analytic model}


The propensity score match pairs specific treatment subjects
with specific controls, often pairing an intervention group member
with multiple controls and sometimes doing the reverse. Say $i \sim j$ if  $i$, an intervention group member, was paired to
$j$, a control, and make the relation reflexive and transitive, so that also $j \sim i$ and $i \sim k$ if there is $j$ with $i \sim j$ and $j \sim k$.  
We write $z_{i}=1$ if $i$ falls in the intervention group, 0 otherwise.
One way to estimate the intervention effect on an outcome $Y$ is to take means of matched differences:
\begin{equation}
\matchdiff{Y} =\sum_{i \in \mathcal{I}} m_{i}\Bigl[ Y_{i} -  \frac{
  \sum\{ Y_{j}: i \sim j, z_{j}=0\} }{\# \{j : i \sim j, z_{j}=0 \}} \Bigr] / \sum_{i: z_{i}=1} m_{i}, \label{eq:matchdiffCEest}
\end{equation} 
where $\mathcal{I} = \{i: z_{i}=1 \& i \sim j,\, \mathrm{some}\, j\, \mathtt{s.t.} z_{j}=0 \}$ and $(m_{i} : i \in\mathcal{I})$ is some set of
match-dependent weights, with different weighting schemes corresponding to different specifications of ``treatment effect.'' Current plans call for effect estimation using $m_{i} \equiv 1$,
``effect of treatment on the treated'' weighting\footnote{More specifically, effect of treatment on the \emph{matched} treatment group weighting, it being possible that some members of the intervention group have been left out of the match.}, with a separate test of the hypothesis of no effect using harmonic weights, to be explained in Section~\ref{sec:perm-test}. Display \eqref{eq:matchdiffCEest} presumes that each member of the intervention group is matched to at least one control
The match will have
been constructed with the aim of eliminating, or at least making
small, the means $\matchdiff{v}$ of paired differences in measured
pre-intervention variables $v$, in this way adjusting for those
variables.

The secondary adjustment uses regression and perhaps other methods to generate a transform $R=r(Y,\mathbf{x})$ of $Y$ embodying covariate adjustment and perhaps other stabilization properties, then using $\matchdiff{R}$ as a test statistic reflecting on the hypothesis of no effect. The transform $R$ used for this purpose will differ in detail, but in any given analysis will attempt to adjust the comparison of $Y$s for a pretest variable $X$ generated without reference to $Y$-values of subjects in the matched sample.  The covariance adjustment model avoids directly accounting for intervention effects, that being handled in separate steps. This fact distinguishes the method from that of \citet{hosmanetal2010}, and enables simplifications of that approach.   For matched analysis we will take $z$, the treatment assignment indicator, to instantiate a random vector $Z$ that, for each $i \in \mathcal{I}$ with $z_i=1$,  might have equally well have assigned $i$ to control and a matched comparison subject, $j$ with $i \sim j$ and $z_j=0$, to treatment.

\section{Permutation test}
\label{sec:perm-test}

Under the null hypothesis, $Y \equiv y_{c}$, and we can think of the potential outcomes as a fixed vector $y$ that does not depend on $Z$.  First, $y$ is robustly regressed on $\mathbf{x}$ \sout{and on matched-set fixed effects $f$} and an intercept, using the \texttt{MASS} package's \texttt{rlm} function and, as data, the entirety of the state-grade cohort.  (Note that this regression does not draw distinctions between intervention and control subjects, and does not involve weighting.)   Write $r$ for the vector that results from starting with the residuals of this regression and then: (i) \emph{centering around matched-set specific means}\footnote{A fast and memory-wise scalable way to do this is to use the SparseM package's \texttt{slm()} function to linearly regress the residual from the earlier $y \sim eta $ regression on matched set fixed effects, ie on the \texttt{optmatch} object giving the working match.}; followed by \emph{``metric Winsorization,'' ie symmetric top- and bottom-coding.}\footnote{\emph{cf e.g. \href{http://www.stats.ox.ac.uk/pub/StatMeth/Robust.pdf}{Ripley 2004}, p.3, which can be had from, if \texttt{e} is the vector to be metrically Winsorized and \texttt{rlm0} is the value of a call to \texttt{rlm()} with argument \texttt{psi} set at its default value of \texttt{psi.huber}}, \texttt{e* rlm0\$psi(e)}.}
The test is based on comparing $Z'r$, the total of intervention-group residuals, to its distribution under permutations of treatment assignment, $Z$, within matched sets. Or, equivalently, on setting $m_{i}$ in \eqref{eq:matchdiffCEest} to 
\begin{equation} \label{eq:8}
m_i = \left\{ \begin{array}{cc}
\frac{2}{1 + 1/(\# \{j: i \sim j,\, z_{j}=0 \})},& \# \{j: i \sim j,\, z_{j}=0 \} > 1;\\
1, & \# \{j: i \sim j,\, z_{j}=0 \} = 1;\\
\frac{2}{1 + \#\{i': i \sim i',\, z_{i'} = 1  \}}, & \#\{i': i \sim i',\, z_{i'} =1 \} > 1
\end{array}\right.
\end{equation}
before comparing $\combdiff{r}$ to the permutation distribution of $\Combdiff{r}$.\footnote{Because $\combdiff{r}$ and $\var\Combdiff{r}$ are computed by \texttt{RItools:::xBalance()}, the statistical calculations needed to produce approximate permutational $p$-values are essentially just those described in this and the previous footnotes.  The rest of this section describes conceptual underpinnings, until the end of it when sensitivity computations are described.} ( The three conditions in \eqref{eq:8} correspond to multiple controls matches, pair matches and matches in which treatments share a control, respectively, the three mutually exclusive possibilities in full matching.  We understand $m_{i}$ in~\eqref{eq:matchdiffCEest} this way%
\footnote{These weights $m_i$ correspond to to harmonic rather than ETT weights \citep{kalton1968,hansen:bowers:2008}. (In harmonic weighting, matched sets $s$ contribute in proportion to the harmonic means $h(n_{st}, n_{sc}) = [(n_{st}^{-1} + n_{sc}^{-1})/2]^{-1}$ of the numbers of treatment and control subjects they contain. )}
for the remainder of Section~\ref{sec:perm-test}.)  
%As a third equivalence (VERIFY ME!), it is the Rao score test of the hypothesis $\tau =0$ in the linear model $R= \tau Z + \upsilon \mathbf{x} + f\zeta + \epsilon$.


Say that a match deconfounds treatment assignment, $Z$, from a variable or variables $V$ if the following properties obtain.  Let a matching arrangement ``$\sim$'' be given, and for each unit $i=1,\ldots, N$ let $[i]$ be the set of indices to which $i$ or one of $i$'s matches is matched, ie $i$'s equivalence class under ``$\sim$.'' Let $z$ be the realized treatment assignment, and let $\mathcal{S}$ be the collection of equivalence classes $s$ recording at least one match of a treatment to a control, ie $z_{i}=0$ and $z_{j}=1$, some $i,j \in s$. Write 
$\mathcal{Z} = \{z^{*} \in \{0, 1\}^{N} : \sum_{j \in [i]} z_{j}^{*} = \sum_{j \in [i]} z_{j},\, \mathrm{all}\, i\}$
Then the match deconfounds $Z$ from $V$ if, conditional on the event $\{ Z \in \mathcal{Z} \}$ and on the random variable $V$, $Z$ is uniform on $ \mathcal{Z}$.  

For sensitivity analysis, relax the assumption that the match deconfounds $Z$ from potential outcomes and instead suppose an unobserved variable $U$ with the property that, for transformations $R= r(Y_{t},Y_c, \mathbf{X})$ of the potential outcome and covariates (but not the treatment variable), the residual of a certain regression of $R$ on $U$, $R^{\perp U}$, is deconfounded from $Z$ by matching.  A related relaxation of assumptions would be to suppose that $R$ and $Z$ are rendered conditionally independent by conditioning on $U$ and $\{Z \in \mathcal{Z}\}$, as opposed to conditioning only on $\{Z \in \mathcal{Z}\}$; the assumption to which we have relaxed unconfoundedness is stronger than this one, but it's difficult to see how the additional structure could introduces errors on par with errors due to variable omissions.  For concreteness, suppose the regression of $R$ on $U$ is an ordinary least squares regression, as opposed to a robust regression, done with a fixed effect for each matched set and with $i$'s quadratic weight set to $1/[i]$, making each matched sets's members' weights sum to 1.  (This specific arrangement will permit us to approximate $\var(\combdiff{r^{\perp u}})$ using $\var(\combdiff{r}) $; see discussion surrounding \eqref{eq:7}, below.)

We now condition on the values of $R$, $U$ and $\mathbf{X}$, regarding them as fixed while only $Z$ is random, as is conventional in the permutation-based literature.  Then the regression coefficients $\alpha$ that determine $r^{\perp u}$ via $r^{\perp u}_{i} = r_{i} - (\alpha_{[i]} + \alpha_{u}u)$ are also constants, not varying across resamples of $Z$, and one has 
\begin{eqnarray}
 \matchdiff{r} - \matchdiff{r^{\perp u}}  &=& \alpha_{u}\matchdiff{u} \label{eq:1} \\
&=& \frac{\sigma_{ru}}{\sigma_{u}^{2}}\matchdiff{u} \nonumber \\
&=& \sigma_{r}\rho_{ru} \matchdiff{u/\sigma_{u}}. \label{eq:6}
\end{eqnarray}
Here $\sigma_{ab}$ is the partial covariance of $a$ and $b$ given $\mathbf{x}$ and $f$, $\sigma_{a}^{2} = \sigma_{aa}$ is the residual variance of $a$ after regression on $\mathbf{x}$ and $f$, and $\rho_{ab} = \sigma_{ab}\sigma_{a}^{-1}\sigma_{b}^{-1}$ is the partial correlation of $a$ and $b$ (with conditioning on $\mathbf{x}$ and $f$), each computed with weight for subject $i$ reciprocally proportional to $\# [i] $. In expectation, \eqref{eq:6} equals the bias in the statistic $\combdiff{r}$, but it's a bit better than the bias because it gives the difference between the statistic we have, $\matchdiff{r}$, and the statistic $\matchdiff{r^{\perp u}}$ we would have liked to have, for any realization of $Z$.

The hypothesis of strictly no effect asserts that $Y_{t} \equiv Y_{c}$, from which it would follow that a transform $r$ of $Y$ and $\mathbf{X}$ takes the same values whatever the value of $Z$, and is of the form given above.  Standard permutation tests compare $\matchdiff{r}$ to their matched permutation distributions, ie the distributions of $\combdiff{r}$ for $Z$ selected uniformly at random from $\mathcal{Z}$; often these permutation distributions are approximated to first order by a Normal distribution, with approximate $p$-values based on the first-order moments $\EE\combdiff{r}$ and $\var \combdiff{r}$ with respect to the permutation distribution.  These are readily calculated from values of $r$: $\EE\combdiff{r} =0$; 
\begin{equation}
\label{eq:7}
\var (\combdiff{r}) = \var\left(\frac{Z'r}{ \sum_{s \in \mathcal{S}} (1 - \frac{1}{\# s}) } \right) =  \frac{\sum_{s \in \mathcal{S}} \sigma^{2}[\{r_i : i \in s \}]}%
{\left(\sum_{s \in \mathcal{S}} (1 - \frac{1}{\# s})\right)^{2}}  = \frac{(\# \mathcal{S})\sigma^{2}_{r}}{\left(\sum_{s \in \mathcal{S}} (1 - \frac{1}{\# s})\right)^{2}},
\end{equation}
%where $\bar{r}_{s}$ and  $\sigma^{2}_{s}$ denote $(\# s)^{-1} \sum_{j \in s} r_{j}$ and $(\# 
%s)^{-1}\sum_{i \in s} (r_{i} - \bar{r}_{s})^{2}$, respectively.
where $\sigma_{r}^{2}$ is defined as in~\eqref{eq:6}.  
By choice of the weighting scheme used in regressing $R$ on $f$ and $U$, that regression effectively sought to minimize $\sum_{s}\sigma_{s}[(r_{i} - \alpha_{[i]} - \alpha_{u}u_{i} : i \in s)] \propto \var \combdiff{(r_{i} - \alpha_{[i]} - \alpha_{u}u_{i} : i) }$; thus  $\var(\combdiff{r^{\perp u}}) \leq \var(\combdiff{r})$.  So, our sensitivity analysis need only address the differences in first moments of $\combdiff{r}$ and $\combdiff{r^{\perp u}}$.  

The permutation test is used to advise on statistical significance within the note accompanying each boxplot.  As a working assumption, we'll take the tests to be \sout{one-sided, against the alternative that treatment is beneficial} \emph{two-sided}.  When the difference is found to be significant at level .05, we include an indication of what level of hidden bias would upset that result.  Specifically, we first \sout{calculate $\sigma_{r}$ (as \texttt{sd()} applied to $r$, then applying correction of multiplication by $[(n-1)/n]^{1/2}$)} \emph{use \eqref{eq:7} to recover $\sigma^2_r = n^{-1} \sum_{s \in \mathcal{S}} \sigma^{2}[\{r_i : i \in s \}]$, where $n = (\# \mathcal{S})$, from $\Combdiff{r}$'s null standard error as returned by \texttt{xBalance()} and the calculation given by}
\begin{verbatim}
> attr(optmatch:::stratumStructure(<...>), 
+      'comparable.num.matched.pairs')
\end{verbatim}
\emph{of the effective sample size of the match, which is equal to $2 \sum_{s\in \mathcal{S}} (1 - \frac{1}{\# s}) $;} then we set $\rho_{ru}$ to the reference value $0.1$.  Next we solve 
$$
\arg\min_{\delta} \left\{ \vert \combdiff{r} \vert - \sigma_{r} \rho_{ru} \delta \leq z_{1-\alpha/2}\var^{1/2}(\Combdiff{r}) \right\} , 
$$
with $\alpha = .05$ and so $z_{1-\alpha/2} = 1.96$, to find the critical value $\delta_{u}$ of $\vert \matchdiff{u/\sigma_{u}} \vert$, which can be seen to be $\delta_{u} = [\vert \combdiff{r} \vert - z_{1-\alpha/2}\var^{1/2}(\Combdiff{r}) ]/(\sigma_{r} \rho_{ru})$.  A second, lower critical value $\delta_l$ is calculated from these formulas after setting $\rho_{ru}$ to the reference value $0.75$.  (The rationale for using these $\rho_{ru}$ values is indicated in Section~\ref{sec:extcalibration} below.) These critical values are to be conveyed in text in the note accompanying the barchart and also indicated on the Love plot.

Love plot annotation: For each outcome comparison that has been presented and found significant, there is to be an indication on the covariate balance plot of the corresponding critical value $\delta_{*}$.  Critical values falling inside the range of standardized covariation differences, both the matched differences and the differences depicting the situation before matching, are to be indicated with markings on or under the lower horizontal axis.  A tooltip or hover bar accompanying each of these markings can be used to convey which outcome it gives the critical value of. Critical values falling outside the range of standard covariate imbalances (before and after matching) that are presented on the plot should be indicated by a single marking at the right boundary of the plotting region that is explained in a figure legend. (Specifically, in that note indicating by name the variables that had these critical values.)  These associations should also appear in a hover box for that point, which additionally carries the critical values for the named variables. 

Some key points: $\matchdiff{r}$, $\var (\combdiff{r})$, and $\sigma_{r}$ in \eqref{eq:6}, are readily calculated from the matched sample. The first two (or near relatives) will have to have been calculated to perform the permutation test anyway, and, with $\combdiff{\cdot}$ calculated using harmonic weights,  \sout{$\sigma_r^2 =\frac{1}{2} \frac{\mathrm{ess}}{n} \var (\combdiff{r})$} $\sigma_r^2 =\frac{1}{4} \frac{\mathrm{ess}^{2}}{n} \var (\combdiff{r})$, where ``ess'' refers to the effective sample size of the match.  On the other hand $\rho_{r u}$ and $\matchdiff{u/\sigma_{u}}$ are speculation parameters.  The Love plot will present $\matchdiff{v/\sigma_{v}}$ for measured covariates $v$, and we'll annotate it in a way to suggest use of these values to calibrate speculations about the value of $\matchdiff{v/\sigma_{v}}$.  For $\rho_{r u}$, users will be offered pre-determined reference values based on figuring the predictive value of a second pretest.
 
\section{External calibration of $\rho_{ru}$}
\label{sec:extcalibration}

 One way to understand \eqref{eq:6}'s $\rho_{ru}$ is in terms of its transform $1-\rho_{ru}^2$, the \textit{proportionate decline in unexplained variation} \citep{hosmanetal2010} when $u$ is added to $r$'s regression on matched-set fixed effects as an additional independent variable.  (This terminology reflects the identity $\sigma^2_{r^{\perp u}} = (1-\rho_{ru}^2) \sigma^2_r$.)  We have taken $u$ to be a scalar-valued variable, for simplicity, but vector- or categorical-valued $u$s can be treated similarly \citep[][\S~4]{hosmanetal2010}.  If we were to substitute raw outcomes as opposed to robust residuals for $r$, remove the fixed effects and differential fitting weights for matched sets, and take $u$ to be either a pretest, a collection of baseline demographic variables, or both, then we would be left with a quantity of secondary interest in the planning of group-randomized trials, Hedges and Hedberg's \citeyear{hedges2007intraclass} ``$\eta_W^2$.'' \citet{hedges2007intraclass} tabulate reference values of $\eta_W^2$, by grade, reading, math or writing scale score, and for samples that are nationally representative or representative of low-income schools.  More specifically, Hedges and Hedberg tabulate proportionate declines in unexplained variation for models with adjustments for demographics, pretests or both relative to models with no covariate adjustment; we used these to calculate variation reductions corresponding  to passing from models with pretest adjustments only to models adjusting for pretests and demographics, or from models with demographic adjustments only to models adjusting for demographics and pretests. Results varied somewhat across grades and subjects, but the values $\rho_{ru} =.1$ and $.75$ were roughly typical of these comparisons.


Because in the EE $r$ will often have been covariate-adjusted for a pretest, $\rho_{ru} =.1$, the value characterizing the addition of demographic explanatory variables to a pretest, is an appropriate starting point for sensitivity analysis. On the other hand $\rho_{ru}$ may better characterize omitted variables hypothesized to be strong predictors of the outcome.  Note that such a predictor would have to be unusually strong, strong enough to add to the existing pretest adjustments as much additional predictive power as a pretest adds to adjustments based only on weaker covariates.


Also to assist planning of randomized trials, \citet*{zhu2012designing} present reference values determining $\rho_{ru}$ for $u$s that may be still more relevant to studies analyzed with the EE, particularly in the upper grades. Zhu \textit{et al.}'s study quantitatively characterizes the additional value of classroom-level data over and above individual- and school-level data, for the planning and analysis of randomized trials.  In the lower grades, classroom-level data turns out to add relatively little from the trialist's perspective, while in high school it makes a meaningful contribution. Back-translating from their measures to ours, results again vary somewhat across grades, tests and data sources, but the value $\rho_{ru}=.1$ is characteristic of classroom data's additional contribution for grades K--8, whereas $\rho_{ru}=.75$ typifies the data situation in high schools.


% \bibliography{abbrev_long,causalinference,computing,misc}  
\begin{thebibliography}{10}
\newcommand{\enquote}[1]{``#1''}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi

\bibitem[{Hansen and Bowers(2008)}]{hansen:bowers:2008}
Hansen, B.~B. and Bowers, J. (2008), \enquote{Covariate balance in simple,
  stratified and clustered comparative studies,} \textit{Statistical Science},
  23, 219--236.

\bibitem[{Hedges and Hedberg(2007)}]{hedges2007intraclass}
Hedges, L.~V. and Hedberg, E.~C. (2007), \enquote{Intraclass correlation values
  for planning group-randomized trials in education,} \textit{Educational
  Evaluation and Policy Analysis}, 29, 60--87.

\bibitem[{Hosman et~al.(2010)Hosman, Hansen, and Holland}]{hosmanetal2010}
Hosman, C.~A., Hansen, B.~B., and Holland, P.~W. (2010), \enquote{The
  sensitivity of linear regression coefficients' confidence limits to the
  omission of a confounder,} \textit{Annals of Applied Statistics}, 4,
  849--870.

\bibitem[{Huber(1981)}]{huber1981robust}
Huber, P.~J. (1981), \textit{Robust Statistics}, Wiley-Interscience.

\bibitem[{Imbens(2003)}]{imbens2003sea}
Imbens, G.~W. (2003), \enquote{{Sensitivity to exogeneity assumptions in
  program evaluation},} \textit{American Economic Review}, 126--132.

\bibitem[{Kalton(1968)}]{kalton1968}
Kalton, G. (1968), \enquote{Standardization: {A} technique to control for
  extraneous variables,} \textit{Applied Statistics}, 17, 118--136.

\bibitem[{Rosenbaum(1986)}]{rosenbaum:1986a}
Rosenbaum, P.~R. (1986), \enquote{Dropping out of high school in the United
  States: An observational study,} \textit{Journal of Educational Statistics},
  11, 207-- 224.

\bibitem[{Rosenbaum(2002)}]{rosenbaum:2002a}
--- (2002), \enquote{Covariance adjustment in randomized experiments and
  observational studies,} \textit{Statistical Science}, 17, 286--327.

\bibitem[{Rosenbaum(2005)}]{rosenbaum:heterogeneity:2005}
--- (2005), \enquote{Heterogeneity and Causality: {U}nit Heterogeneity and
  Design Sensitivity in Observational Studies,} \textit{American Statistician},
  59, 147--152.

\bibitem[{Zhu et~al.(2012)Zhu, Jacob, Bloom, and Xu}]{zhu2012designing}
Zhu, P., Jacob, R., Bloom, H., and Xu, Z. (2012), \enquote{Designing and
  analyzing studies that randomize schools to estimate intervention effects on
  student academic outcomes without classroom-level information,}
  \textit{Educational Evaluation and Policy Analysis}, 34, 45--68.

\end{thebibliography}

\end{document}
