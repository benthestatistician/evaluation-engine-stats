\documentclass[11pt]{article}
\input{./ee-planning-preamble}
\newcounter{saveenum}
\author{BH, \ldots}
\date{\currenttime, \today}
\title{Variance approximations for matched differences estimators}
\newcommand{\EE}{\mathbf{E}}
\newcommand{\PP}{\mathbf{P}}
\newcommand{\var}{V}
\newcommand{\cov}{C}
\newcommand{\combdiff}[1]%
{\ensuremath{\langle #1\rangle^{(z)}}}
\newcommand{\Combdiff}[1]%
{\ensuremath{\langle #1\rangle^{(Z)}}}
\newcommand{\combd}[1]%
{\ensuremath{\langle #1\rangle}}
\newcommand{\mdiff}[2]%
{\ensuremath{\langle #1 \rangle_{#2}}}%^{(d)}
\newcommand{\Mdiff}[2]%
{\ensuremath{\bigg\langle #1 \bigg\rangle_{#2}}}%^{(d)}
\newcommand{\mtx}[2]%
{\ensuremath{\langle #1 ]_{#2}}}%^{(t)}
\newcommand{\mctl}[2]%
{\ensuremath{[ #1 \rangle_{#2}}}%^{(c)}


\title{Subgroup ``effects'' and associated covariances via estimating equations}
\author{BH}
\begin{document}
\maketitle

Let $Z$ be a vector indicating assignment to treatment ($Z_{i}=1$)
or control ($Z_{i}=0$); $Y$ a random
outcome vector w/ potential versions $Y_{t}$, observed only when
$Z=1$, and $Y_{c}$, observed only if $Z=0$; $X^{(1)}$, \ldots,
$X^{(k)} $, variables the value of which does not depend on $Z$; and
$ \mathbf{X}$, the $ n \times k$ with columns $X^{(j)}$, $j=1, \ldots,
k$.  

Study subjects come to us having been arranged into matched sets or
strata.  The arrangement may be a function of information encoded in
$Z$ and $\mathbf{X}$, but as our argument will always condition on at
least enough information about $Z$ and $\mathbf{X}$ to determine the
matched arrangement, we treat the composition of matched sets as fixed
rather than random.  For each $i = 1, \ldots, n$, $[i]$ is the
collection of subjects matched to $i$ or a matched counterpart of
$i$.  Provided that each matched set is
either one to one, many to one or one to many, as shall be assumed
here, this defines an
equivalence relation on $\{1, \ldots, n\}$; we write $i\sim j$
when $i \in [j]$, or equivalently $j \in [i]$. (Without-replacement pair matching, with-replacement
one-nearest-neighbor matching, matching with multiple controls and
full matching all have this property, ``full match'' being the general
category within which the others are sub-categories.)  

For $V= X^{(1)}, \ldots, X^{(k)}, Y_{c}, Y_{t}$, or $Y$, 
and any nonempty $s \subseteq \{1,\ldots, n\}$, write
$V_{s}$ for $(V_{i}: i \in s)$. Let the numbers of treatment and
control subjects in each stratum $s$ be fixed at
$n_{ts}= Z_{s}'Z_{s}$ and $n_{cs} = (1-Z)_{s}'(1-Z)_{s} $, respectively,
define
\begin{equation}
  \label{eq:6}
\mdiff{V}{s} := \left\{ \begin{array}{cl}
\frac{Z_{s}'V_{s}}{n_{ts}} -
\frac{(1-Z)_{s}'V_{s}}{n_{cs}}  & \mathrm{if}\, 0 <
n_{cs}\, \mathrm{and}\, n_{ts},\\
0 & \mathrm{otherwise.}
\end{array}
\right.
\end{equation}
 Mean-of-matched differences estimates of treatment effects are then represented as
\begin{equation} \label{eq:7}
\combd{Y} \equiv \sum_{s \in \{[i]: 1 \leq i \leq n\}} w_{s} \left( \frac{Z_{s}'Y_{s}}{n_{ts}} - \frac{(1-Z)_{s}'Y_{s}}{n_{cs}} \right)  = \sum_{s} w_{s}\mdiff{Y}{s},
\end{equation}
for appropriate weights $(w_{s}: s)$.  

For instance, if $n_{t} = \sum \{n_{ts} : 0 < n_{cs}, n_{ts} \}$, then taking $w_{[i]} =
n_{t[i]}/n_{t}$ in \eqref{eq:7} gives effect of treatment on the treated
weighting, the weighting appropriate to estimation of $\EE \left(Y_{t}
  - Y_{c} | Z=1\right) $ (absent unmeasured or residual bias).  In
this case \eqref{eq:7} reduces to
\begin{equation}
\sum_{s \in \{[i]: 1 \leq i \leq n\}} \frac{n_{ts}}{n_{t}}
\mdiff{Y}{s} \equiv \frac{1}{n_{t}}\sum_{i: Z_{i}=1} Y_{i} -
\frac{\sum_{j: j \sim i,\, Z_{j}=0} Y_{j}}{\# \{j: j \sim i,\, Z_{j}=0\}}.
  \nonumber
\end{equation}


Note that while the values of $\{ (Z_{s}'Z_{s}, (1-Z)_{s}'(1-Z)_{s}) = (n_{ts}, n_{cs}): s \in \{[i] : 1\leq i \leq n\}\} $ have been assumed to be fixed, the
full array of values $(Z_{i}: 1 \leq i \leq
n)$ have not.  
% When it is necessary to distinguish these conditioning
% schemes, $\mdiff{V}{s}^{(Z)}$  and $\Combdiff{V}$ will indicate conditioning on the full
% vectors $Z_{s}$ or $Z$, respectively, while $\mdiff{V}{s}$ and
% $\combd{V}$ indicate conditioning only $ (Z_{s}'Z_{s},
% (1-Z)_{s}'(1-Z)_{s}) = (n_{ts}, n_{cs}) $ or on $\{ (Z_{s'}'Z_{s'},
% (1-Z)_{s'}'(1-Z)_{s'}): s' \in \{[i] : 1\leq i \leq n\}\} $, respectively. 

Combinations of matched difference estimation and regression-based
covariate adjustment lead to estimators of the form
$$
\combd{R} \equiv \sum_{s} w_{s}\mdiff{R}{s} = \sum_{s}w_{s} \left\{ \mdiff{Y}{s} - \mdiff{f_{\theta}(\mathbf{X})}{s}\right\}
$$
where $R = Y - f_{\theta}(\mathbf{X})$ and  $f_{\theta}(\cdot)$ is a regression function intended to predict
$Y$ from baseline variables exclusive of $Z$.  (For the remainder of
this note, $\theta$ is regarded as fixed; but we'll develop things
with a view to relaxing this assumption later.)  Formulas involving
$R$'s cover matched difference estimation without covariate adjustment
as a special case, by setting $f_{\theta} \equiv 0 $.


\section{Paired $t$ matched variances and covariances}
\label{sec:abadie--imbens}

We motivate a variance estimation strategy of Abadie \& Imbens's
martingale representation paper (2012, JASA) under somewhat different
conditioning scheme, under which the martingale representation isn't
needed, and in such a way as to permit an extension to subgroup
effects. Like A. \& I., we wind up with formulas reminiscent of
the paired $t$ test.

Following A. \& I., we'll estimate the variance
of matched pair differences with conditioning on $\mathbf{X}$ but not
$Y$ or any function of $Y$, in contrast to permutation-based setups. A
\& I also appear to condition on $Z$, as in the paired $t$-test;
although what follows can accommodate that, we will condition more
narrowly, on the intersection, over matched sets $s \subseteq \{1,
\ldots, n\}$, of vectors $Z_{s}$ falling within specific orbits of
$\{0,1\}^{\# s}$ under permutations of coordinates $s$.  In other
words, we condition on the numbers of treatment and control subjects
per matched set, $\{ (Z_{s}'Z_{s}, (1-Z)_{s}'(1-Z)_{s}) = (n_{ts},
n_{cs}): s \in \{[i] : 1\leq i \leq n\}\} $, or equivalently on $\{
Z_{s}'Z_{s} = n_{ts}: s \in \{[i] : 1\leq i \leq n\}\} $.  This
conditioning appears also to lead to estimates of error like those of
the $t$-test, at least when coupled with suitable assumptions, and it
aligns more neatly with the heuristic that matched sets should ideally
function as blocks in a randomized study.

A. \& I. also condition on $\mathbf{X}$.  Their
argument applies equally well with conditioning on a reduction of
$\mathbf{X}$, as we'll do here.  They might insist that the
conditioning be sufficient to capture information determining the
matches; I don't see this setup as requiring that. For now we proceed using a
smaller sigma field, namely $\mathcal{F} =
\sigma (\{
Z_{s}'Z_{s} = n_{ts}: s \in \{[i] : 1\leq i \leq n\}\},
f(\mathbf{X}))$, some $f(\cdot)$.\footnote{To accommodate propensity score
matching we'd also need to include a sufficient statistic for the
propensity score. If the PS is estimated using logistic regression,
that would be $Z'\mathbf{X}$.  I suspect this modification could be imposed
without significant modification to the below, but as I don't have the
time to think it through carefully and I don't want to make a mistake,
I'm leaving it for later.   

(A more ambitious undertaking is
to adapt everything in such a way as to permit matching on prognostic
scores, which would require conditioning also on that sufficient
statistic; I'm not totally optimistic about the currently line of
thinking extending that far, but who knows.)} 


\subsection{A conservative standard error for combined paired
  differences }
Let $\mathcal{F}$ be a sigma field generated
by the numbers of treatment subjects per matched set, potentially in
conjunction with full or partial information about covariates:
$$
\mathcal{F} = \sigma \{\{ Z_{s}'Z_{s} = n_{ts}: s \in \{[i] : 1\leq i \leq n\}\}, f(\mathbf{X})\},
$$
some designated function $f(\cdot )$. Assume this information
suffices to determine the weighting scheme $\{w_{s}: s \in \{[i] :
1\leq i \leq n\} \}$.  One has, letting $s$ range over $\{[i]: 1 \leq i \leq n\} $ and
taking $\mdiff{\cdot}{s}$ to be as defined in \eqref{eq:6},
\begin{eqnarray}
\var(\combd{R} | \mathcal{F}) &=&
%\sum_{s}w_{s}^{2}\var\left(\mdiff{R}{s}  | \mathcal{F} \right) =
\sum_{s} w_{s}^{2} \var \left(\mdiff{R}{s}   | \mathcal{F} \right)
\nonumber \\
&=& \sum_{s} w_{s}^{2} \EE\left[ \left( \mdiff{R}{s} - \mu_{\mdiff{R}{s}}\right)^{2}  |
  \mathcal{F} \right] \nonumber \\
&\leq & \sum_{s} w_{s}^{2} \EE\left[ \left( \mdiff{R}{s} - \mu_{{R}}\right)^{2}  |
  \mathcal{F} \right], \label{eq:3}
\end{eqnarray}
where $\mu_{\mdiff{R}{s}} = \EE(\mdiff{R}{s}| \mathcal{F}) $ and  
$\mu_{{R}} = \EE(\combd{R}| \mathcal{F}) = \sum_{s} w_{s}
\mu_{\mdiff{R}{s}}$.  The sum in \eqref{eq:3} is estimable by
$\sum_{s} w_{s}^{2} (\mdiff{r}{s} - \combd{r})^{2}$.

Remarks:\\
\begin{itemize}
\item  in the case of
ETT weighting, the variance estimate boils down to
$$
\sum_{s} \left(\frac{n_{ts}}{n_{t}} \right)^{2} (\mdiff{r}{s}
-\combd{r})^{2} .
$$
\item Note that \eqref{eq:3} would hold for any
$\mathcal{F}$, not only $\mathcal{F} =\sigma\left( \{ Z_{s}'Z_{s} =
  n_{ts}: s \in \{[i] : 1\leq i \leq n\}\} , f(\mathbf{X}) \right)$ ,
provided that $\mu_{\mdiff{R}{s}}$ and $\mu_{{R}}$ were defined with
respect to the same $\mathcal{F}$.
\item The bound in \eqref{eq:3} is sharp, with attainment under ``constant
effects,'' more specifically if $\EE (\mdiff{R} {s} | \mathcal{F})=
\EE (\combd{R}| \mathcal{F})$, all $s$.  That is, if there is no
variation in expected values of matched differences.

\item The ``constant effects'' assumption \footnote{In the special
    case that $\mdiff{R}{s}$ is conditionally unbiased for
    $\EE\left(n_{s}^{-1}\sum_{i \in s}(Y_{ti} - Y_{ci})\vert
      \mathcal{F}\right)$, each $s$, the assumption we're now calling
    ``constant effects'' is equivalent to assuming the (conditional
    expectation given $\mathcal{F}$ of the) treatment effect to be
    constant; but for present purposes, ie variance and covariance
    estimation for both biased and unbiased matched differences
    estimators, that is only a special case.} can be made more
  palatable by certain choices of sigma field $\mathcal{F}$.  Say $\mathcal{F}$ is the sigma field generated by $Z$,
  $\phi(\mathbf{X})$ and subgrouping variables, as opposed to $Z$ and
  the full covariate $\mathbf{X}$.  Here $\phi(\cdot)$ is a scalar
  valued function with the property that matched differences on
  $\phi(\mathbf{X})$ diminish uniformly toward 0 as the size of the
  matching problem increases. By comparison with conditioning on all of
  $\sigma\left(Z, \mathbf{X} \right)$, conditioning on this $ \mathcal{F}$
  makes it more plausible that the conditional expectation $\EE (\mdiff{R} {s}
  | \mathcal{F}) $ should not vary across matched sets. With conditioning on all of $\mathbf{X}$, if the outcome $Y$
  has a boundary restriction then there can easily be some values of
  $\mathbf{x} $ for which $\EE ( Y | Z= 0, \mathbf{X}=\mathbf{x})$
  approaches the upper boundary, so that if one of these should appear
  as the control in a matched pair then for that pair $\EE (\mdiff{R}
  {s} | Z_{s}, \mathbf{X}_{s}) $ has to be small.  In contrast,
  suppose that $\phi(\cdot )$ is a scalar function of $\mathbf{X}$
  intended to model the propensity score. Propensity score estimates
  typically correlate only weakly with $\EE (Y | Z=z, \mathbf{X})$, so
  it would be unlikely that there would be $\EE ( Y | Z= 0,
  \phi(\mathbf{X}))$'s falling close to boundaries of the range of
  $Y$\footnote{Or if there were, that would be because many or most of
    them fall similarly close to the boundary, in which case it isn't
    an anomaly and it isn't a challenge to the premise that $\EE
    (\mdiff{R} {s} | Z_{s}, \phi(\mathbf{X}_{s})) $ does not vary with
    $\phi(\mathbf{X}) $.}.
\end{itemize}


\subsection{Subgroup analysis}

Subgroup ``effects'' can be seen as by-products of additional
estimating equations. (Again, scare quotes mean the indicated term is
interpretable as a causal effect or estimate thereof in absence of
unmeasured or residual bias only.)  Let $C^{(1)}$, $C^{(2)}$, \ldots, refer
to subgroup indicator (dummy) variables, indicating mutually exclusive
subgroups; likewise for $D^{(1)}$, $D^{(2)}$, etc. Expand the sigma
field $\mathcal{F}$ to include matched differences on each of the
subgrouping indicators:
$$ 
\mathcal{F} = \sigma \{\mdiff{C^{(1)}}{s}, \mdiff{C^{(2)}}{s}, \ldots;
\mdiff{D^{(1)}}{s}, \mdiff{D^{(2)}}{s}, \ldots; \{ Z_{s}'Z_{s} = n_{ts}: s \in \{[i] : 1\leq i \leq n\}\}; f(\mathbf{X})\},
$$

\subsubsection{Estimates and estimating equations}
The main ``effect'' $\mu_{{R}}$ is
defined to be the solution of
$$
 \arg\min_{\mu} \sum_{s} w_{s}\EE \left\{
  (\mdiff{R - Z\mu}{s})^{2}| \mathcal{F}\right\}
=
\arg\min_{\mu} \sum_{s} w_{s} \EE \left\{
  \left(\mdiff{R}{s} - \mu \right)^{2}| \mathcal{F}\right\} ,
$$
for
which the corresponding estimating equation is simply
\begin{eqnarray*}
\arg_{\hat{\mu}} \{\sum_{s} w_{s}\left[\mdiff{r-z\hat{\mu}}{s} \right] &=& 0\}, \mbox{ or}\\
\arg_{\hat{\mu}} \{\sum_{s} w_{s}\left[\mdiff{r} {s} -\hat{\mu} \right]&=& 0\}, \mbox{ or}\\
\arg_{\hat{\mu}} \{\combd{r} - \hat{\mu}\sum_{s} w_{s} &=& 0\}; \mbox{ so that}\\
\hat{\mu}&:=& \combd{r} .\\
\end{eqnarray*}
On the other hand if subgroup-specific ``effects''
are permitted then
their defining equation is, using $Z \cdot D$ to
indicate column-wise multiplication of vectors\footnote{Not the vector
cross product, $Z'D = \sum_{i=1}^{n} Z_{i}D_{i}$, but rather the mapping of pairs of
length-$n$ vectors into their element-wise product, a vector of length
$n$.},
\begin{eqnarray*}
\lefteqn{\arg\min_{\mu_{g}: g} \sum_{s} w_{s} \EE \left\{
\mdiff{R - Z \cdot (\sum_{g} D^{(g)}\mu_{g}) }{s}^{2}|
  \mathcal{F}\right\}
 }\\
&=&\arg\min_{\mu_g: g} \sum_s w_s\EE \left\{
  \left( \mdiff{R}{s} - \sum_{s} \sum_{g} \frac{Z_{s}' D^{(g)}}{n_{ts}} \mu_{g}
\right)^{2}|
  \mathcal{F}\right\} ,  \\
\end{eqnarray*}
where $g$ ranges over subgroups%, $n_{t} = \sum_{s} n_{ts} = z'z$ and
                               %the final equation presumes ETT
                               %weights, $w_{s} \propto n_{s}$
.  Adding in main effects for the subgrouping variable gives
\begin{eqnarray*}
\lefteqn{\arg\min_{\mu_{g}, \beta_{g}: g} \sum_{s} w_{s} \EE \left\{
\Mdiff{R -  (\sum_{g} D^{(g)}\beta_{g} + Z \cdot D^{(g)}\mu_{g}) }{s}^{2}|
  \mathcal{F}\right\}
 }\\
&=&\arg\min_{\mu_g, \beta_g: g} \sum_s w_s\EE \left\{
  \left[ \mdiff{R}{s} -  
    \sum_{g}  \left(\mdiff{D^{(g)}}{s}\beta_{g} + \frac{Z_{s}' D^{(g)}}{n_{ts}} \mu_{g} \right)
\right]^{2}|  \mathcal{F}\right\}  \\
&=&\arg\min_{\mu_g, \beta_g: g} w_s\EE \left\{
  \left( \mdiff{R}{s} - \sum_{g} \mdiff{D^{(g)}}{s}\beta_{g} - \sum_{g}
    \mdiff{Z \cdot D^{(g)}}{s}\mu_{g}
\right)^{2}|  \mathcal{F}\right\}  \\
\end{eqnarray*}

If subgroup analysis has been requested for two subgrouping variables
then the equation becomes
\begin{eqnarray}
\lefteqn{\arg\min_{\mu_{h}, \beta_{h}: h; \alpha_{g}: g} \sum_{s} w_{s} \EE \left\{
\Mdiff{R -  \sum_{g} C^{(g)}\alpha_{g} - \sum_{h} \left(D^{(h)}\beta_{h} + Z \cdot D^{(h)}\mu_{h}\right) }{s}^{2}|
  \mathcal{F}\right\}
 } \label{eq:11}\\
&=&\arg\min_{\mu_{h}, \beta_{h}, \alpha_{g}} w_s\EE \left\{
  \left( \mdiff{R}{s} -\sum_{g} \mdiff{C^{(g)}}{s}\alpha_{g} - \sum_{h} \mdiff{D^{(h)}}{s}\beta_{h} - \sum_{h}
    \mdiff{Z \cdot D^{(h)}}{s}\mu_{h}
\right)^{2}|  \mathcal{F}\right\} , \nonumber
\end{eqnarray}
where the second subgrouping variable is represented by indicator
vectors $( C^{(j)}: j)$.  Note carefully that the subgrouping
variables are interacted with treatment one at a time.

These displays treat the case of two subgrouping variables, but in
practice each of the five supported subgrouping variables should
contribute a main effect to each estimating equation, whether or not
subgroup analysis has been requested for it%
\footnote{Main effects are included for subgrouping variables that
  were not requested for the purpose of consistency of subgrouping
  estimates across near-identical reports.  If a user submits a job
  requesting two subgroups, then resubmits it requesting also a third,
  we'd like the effect estimates returned in the two jobs to look
  similar (despite the fact that the $p$-values returned in the second
  report will be somewhat attenuated due to the broader multiplicity
  adjustment).}.  
To keep notational complexity in
check, I won't explicitly write out equations analogous to
\eqref{eq:11}, or to \eqref{eq:12} below, for three or more
subgrouping variables.

The sample analogue of \eqref{eq:11} is simply%\marginpar{To do: here and below,``$R$'' $\mapsto r$, ``$D^{(h)}$'' $ \mapsto d^{(h)}$, ``$Z$'' $\mapsto z$. }
\begin{equation} \label{eq:12}
\arg\min_{\mu_{h}, \beta_{h}, \alpha_{g}} w_s 
  \left( \mdiff{r}{s} - \sum_{g} \mdiff{c^{(g)}}{s}\alpha_{g} - \sum_{h} \mdiff{d^{(h)}}{s}\beta_{h} - \sum_{h}
    \mdiff{z \cdot d^{(h)}}{s}\mu_{h}
\right)^{2} . 
\end{equation}
On differentiation, \eqref{eq:11} gives rise to
a system of estimating equations $\left(E_{h}' = 0, E_{h} = 0: h\right)$, $\left(E_{g}'=0: g\right)$  where
\begin{eqnarray*}
E_{h}'\left(\vec{\alpha}, \vec{\beta}, \vec{\mu} \right) &=&  \sum_{s} w_{s} \mdiff{D^{(h)}}{s}
\left[ \mdiff{R} {s} -  \sum_{l} \mdiff{C^{(l)}}{s}\alpha_{l} - \sum_{k} \left(\mdiff{D^{(k)}}{s}\beta_{k} + \frac{ Z_{s}'
      D_{s}^{(k)}}{n_{ts}} \mu_{k} \right)
\right]  ;\\
E_{g}'\left(\vec{\alpha}, \vec{\beta}, \vec{\mu} \right) &=&  \sum_{s} w_{s} \mdiff{C^{(g)}}{s}
\left[ \mdiff{R} {s} -  \sum_{l} \mdiff{C^{(l)}}{s}\alpha_{l} - \sum_{k} \left(\mdiff{D^{(k)}}{s}\beta_{k} + \frac{ Z_{s}'
      D_{s}^{(k)}}{n_{ts}} \mu_{k} \right)
\right]  ;\\
E_{h}\left(\vec{\alpha}, \vec{\beta}, \vec{\mu} \right) &=&  \sum_{s} w_{s}\left( \frac{Z_{s}'D_{s}^{(h)}}{n_{ts}} \right)
\left[ \mdiff{R} {s} -  \sum_{l} \mdiff{C^{(l)}}{s}\alpha_{l}-  \sum_{k} \left( \mdiff{D^{(k)}}{s}\beta_{k} +
    \frac{ Z_{s}'
      D_{s}^{(k)}}{n_{ts}} \mu_{k} \right)
\right].
\end{eqnarray*}
% square inconsistency that this has RVs whereas prev "EE" had data vectors
The sample analogues of these equations
 % $e'_{g}(\vec{\alpha}, \vec{\beta}, \vec{\mu})$, $e'_{h}(\vec{\alpha},
 % \vec{\beta}, \vec{\mu})$, and $e_{g}(\vec{\alpha}, \vec{\beta}, \vec{\mu})$
are the estimating equations of the
least squares regression \texttt{y \textasciitilde g + h -1 +
  z\_h}, with observations $\mathtt{y} = (\mdiff{R}{s}: s)'$,
$\mathtt{g} = ( (\mdiff{C^{(g)}}{s}: s)': g)$, $\mathtt{h}= (
(\mdiff{D^{(h)}}{s}: s)': h)$, and $\mathtt{z\_{}h} = ( (\mdiff{Z\cdot
  D^{(h)}}{s} : s)': h) $, and with case weights $(w_{s}: s)$.    

\paragraph{Computing notes.}  We can fit the sample analogue of
\eqref{eq:12} by running OLS on a data frame of matched set
differences.

\begin{enumerate}
\item  The development function \texttt{mlm} is intended for this purpose.
\item Note the absence of an intercept term.
\item Note also that if members of
the $g$th subgroup are matched only amongst themselves, then
$\mdiff{D^{(h)}}{s} \equiv 0$; the corresponding variable might have to be manually
dropped from the regression specification. 
\item It will occasionally happen that the columns of the matrix
$$ 
\left( \left( (\mdiff{c^{(g)}}{s} :s)' : g
  \right) ; \left( (\mdiff{d^{(h)}}{s} :s)' : h \right) ; \left(
    (\mdiff{z \cdot d^{(h)}}{s} :s)' : h \right) \right)
$$
are linearly dependent, in which case \texttt{lm.fit} will report NAs
for some of the coefficients. We need to arrange that in these cases
nuisance coefficients, the coefficients on $(\mdiff{c^{(g)}}{s} :s)'$
columns or $(\mdiff{c^{(h)}}{s} :s)'$ columns, get nulled out before interest
coefficients, coefficients on $(\mdiff{z \cdot d^{(h)}}{s} :s)'$
columns. Perhaps this can be arranged by some combination of
pre-screening for constant columns, within \texttt{mlm}, and/or
entering right hand side variables into the model formula in the
proper order.
\item But we may not be so lucky; rooting out linear
dependencies among subgrouping variables and the interaction of some
one of them with the treatment may necessitate additional steps.
\item Regarding variance estimation (discussed below): If a coefficient
  is nulled out, I suggest simply removing the corresponding estimating
  equation and rows and columns of the $A$ and $B$ matrices.  It may
  be that routines from the \texttt{sandwich} package have usable defaults.
\item There's a statistical subtlety lurking in the possibility of
  linear dependence, having to do with the fact that under
  $\mathcal{F}$ the $\left(
    (\mdiff{z \cdot d^{(h)}}{s} :s)' : h \right) $ columns represent
  random regressors. They might be collinear with remaining columns
  under some but not all of the realizations of $Z$ that $\mathcal{F}$
  sees as equivalent to the observed $z$. I confess to not having
  entirely thought this and its implications through.  It may be that
  the estimating equations setup should be drawing a distinction
  between instances in which $C$ and $D$ columns are linearly
  dependent and instances in which the main effects design matrix is
  of full rank while its expansion to include interactions with the
  treatment is of reduced rank. For now I'll lean on the fact that
  relative to the  sigma field  $\mathcal{G}$ defined below, these too
  can be treated as fixed regressors, so we won't draw such
  a distinction at present.  However, it should be coded with an eye
  to the possibility that in the future we may want to eliminate
  linear dependencies in the main effects design matrix while
  addressing the possibility of dependency among $\left(
    (\mdiff{z \cdot d^{(h)}}{s} :s)' : h \right) $ columns otherwise. 
\end{enumerate}


\subsubsection{``A'' matrices (for $A^{-1}BA^{-1}$ parameter estimate covariances)}

The ``bread'' component of sandwich-based variance estimators follows
immediately from this representation: Extract gradient of estimating equations
from the \texttt{mlm} fit; invert, or for good measure compute
generalized inverse; if there are multiple subgrouping analyses
and thus multiple \texttt{mlm} fits, then create block-diagonal matrix
from gradient matrix inverses, this being an inverse of the
block-diagonal matrix of estimation equation gradient matrices. The
main effect estimate is represented in this stack of estimating
equations by a $1\times 1$ matrix containing the value $-1$, this
being the derivative of $E_{0} = \sum_{s}w_{s} [\mdiff{R} {s} -
\mu]$ w.r.t. $\mu$.

I expect it will be most efficient to tap into the \texttt{sandwich}
package's setup, using its \texttt{bread} functions to extract
estimating equation gradients from \texttt{mlm} fits, which
\texttt{lm} methods should be able to handle.
% In the  $A$ matrix, rows correspond to estimating equations and
% columns to estimated parameters.  Since there is one estimating
% equation per parameter, this yields a square matrix. Rows of the
% matrix are simply (expected values of) gradients, with respect to parameter values, of the estimating
% equations.




\subsubsection{The covariance among estimating equations (B submatrix) for a particular
subgrouping variable}

Let $\mathcal{G} = \sigma\left(\mathcal{F} \cup
  \sigma(\{ (Z_{s}'C^{(g)}:s, g); \ldots; (Z_{s}'D^{(h)}_{s}: s, g) \})\right) $. Treating the $\beta_{k}$s and $\mu_{k}$s as constants, one has associated conditional covariances
\begin{eqnarray}
\lefteqn{\cov \left(E_{h}(\vec{\alpha}, \vec{\beta}, \vec{\mu}), E_{h'}(\vec{\alpha}, \vec{\beta}, \vec{\mu}) |
    \mathcal{G}
\right) =} \label{eq:1}\\
&  &
\sum_{s} w_{s}^{2} \left( \frac{Z_{s}'D_{s}^{(h)}}{n_{ts}} \right) \left( \frac{Z_{s}'D_{s}^{(h')}}{n_{ts}}
\right) \var\left(  \mdiff{R} {s} - \sum_{l}
  \mdiff{C^{(l)}}{s}\alpha_{l} -   \sum_{k} \mdiff{D^{k}}{s}\beta_{k}- \sum_{k} \frac{ Z_{s}'
      D_{s}^{(k)}}{n_{ts}} \mu_{k} | \mathcal{G}\right), \nonumber\\
\lefteqn{\cov \left(E_{h}'(\vec{\alpha}, \vec{\beta}, \vec{\mu}), E_{h'}'(\vec{\alpha}, \vec{\beta}, \vec{\mu}) |
    \mathcal{G}
\right) = } \\
&  &
\sum_{s} w_{s}^{2} \cdot \mdiff{D^{(h)}}{s} \cdot \mdiff{D^{(h')}}{s}
\cdot \var\left(  \mdiff{R} {s}  - \sum_{l}
  \mdiff{C^{(l)}}{s}\alpha_{l} -   \sum_{k} \mdiff{D^{k}}{s}\beta_{k}- \sum_{k} \frac{ Z_{s}'
      D_{s}^{(k)}}{n_{ts}} \mu_{k} | \mathcal{G}\right) ,\,
  \mbox{and\ } \nonumber \\
\lefteqn{\cov \left(E_{h}(\vec{\alpha}, \vec{\beta}, \vec{\mu}), E_{h'}'(\vec{\alpha}, \vec{\beta}, \vec{\mu}) |
    \mathcal{G}
\right) = } \label{eq:9} \\
&  &
\sum_{s} w_{s}^{2} \cdot \left( \frac{Z_{s}'D_{s}^{(h)}}{n_{ts}}
\right) \cdot \mdiff{D^{(h')}}{s}
\cdot \var\left(  \mdiff{R} {s} - \sum_{l}
  \mdiff{C^{(l)}}{s}\alpha_{l} -   \sum_{k} \mdiff{D^{k}}{s}\beta_{k}- \sum_{k} \frac{ Z_{s}'
      D_{s}^{(k)}}{n_{ts}} \mu_{k} | \mathcal{G}\right) .\nonumber 
\end{eqnarray}
Some observations about \eqref{eq:1}--\eqref{eq:9}:\\
 \begin{enumerate}
\item The conditioning on $\mathcal{G}$,
  as opposed to just $\mathcal{F}$,
  reflects what would be assumed for the ``meat'' stage of sandwich
  estimation of error for a regression model with \textit{fixed}
  regressors.  (Fixed regressors being the usual assumption, whereas
  conditioning on $\mathcal{F}$ corresponds to random regressors
  because $Z_{s}'D_{s}^{(h)}$ is not constant given $\mathcal{F}$.)
\item In consequence, off-the-shelf meat extraction for \texttt{lm}'s,
  e.g. as implemented in the \texttt{sandwich} package, ought to give
  us valid estimates of  \eqref{eq:1}--\eqref{eq:9}.  (However, unless
  the off-the-shelf routines can also handle cross-subgrouping
  variable covariances as in \S~\ref{sec:cross-subgr-vari}, we'll have
  to grow our own. In that case the off-the-shelf routines may mostly
  be useful for testing ours. )
\item Conventional wisdom has it that the fixed-regressors variance
  estimate is consistent for regression models with random regressors
  too.  One argument I've seen for this relies on the principle
  $\cov(X, Y) = \EE (\cov(X, Y| A)) + \cov (\EE(X|A), \EE(Y|A))$, plus
  an inference from model assumptions that $\EE(X|A) = \EE(Y|A)=0$. I
  think this will follow from \eqref{eq:2}, but 
I haven't fully thought it through as of this writing.
 \item For fixed $(\alpha_{l} : l )$,  $(\beta_{h}, \mu_{h}: h) $,  and for any particular $s$, the
   variance contribution at right of \eqref{eq:1} is estimable, as
   the square of $\mdiff{r} {s} - (\sum_{l}
   \mdiff{c^{(l)}}{l}\alpha_{l}) - (\sum_{h} \mdiff{d^{(h)}}{s} \beta_{h} +  z_{s}'d_{s}^{(h)}
   \mu_{h}/n_{ts} )$.  Relative to the model
   \begin{equation}
     \label{eq:2}
     \EE(\mdiff{R} {s}| \mathcal{G})  =
     \sum_{l} \mdiff{C^{(l)}}{s}\alpha_{l} + \sum_{h}\mdiff{D^{(h)}}{s}\beta_{s} +  n_{ts}^{-1} \sum_{h} Z_{s}D_{s}^{g}
     \mu_{h}, \, \mathrm{all}\, s,
   \end{equation}
the estimate is unbiased. By extension, with consistent estimates $\hat{\mu}_{h}$ then
the plug-in covariance estimate based on \eqref{eq:1}
will be consistent, in the sense of its ratio with the
true covariance converging in probability to 1, under assumption  (\ref{eq:2}).
\item  otherwise, it's unbiasedly an overestimate:
\begin{eqnarray}
  \label{eq:8}
\lefteqn{\var\left(  \mdiff{R} {s} -  \sum_{l}
    \mdiff{C^{(l)}}{s}\alpha_{l} -  \sum_{k} \mdiff{D^{k}}{s}\beta_{k}- \sum_{k} \frac{ Z_{s}'
      D_{s}^{(k)}}{n_{ts}} \mu_{k} | \mathcal{G}\right)} \\
& = & \EE  \left[  \left( \mdiff{R} {s} -  \sum_{l}
    \mdiff{C^{(l)}}{s}\alpha_{l} -  \sum_{k} \mdiff{D^{k}}{s}\beta_{k}- \sum_{k} \frac{ Z_{s}'
      D_{s}^{(k)}}{n_{ts}} \mu_{k} \right)^{2} | \mathcal{G}\right] \nonumber \\
& & -  \left(\EE
    (\mdiff{R}{s} | \mathcal{G}) - \sum_{l}
    \mdiff{C^{(l)}}{s}\alpha_{l} -  \sum_{k} \mdiff{D^{k}}{s}\beta_{k}- \sum_{k} \frac{ Z_{s}'
      D_{s}^{(k)}}{n_{ts}} \mu_{k}  \right)^{2}.   \nonumber
\end{eqnarray}
To be explored: is there a meaningful sense in which the matrix that's
estimated by the matrix of plug-in covariance estimates based on
\eqref{eq:1}--\eqref{eq:9} can be taken as an ``overestimate'' of the
actual covariance matrix?  I think so, but no time (8/2014) to think
through.
\item Implicitly \eqref{eq:1}--\eqref{eq:9} rely on $\mathcal{G}$
  being so specified as to make $\mdiff{R}{s}$ and $\mdiff{R}{s'}$
  conditionally independent, given $\mathcal{G}$, for $ s \neq s'$;
  otherwise there would be cross-stratum summands as well.  (Noted in
  part b/c cross-stratum independence of $R$'s would be compromised by
  expanding conditioning statement to incorporate statistics of a
  prognostic score.)
\end{enumerate}

\subsubsection{Cross-subgrouping variable covariances} % to account for multiple main effects
\label{sec:cross-subgr-vari}

In order to associate a covariance with the totality of main effect
and subgrouping parameters being estimated, we'll also need
covariances for the estimating equations associating with differing
subgrouping variables.  Hold open the possibility that
the categorical variable represented by dummy variables $(C^{(j)}:
j)$ has only one ``subgroup'' $j$, to handle the main effect's
covariance with subgroup effects in the same stroke.  
% Expand
% the sigma field yet again:
% $$
% \mathcal{G} = \sigma\left(\mathcal{F},
%    (\mdiff{C^{(h)}}{s}:s, g), (Z_{s}'C^{(h)}_{s}: s, g),
%    (\mdiff{D^{(h)}}{s}:s, g), (Z_{s}'D^{(h)}_{s}: s, g) \right)
% $$

The covariances of subgroup effects are given by
\begin{eqnarray}
\cov \left(E_{h}(\vec{\alpha}, \vec{\beta}, \vec{\mu}), E_{j}(\vec{\gamma}, \vec{\delta}, \vec{\upsilon}) | \mathcal{F}
\right) &=& \sum_{s} w_{s}^{2} \left( \frac{Z_{s}'D_{s}^{(h)}}{n_{ts}} \right) \left( \frac{Z_{s}'C_{s}^{(j)}}{n_{ts}}
\right) \cdot  E^{C,D}_{s} \label{eq:4}\\
\cov \left(E_{h}'(\vec{\alpha}, \vec{\beta}, \vec{\mu}), E_{j}'(\vec{\gamma}, \vec{\delta}, \vec{\upsilon}) | \mathcal{F}
\right) &=& \sum_{s} w_{s}^{2} \mdiff{D^{(h)}}{s} \cdot
\mdiff{C^{(j)}}{s} 
\cdot  E^{C,D}_{s}  \label{eq:10} \\
\cov \left(E_{h}(\vec{\alpha}, \vec{\beta}, \vec{\mu}), E_{j}'(\vec{\gamma}, \vec{\delta}, \vec{\upsilon}) | \mathcal{F}
\right) &=& \sum_{s} w_{s}^{2} \left( \frac{Z_{s}'D_{s}^{(h)}}{n_{ts}} \right) \cdot
\mdiff{C^{(j)}}{s} % \left( \frac{Z_{s}'C_{s}^{(j)}}{n_{ts}}\right) 
\cdot E^{C,D}_{s} \\
\cov \left(E_{h}'(\vec{\alpha}, \vec{\beta}, \vec{\mu}), E_{j}(\vec{\gamma}, \vec{\delta}, \vec{\upsilon}) | \mathcal{F}
\right) &=& \sum_{s} w_{s}^{2}  \mdiff{D^{(h)}}{s}  \cdot
\left( \frac{Z_{s}'C_{s}^{(j)}}{n_{ts}}\right) 
\cdot E^{C,D}_{s} 
\end{eqnarray}
with $k$ ranging over levels of $D$ while $j$ ranges over levels of
$C$ and 
\begin{eqnarray*}
E^{C,D}_{s} =  \cov \bigg( &  \mdiff{R} {s} & - \sum_{l}
  \mdiff{C^{(l)}}{s}\alpha_{l} -  \sum_{k} \mdiff{D^{(k)}}{s}
  \beta_{k} -
\sum_{k} \frac{ Z_{s}'D_{s}^{(k)}}{n_{ts}} \mu_{k}^{(D)} ,
\nonumber \\ 
&   \mdiff{R} {s} & - \sum_{l} \mdiff{C^{(l)}}{s}\gamma_{l} 
-  \sum_{k} \mdiff{D^{(k)}}{s}  \delta_{k}
- \sum_{l}
\frac{ Z_{s}' C_{s}^{(l)}}{n_{ts}} \upsilon_{l}^{(C)}
| \mathcal{F} \bigg) .
\end{eqnarray*}

Under the somewhat odd assumption that each of the two subgrouping
models is correctly specified unto itself (\textit{i.e.} without interactions of
the other set of subgrouping indicators with the treatment variable),
   \begin{eqnarray*}
     \EE(\mdiff{R} {s}| \mathcal{G}) & = & \sum_{j}
     \mdiff{C^{(j)}}{s}\delta_{j} + \sum_{h}\left( \mdiff{D^{(h)}}{s} \beta_{h}
     +  \frac{Z_{s}D_{s}^{(h)}}{n_{ts}} \mu_{h} \right) \\
& = & \sum_{h}\mdiff{D^{(h)}}{s} \delta_{h} + 
   \sum_{j} \left(\mdiff{C^{(j)}}{s}\gamma_{j} +
     \frac{Z_{s}C_{s}^{(j)}}{n_{ts}} \upsilon_{j} \right),\, \mathrm{all}\, s,
   \end{eqnarray*}
the covariance terms $E^{C,D}_{s}$ are consistently estimated by
$$
e^{C,D}_{s} := \left(  \mdiff{r} {s} - \sum_{j}\mdiff{c^{(j)}}{s}\hat{\alpha}_{j} -  \sum_{k} \left(\mdiff{d^{(k)}}{s} \hat{\beta}_{k}
    + \frac{ z_{s}'
      d_{s}^{(k)}}{n_{ts}} \hat{\mu}_{k}^{(D)}\right) \right)
\left(  \mdiff{r} {s}  - \sum_{h}\mdiff{d^{(h)}}{s} \hat{\delta}_{h} -  \sum_{j} \left(\mdiff{c^{(j)}}{s} \hat{\gamma}_{j}
    + \frac{ z_{s}'
      c_{s}^{(j)}}{n_{ts}} \hat{\upsilon}_{j}^{(C)}\right) \right) 
$$
{}--- that is, the product of the $s$th residual of the fitted model
with $Z$-$D$ interactions and the $s$th residual of the fitted
model with$Z$-$C$ interactions.
I don't know that we'll find ready-made extractor functions to
assemble covariance estimates based on all of this, but we should be
able to get all of it by taking cross products of appropriate
by-products of the respective \texttt{mlm} fits. 
 
% The function \texttt{eem:::esteqn\_cov} encodes this expression with reference to ``pieces'' 1, 2a and 2b,
% indicated in the following:
% \begin{equation} \label{eq:5}
% \sum_{s} \underbrace{\left( \frac{z_{s}'d_{s}^{(h)}}{n_{t}} \right) }_{1}
% \left(  \underbrace{\mdiff{r} {s}}_{2a} -  \underbrace{\sum_{k} \left(\frac{ z_{s}'
%       d_{s}^{(k)}}{n_{ts}} \hat{\mu}_{k}^{(D)}\right)}_{2b} \right)
% \underbrace{\left(  \frac{z_{s}'c_{s}^{(j)}}{n_{t}} \right) }_{1}
% \left(  \underbrace{\mdiff{r} {s}}_{2a} -  \underbrace{\sum_{j} \left(\frac{ z_{s}'
%       c_{s}^{(j)}}{n_{ts}} \hat{\mu}_{j}^{(C)}\right)}_{2b} \right) .
% \end{equation}



%\subsection{Adaptation to variances that condition on the true PS}


\subsection{Multiple outcomes}
There are no immediate plans for the EE to have multiplicity adjustments involving
distinct outcome variables.   If there were, however, their
covariances would be treated as follows.  (We don't presently treat
covariances of subgroup effects involving distinct outcomes, but that
could be added without great difficulty.)

Assuming constant effects for both outcomes, 
the constant effects model
$$
\EE (\mdiff{Y} {s} | \mathcal{F}) \equiv \mu_{Y},\,\,
\EE (\mdiff{W} {s} | \mathcal{F}) \equiv \mu_{W}
$$
(where $\mu_{Y}= \EE(\combd{Y}| \mathcal{F}) $ and
$\mu_{W}= \EE(\combd{W}| \mathcal{F}) $), one has 
\begin{eqnarray*}
%\lefteqn{
  \cov(\combd{Y}, \combd{W} | \mathcal{F}) %}
&=& \sum_{s} w_{s}^{2} \cov\left(\mdiff{Y} {s}, \mdiff{W} {s} | \mathcal{F}\right)
\\
&= & \sum_{s} w_{s}^{2} \EE \left(  (\mdiff{Y} {s} - \hat{\mu}_{Y})(
  \mdiff{W} {s} - \hat{\mu}_{W})| \mathcal{F}\right),
\end{eqnarray*}
the pair of constant effects assumptions
being necessary for the second equality.  The straightforward covariance
estimate is then
$$
\sum_{s}w_{s}^{2}(\mdiff{y} {s} - \combd{y}) (\mdiff{w} {s} - \combd{w})
$$
or in the presence of covariance adjustment
$$
\sum_{s}w_{s}^{2}\left(\mdiff{y - f_{\theta}(\mathbf{x})}{s} - \combd{y -  f_{\theta}(\mathbf{x})}\right)%
\left(d_{s}(z, w - g_{\gamma}(\mathbf{x})) - \combd{w -
    g_{\gamma}(\mathbf{x})}\right) .
$$



\end{document}
