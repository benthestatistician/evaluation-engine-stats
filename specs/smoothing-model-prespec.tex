\documentclass[11pt]{article}
\input{ee-planning-preamble}
\newcounter{saveenum}
\author{BH, \ldots}
\date{\currenttime, \today} %maybe something better, such as an imprint of the git commit?
\title{Smoothing of scores to be used as pretests:\\ version based on empirical Bayes + EM}
\begin{document}
\maketitle

\section{Concept summary}

Whenever suitable pretest data are available, the Evaluation Engine will use them to adjust outcome comparisons. Two common limitations of pretest score data are expected to apply here also:
\begin{enumerate}
\item some subjects for whom outcome and treatment information is available will lack pretest scores (\textit{missing data}); and
\item pretest scores are, at best, sums of students' true prior ability and a random disturbance (\textit{measurement error}). \setcounter{saveenum}{\value{enumi}}\end{enumerate}

In  the propensity matching literature, neither of these are usually treated as an important limitations.  However, both issues are ordinarily felt call for special accommodations in analyses comparing intervention and control groups that use regression methods\footnote{E.g., Trochim (2006),``Nonequivalent groups analysis,'' \textit{Research Methods Knowledge Base}, \url{http://www.socialresearchmethods.net/kb/statnegd.php}}. Such methods are more broadly known than propensity score methods, and besides we are likely to use them in a subsidiary role in the Evaluation Engine, to address imbalances that may remain after propensity score matching and to reduce standard errors. Some steps to address measurement error in the pretests and missing data in the pretests may help bolster the credibility of the overall analysis.

This note outlines some measures that will help to address these concerns.  The measures are relatively concrete and easy to understand, can be implemented off-line and in advance, and may be particularly helpful with vertically linked test scores (that are in principle comparable across grades).  The process of creating and fitting these models will produce additional by-products of use to the Engine:
\begin{enumerate} \setcounter{enumi}{\value{saveenum}}
\item characterizations of \textit{growth rates}, namely estimates of the $ \delta_{t} $ coefficient in \eqref{eq:4} that the EE will use to present estimated treatment effects in an interpretable fashion; and
\item ``\textit{school effects}'' characterizing schools' effectiveness relative to other schools in the same state and/or relative to other schools in the same state serving a similar mixture of students, which can be used to inform the matching (and that would not otherwise be available).
\end{enumerate}


\textbf{Assumptions.}  The remainder of the note assumes:\\
\begin{enumerate}
\item With rare exceptions, missingness occurs \textit{only} for assessment measures, not demographic or other background measures.  (This based on Bob F's summary of administrative data from ``unnamed state,'' from 10 Jan 2013 meeting.)
\item (\ldots)
\end{enumerate}
  
\section{Structure of basic smoothing model}
Let the subscript $t$ indicate a grade level for a cohort (so that $t-1$ refers to the same cohort a year earlier, not the next younger cohort at the same time); $s$, a school; $i$, an individual student within the school.  Let $y$ be the test score of interest, $\mathbf{x}$ a collection of covariates (discussed further in \S~\ref{sec:choice-covariates}), partitioned as level~1 or individual level covariates, $\mathbf{x}^{(1)}$, and  level~2 or school and district-level covariates ($\mathbf{x}^{(2)}$). Then the smoothing model is a mixed model, more specifically a three-level hierarchical model, as follows:
\begin{eqnarray}
  \label{eq:gmodlev1}
  Y_{t'si} &=& \pi_{tsi} + (t-t')\delta_{ts} + \epsilon,\, t'=t, t-1 \mbox{ (level 0)};\\
\pi_{tsi} &=& \pi_{ts} + \mathbf{x}^{(1)}\gamma_{t}^{(1)} + \epsilon_{\pi si}, \label{eq:1} \\ 
\delta_{tsi} &=& \delta_{ts} + \mathbf{x}^{(1)} \zeta_{t}^{(1)} + \epsilon_{\delta si} \mbox{ (level 1)} \label{eq:2}; \\
\pi_{ts} &=& \pi_{t} + \mathbf{x}^{(2)}\gamma_{t}^{(2)} + \epsilon_{\pi s}, \label{eq:3} \\ 
\delta_{ts} &=& \delta_{t} + \mathbf{x}^{(2)} \zeta_{t}^{(2)} + \epsilon_{\delta s} \mbox{ (level 2)} \label{eq:4}.
\end{eqnarray}
%where $ \mathcal{I}\{ \cdot \}$ equal 1 if condition $ \cdot$ is true, 0 otherwise.  
Here $\{ (\epsilon_{\pi s}, \epsilon_{\delta s}) : s\}$ are assumed to be drawn from a common, mean-zero multivariate normal distribution, as are $\{ (\epsilon_{\pi sI}, \epsilon_{\delta sI}) : s, i\}$.  (Two different distributions, one common to school-level random effects and another common to individual-level random effects.)  No restriction (other than symmetry and positive-definiteness) is placed on its covariance --- the Stata option \texttt{cov(uns)}, if I'm not mistaken.  In (\ref{eq:gmodlev1}), a prior test is considered a pretest, $Y_{t-1}$, only if it and the current examination $Y_t$ are vertically aligned.  (``Vertical moderation'' or other weak forms of vertical scaling are OK for present purposes.)  Handling of prior tests that are related but not vertically aligned with the current test is discussed below in Section~\ref{sec:joint-vs-sep-domain}.  

Provided there are least two comparable test measures $Y_{tsi}$ and $Y_{(t-1)si}$ for at least some students $i$, this is a straightforward linear growth model \citep[e.g.][ch.6]{raudenbush:bryk:2002}. For the first release of the EE there'll never be more than two consecutive test measures being used as pretests, so the question of whether to model growth as linear or a a polynomial or other nonlinear function of time does not arise.  For now, we'll plan only to use two years' data even when more become available, for simplicity and uniformity of operations. 
% When there are multiple pretests, it's common in the growth modeling literature to replace the linear time contribution  $ (t-t')\delta_{ti}$ with a (random-coefficient) polynomial in $t$; but we'll save the complexity that would add to the model for other things (\S~\ref{sec:mod-elab}, below).  
For cohorts with measurements at time $t$ but not at $t-1$, (\ref{eq:gmodlev1}) is simplified to exclude the slope term ($\delta$) so that (\ref{eq:2}) and (\ref{eq:4}) disappear, leaving a structure still simpler in form than the linear growth model.  The $\delta$ coefficients in (\ref{eq:gmodlev1}) and (\ref{eq:2}) describe growth rates, and we might use the fitted models' characterizations of them to help contextualize intervention effects.

Models of form (\ref{eq:gmodlev1})--(\ref{eq:4}) are not vulnerable to measurement error in test scores because the test scores appear at left of the model equations, in the dependent variable role. Random noise in the ability measure affects only the precision with which the model can be estimated, but does not impede its identifiability. 

Models fitting the template (\ref{eq:gmodlev1})--(\ref{eq:4}) are also relatively robust to missingness: under MCAR, or under suitable MAR assumptions, the model is validly estimable \citep[][ch.6]{raudenbush:bryk:2002}. And the estimated model provides a straightforward basis with which to impute scores for students who are missing them, imputations that are fully informed by whatever other baseline information may be available for those students.  The imputations are simply the  fitted values, for all students and test administrations, found by populating (\ref{eq:gmodlev1})--(\ref{eq:4}) with fitted coefficient values and fitted values of the random effects $\epsilon_{\pi}, \epsilon_{\delta}$.  (Later propensity score adjustments will probably attempt to balance intervention and control in terms of their observed pretest scores \textit{and} in terms of their smoothed pretest scores, as well as a dummy variable for missingness.)

Even if test scores were completely observed, modeling outcomes in terms of these smoothed covariates rather than the observed covariate would be preferable from a measurement-error perspective. The substitution of $\hat{\pi}_{ti}$ for $Y_{ti}$ on the right hand side of a regression equation amounts to calibration, an approach to measurement error in covariates that is particularly effective for generalized linear models \citep[][ch.4]{carroll2006measurement}, including the linear and logistic models that will play important supporting roles in the EE's outcome analyses.

The model template could in principle be elaborated, for instance to permit the level-one coefficients $\gamma_{t}^{(1)}$ and $\zeta_{t}^{(1)}$ to vary randomly across schools; but I expect this will be more than enough complication.  In fact, if fitting this 3-level model turns out to be burdensome, we should consider reverting to a 2-level model grouping school, district and individual predictors into the same equation. 

\section{Aspects of the model to be pinned down through further analysis}
\label{sec:mod-elab}

\subsection{Choice of covariates}
\label{sec:choice-covariates}

In (\ref{eq:1})--(\ref{eq:4}), ``$\mathbf{x}$'' stands for student, school and district variables other than test scores that are ascertained at or before time $t$.  (The significance of these variables' not being test scores is that the model will take them to be free of measurement error.)  But which student, school and administrative variables, arranged in what way? The specifics will be determined by the Michigan team, guided by the following principles:

\begin{enumerate}
\item Better to do model development in Stata from the get-go, if we can. 
\item Our first list of individual-level $x$es will be the \href{https://git.mprinc.com/ee_statistical_component/wikis/balancevars#subgroups}{subgrouping variables}, plus, in grades for which it is available, \texttt{gpacum} (Cumulative GPA.)
\item School-level variables: school mean proportions on each of the individual $x$es just listed.  Plus, \textit{possibly,} a parsimonious list of other important school-level descriptors, to be determined. (For states that do not provide student economic disadvantage variables, at least, we'll include school-level economic disadvantage indicators drawn from the Common Core.)
\item (Should we get the opportunity to consider additional individual-level covariates, we'll draw them first from the ``\href{https://git.mprinc.com/ee_statistical_component/wikis/balancevars#main}{main}'' list of matching variables and then from the ``\href{https://git.mprinc.com/ee_statistical_component/wikis/balancevars#inclusive}{inclusive}'' list.)
\item For computational convenience, we'll develop models on samples from cohorts rather than entire cohorts --- 5K samples, let's say.  To address sampling variability, we'll run similar model selection procedures on replicate subsamples, then take the \textit{union} of the selected model and select down from it, again on replicate subsamples.  (This could in principle go on and on until convergence, but I'll propose to rinse-and-repeat just once.  As long as we're working with 3-level models, important to do a 2-stage sample to ensure sufficient sample size w/in each sampled school.) 
\item Individual variables straightforwardly give rise to school-level variables, as school means of the individual variable.  At first pass we'll use these at the uppermost level, level 3, simulataneously grand-mean centering the variables at level 2.  Carrying the information at the school level of the model when possible should improve our capacity for imputation in the face of partial information. (or will it?  to think about.)
\item For continuous $x$es at the uppermost level of the model hierarchy (ie, school or district variables), let's be flexible and use a spline basis, or at least allow the possibility of a spline expansion if the data permit it.  Perhaps a linear spline basis, for simplicity, of the form $\{(x - q_{p})_{+}: p=0, 1/3, 2/3\}$, where $q_{p}$ is the $p$th quantile.
\item Ditto for continuous $x$es at the middle level, if there is a middle level.  (The main variable of  this nature will be high school GPA.)
\item The ending list of variables will be a superset of those variables (other than test scores) that will be permitted to appear in outcome regressions (that will be used to add power to  matched inferences). (This requirement flows from the needs of measurement error calibration.) So if we should for some reason decide that a specific covariate is important for that model, then by extension the same covariate will have to be retained here as well, even if it does not justify its inclusion by its contributions to the measurement model.
\item As we simplify the models in the sense of removing covariates, we should also check for opportunities to simplying by removing random effects.  (In particular, I'm curious as to whether we'll be able to get away with using fixed rather than random effects for $\delta$, ie to describe growth.)  Covariances should at first be set to "unstructured", but then we should test whether that can be simplified to one of Stata's more structured specifications gives much the same thing.
\end{enumerate}

\subsection{Joint versus separate modeling of scores in different domains}
\label{sec:joint-vs-sep-domain}
The simplest usable operationalization of (\ref{eq:gmodlev1})--(\ref{eq:4}) models each domain score $d$ (Math, Writing, \ldots) separately, taking each pupil's $(\epsilon_{\pi d} , \epsilon_{\delta d})$ for math to be independent of his $(\epsilon_{\pi d} , \epsilon_{\delta d})$ for other domain scores.  But nothing recommends that separation a priori.  Once we've decide on what covariates should go in (to the models for each test score as outcome), we (the Michigan team) will explore possibilities for merging the models.  


The assumption that a student's random effects for different domains are independent of one another is relaxed in the joint model
\begin{eqnarray}
  \label{eq:gmodlev2}
  Y_{dt'i} &=& \pi_{dti} + (t-t')\delta_{dti} + \epsilon,\, t'=t, t-1,\ldots;\\
\pi_{dti} &=& \mathbf{x}_{i}^{(1)}\gamma_{dt}^{(1)} + \epsilon_{\pi d}^{(1)} \mbox{ and}\\ 
\delta_{dti} &=& \mathbf{x}_{i}^{(1)} \zeta_{dt}^{(1)} + \epsilon_{\delta d}^{(1)}, \mbox{ all } s \label{eq:5}
\end{eqnarray}
where $d$ ranges over test domains and $ \mathcal{I}\{ \cdot \}$ equals 1 if condition $ \cdot$ is true, 0 otherwise.  To limit notational clutter, model~(\ref{eq:gmodlev2})--(\ref{eq:5}) assumes each $d$ has pretests, although not all will.  (For some $d$es, $Y_{dti}$s are available only for a single time point $t_0$, not also for $t=t_0-1,\ldots$.)  Tests occurring prior to time $t$, but not vertically aligned with a later examination also occurring prior to $t$, are reconciled with the form of equation (\ref{eq:gmodlev2})  by using a distinct domain index $d$ and, perhaps confusing, the time index $t$.  


The big advantage of this has to do with the model's use in missing data scenarios.  If Johnny gets sick and misses the math test, but not the verbal test the next day, then the merged model permits us to impute him a math score based not only on his covariates but also on the deviation of his verbal test result from its covariate-based prediction.  The added value (of the merged-model prediction as compared to the separate-models prediction) will be determined by the  degree of correlation between reading and math random effects in the fitted version of the merged model.

On the other hand, model merging invokes computational costs. Note that if there are $d$ domains and $n$ students, then the overall merged model will have $ns$ observations contributing to it, and the covariance of the random effects term will be roughly $DT \times DT$, where $D$ is the number of domains and $T$ is the number of time points.  A sufficiently large $D$ will break or bog down whatever software we're using for fitting.   Relatedly, whether to leave the covariance of this expanded collection of random effects totally unstructured or to endow it with block-diagonal or other structure is also an open question.   (I am told that Stata gives the analyst great flexibility about how to structure this covariance.)  So we should only merge models when doing so leads to an appreciable fitted correlation, simplifying the assumed covariance structure however is warranted.


I'd propose that we set priorities (as to which models to merge) based on a combination of missing data considerations and empirical correlations among fitted random effects within the separate models. A specific plan in this direction:
\begin{enumerate}
\item For a given cohort and time $t$, order test scores ($Y_{dt}$s) in terms of how much missing data are observed for them.
\item Starting with the $Y_{dt}$ with the most missingness, examine remaining $Y_{dt'}$s for presence of test scores among students for whom $Y_{dt}$ is missing.  Restrict consideration for merging to those $d'$ for which an appreciable fraction of students $i$ with $Y_{dti}$ missing have $Y_{dt'i}$ present. \label{item:1}
\item For each $d'$  remaining under consideration, calculate empirical correlation between $\hat{\epsilon}_{\pi dt}$ and $\hat{\epsilon}_{\pi d't}$, and (if the models contain random slopes) between $\hat{\epsilon}_{\delta dt}$ and $\hat{\epsilon}_{\delta d't}$.  Merge models for which these correlations are highest. \label{item:2}
\end{enumerate}
 Steps~\ref{item:1} and \ref{item:2} acknowledgedly contain  criteria that are only loosely defined.  We should make these specific as we go.

\section{To-dos when data arrive}
\begin{itemize}
\item Check that for all cohort-times $t$ and tests $d$, the mapping of students to schools is one or many to one, but that each student can be associated to a unique school.  (Needed for calculating $x^{(2)}$s that are  determined from $x^{(1)}$, eg school mean free lunch.)
\item For each of the main student-level variables (ELL, FRSL, Special Ed Status, Sex, race indicators, ??) calculate school and district means, for inclusion as $x^{(2)}$s.
\item Enumerate test administration times $t$.  Choose three for detailed study, one from lower grades, one from middle school and one from high school.
\item Starting w/ lower grade case and working up, fit models with $x$es determined from aforementioned student variables, adding in main additional school and district variables (??) at level 2. (Expect complexity of modeling to increase w/ age, as numbers of pretests increase and missingness pattern becomes increasingly informative.)  Focus first on fitting models of these basic structures, w/o regard to e.g. potential interaction terms.
\item Check to see whether models could be simplified by using fixed rather than random slopes.
\item \ldots
\end{itemize}
\bibliographystyle{asa}
\bibliography{abbrev_long,causalinference,computing,misc} % find these in
% http://stat.lsa.umich.edu/~bbh/texmf/bibtex/bib/

\section*{Appendix: Variables to be used as independent predictors}

\end{document}

