# Evaluation Engine Statistical Component

The Evaluation Engine project is made up of four repositories:

**Evaluation Engine Web UI:**
https://github.com/RTICWDT/evaluation-engine-website

**Evaluation Engine Service Broker:**
https://github.com/RTICWDT/evaluation-engine-service-broker

**Evaluation Engine Console Application:**
https://github.com/RTICWDT/evaluation-engine-console

**Evaluation Engine Statistical Component:**
https://bitbucket.org/benthestatistician/evaluation-engine-stats


This repository corresponds to the statistical component.  The
the Web UI repository's `README.md` gives an overview of these
components' respective roles. 

## Directory Structure

- `docker-images`: We use [Docker](https://www.docker.com/) to manage software
  dependencies and handle app deployment. Under this directory there are several
  subdirectories, each corresponding to a Docker image.
  - `R`: A base R image, pulled from the [rocker project](https://github.com/rocker-org/rocker).
  - `eem`: The actual statistical package itself, which sits in the `eem`
    subdirectory. This image builds on the R image.
  - `app`: This image builds on the `eem` image and supplies the additional
    dependencies required to talk to a Microsoft SQLServer instance.
  - `frontend`: Provides the HTTP server and gearman server to handle incoming
    job requests. This image exposes port 80 for HTTP requests and app instances
    can link for the gearman server.
- `specs`: as the name suggestions, planning documents and descriptions of how
  the sytem works.
- `tests`: despite the name, this directory is mostly used to store certain
  metadata.
- `preprocessing`: Various pre-processing scripts that create summary
  information from the large database.
- `utils`: scripts for starting Docker images and service files for setting up a
  server instance to start the docker containers.

## Requirements

The only required software to install is [Docker](https://www.docker.com/).
Docker will handle downloading and building all additional software.

Most UNIX style environments will include GNU Make, which is useful for running
tasks in the supplied `Makefile`. Strictly speaking, this software is not
necessary, but helps automate repetitive tasks.

## Getting Started

We only support UNIX like environments. If you are on Mac OS X, be sure to
install the developer tools, available from the app store.

You will need to have Docker build the following images (in order) R, frontend, eem, app. The easiest way to do this is use the supplied `Makefile`:

    $ make
    
Alternatively, you can run the commands directly:

	$ docker build -t eestats/r:1.0.0 docker-images/R
	$ docker build -t eestats/frontend:1.0.0 docker-images/frontend
	$ docker build -t eestats/eem:1.0.0 docker-images/eem
	$ docker build -t eestats/app:1.0.0 docker-images/app

Most statistical development occurs in the `docker-images/eem/eem/` folder. When you make a change, rebuild the `eem` image. When you are going to push a new app to your server, also rebuild the `app` image. 

## Server Setup

When you have pushed your images to your server, you will need to start one
`frontend` container and one or more `app` containers. The
`utils/startserver.sh` is a tool you can use to set up this process. For example
if you wish to set up a "development" instance that uses port 8080 for front end
requests,

    $ export EE_ENVIRONMEN=development
    $ export EE_TAG=latest
    $ export EE_PORT=8080
    $ export EE_DB_USER=dbuser
    $ export EE_DB_PASS=dbpass
    $ utils/startserver.sh 3
    
This invocation will start 3 backend workers read to handle requests. 

If you want to have several different instances running, you may want to tag the
docker images to distinguish them.

    $ docker tag eestats/frontend:latest eestats/frontend:production
    $ docker tag eestats/app:latest eestats/app:production
    
This way, if you update the frontend or app images, the production image will
remain stable.

## TODO

- Need to update preprocessing to Docker infrastructure. 
- Need to provide dedicated Docker image for testing.

<!-- vim: set tw=78 spell -->
