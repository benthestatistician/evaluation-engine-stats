##' @param coefs Names of coefs to use
##' @param resp Names of response to use
##' @param data Data frame to use
##' @return A formula.
build_pred_formula<- function(coefs, resp, data) {
  stopifnot(all(coefs %in% names(data)))
  stopifnot(resp %in% names(data))

  # remove a few coefs if they haven't been removed already
  coefs <- coefs[!(coefs %in% c("seqno", "study_id", "donotuse","Id"))]

  # find and remove any coefs which are constant, or which are non-NA for under 1% of the sample.
  usefulcoefs <- apply(data[,coefs], 2, function(x) {
    length(na.omit(unique(x))) > 1 & sum(!is.na(x)) > .01*nrow(data)
    })

  as.formula(paste(resp, paste(coefs[usefulcoefs], collapse='+'), sep="~"))
}





if (!exists("df12")) {
  df12 <- dbfetch("SELECT * FROM nm2012_drv WHERE gradelevel = 12")
}

# create outcome. diploma_type_p0 is 1,2 or 4 for various types of diploma, 0 for none.
df12$hs_graduate<- 1*(df12$diploma_type_p0 > 0)

df12 <- df12[df12$donotuse == 0,]

# drop off all potential responses
coefs <- names(df12)[c(1:97, 185:203)]

expect_error(pred_model(5,5,5))
expect_error(pred_model(coefs, "fjdklsfjds", data=df12))

o <- build_pred_formula(coefs, "hs_graduate", data=df12)
print(sum(complete.cases(df12[,all.vars(o)])))
expect_true(inherits(o, "formula"))

library(arm)
mod <- bayesglm(o, data=df12, family=binomial)

set.seed(12412)
s <- sample(1:nrow(df12), 500)
df12sub <- df12[s,]
df12rem <- df12[-s,]
length(unique(df12$schoolname_enroll))    # 216 total
length(unique(df12sub$schoolname_enroll)) # 139 found in here
length(unique(df12rem$schoolname_enroll)) # remainder has all 216

o2 <- build_pred_formula(coefs[12:97], "hs_graduate", data=df12sub)
dim(model.matrix(o, data=df12))
mod2 <- bayesglm(o2, data=df12sub, family=binomial);print("done")
