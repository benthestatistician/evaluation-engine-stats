df <- dbfetch("SELECT * FROM nm2012_drv WHERE gradelevel = 12")

# drop any donotuse students
df <- df[!df$donotuse,]

# create outcome. diploma_type_p0 is 1,2 or 4 for various types of diploma, 0 for none.
df$hs_graduate<- 1*(df$diploma_type_p0 > 0)

# drop off student id #, and all responses
coefs <- names(df)[c(3:97, 185:203)]

# remove any coefs which are constant or unique per observation
usefulcoefs <- apply(df[,coefs], 2, function(x) {
  lu <- length(na.omit(unique(x)))
  lu < nrow(df) & lu > 1
  })

coefs <- coefs[usefulcoefs]

# formally make these factors just to avoid some warnings
df$districtid_state_enroll <- as.factor(df$districtid_state_enroll)
df$districtname_enroll <- as.factor(df$districtname_enroll)
df$schoolid_nces_enroll <- as.factor(df$schoolid_nces_enroll)
df$schoolid_state_enroll <- as.factor(df$schoolid_state_enroll)
df$schoolname_enroll <- as.factor(df$schoolname_enroll)

# some manual removals
coefs <- coefs[coefs != "lep_midd"] # only 6 non-NA values.
coefs <- coefs[coefs != "rfep_midd"] # only 3 non-NA values.
coefs <- coefs[coefs != "specialed_midd"] # only 5 non-NA values.
coefs <- coefs[coefs != "writng_smhsch_m2"] # only 5 non-NA values.
coefs <- coefs[coefs != "writng_smhstu_m2"] # only 5 non-NA values.

print("ready for model fitting")

library(arm)

form <- as.formula(paste("hs_graduate", paste(coefs, collapse='+'), sep='~'))
fullmod <- bayesglm(form, data=df, family=binomial);print("done")
print(fullmod$aic)
