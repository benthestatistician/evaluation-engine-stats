#!/bin/sh


TEMP=`getopt -o gv --long development,staging,production,test -- "$@"`
if test $? != 0
then
    echo "Usage: rerun.sh [-gv] [--development | --staging | --production | --test] JOBGUID"
    echo "  -g Submit via Gearman"
    echo "  -v Visit the /srv/stats directory corresponding to the branch before running the job command"
    echo "  --development Use the development DB (default branch)"
    echo "  --staging Use the staging branch"
    echo "  --production Use the production branch"
    echo "  --test Use the testing db (fake data), -v is silently ignored for this option."
    exit 1
fi

eval set -- "$TEMP"
EE_ENVIRONMENT=development
GEARMAN=0
VISIT=0
VISIT_DIR=/srv/stats/master

while true ; do
    case "$1" in
        -g) GEARMAN=1; shift ;;
        -v) VISIT=1; shift ;;
        --development) EE_ENVRIONMENT=development ; VISIT_DIR=/srv/stats/master ; shift ;;
        --staging) EE_ENVRIONMENT=staging ; VISIT_DIR=/srv/stats/staging ; shift ;;
        --production) EE_ENVRIONMENT=production ; VISIT_DIR=/srv/stat/production ; shift ;;
        --test) EE_ENVIRONMENT=test ; VISIT_DIR=. ; shift ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

if [ $VISIT -eq 1 ]
then
    cd $VISIT_DIR
fi

if [ $GEARMAN -eq 1 ]
then
    echo "Gearman submission not implemented yet"
fi

export R_LIBS=.local
export EE_ENVIRONMENT=$EE_ENVIRONMENT
R --quiet --vanilla -e "library(eem); processJob('$1')"

