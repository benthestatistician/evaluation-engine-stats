TAG=1.0.0

all: docker-r docker-eem docker-frontend docker-app

docker-r:
	docker build -t eestats/r:$(TAG) docker-images/R

docker-eem: docker-r
	docker build -t eestats/eem:$(TAG) docker-images/eem

docker-frontend:
	docker build -t eestats/frontend:$(TAG) docker-images/frontend

docker-app: docker-eem
	docker build -t eestats/app:$(TAG) docker-images/app

docker-test: docker-eem
	docker build -t eestats/test docker-images/test

test: docker-test
	docker run --rm eestats/test
