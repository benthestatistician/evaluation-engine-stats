#!/bin/bash

set -e

echo "Starting Gearman Server..."
/usr/sbin/gearmand -d
echo "Gearman started.\nStarting apache server..."
/usr/local/bin/apache2-foreground
