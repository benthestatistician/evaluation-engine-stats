<?php

/*
   A collection of functions to create, update, and modify jobs sent from the 
   web front end to the statistical back end.
 */

// To be incremented as the API changes. No particular need at this time,
// but it is good to be forward thinking
define("JOB_API_VERSION", 1);

function valid_api() {
  return(array(
    'id'          => array('filter'  => FILTER_SANITIZE_STRING),
    'api_version' => array('filter'  => FILTER_VALIDATE_INT, 
                           'flags'   => FILTER_REQUIRE_SCALAR,
                           'options' => array('min_range' => 1, 'max_range' => JOB_API_VERSION))));
}

function job_config() {
  $setup = parse_ini_file(dirname(__FILE__) . "/config.ini");
  if (!is_dir($setup["fileroot"] . "/jobs")) {
    mkdir($setup["fileroot"] . "/jobs");
  }
  return($setup);
}


// @param $arr An array, such as $_GET or $_POST that includes the specification
//  of the job to be created. 
// @return An new job object, which contains the ID of the job and its current status.
function api_array_to_job($arr) {

  // filter the input for safety and drop non-API stuff
  $job = array_intersect_key(filter_var_array($arr, valid_api()), valid_api());

  foreach (array_keys(valid_api()) as $k) {
    if (!$job[$k]) {
      throw new Exception("Missing/malformed API item: " . $k);
    }
  }

  return($job);
}

   
// @param $job A job array, probably created by api_array_to_job
// @return An id indicating the id of the job
function start_job($job, $queue) {
  // safety first!
  $job = api_array_to_job($job);
  
  # create our client object
  $gmclient= new GearmanClient();
  
  # add the default server (localhost)
  $gmclient->addServer();
  
  # run reverse client in the background
  $job_handle = $gmclient->doBackground("match_" . $queue, json_encode($job) . "\n");
  
  // if ($gmclient->returnCode() != GEARMAN_SUCCESS)
  // {
  //  TODO: handle, probably with an exception  
  // }
  
  return($job_handle);
}
