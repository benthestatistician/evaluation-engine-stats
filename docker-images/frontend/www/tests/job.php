<?php
require_once "PHPUnit/Autoload.php";
require_once(dirname(__FILE__) . "/../job.php");

class JobTest extends PHPUnit_Framework_TestCase {

  protected $mock_request;
 
  protected function setUp()
  {
      $this->mock_request = array("id" => 1234,
      "api_version" => 1);
  }

  public function testBasicInputChecks() {
    
    $this->assertEquals($this->mock_request, api_array_to_job($this->mock_request));

    // unused keys should be stripped out
    $mock_request2 = $this->mock_request;
    $mock_request2["foo"] = "bar";

    $this->assertEquals($this->mock_request, api_array_to_job($mock_request2));
  }

  /**
    * @expectedException exception
    */
  public function testMissingJobID() {
    
    $bad_treatment_no_job= array(
      "api_version" => 1);

    $res = api_array_to_job($bad_treatment_no_treated);
  }

  // public function testStartJob() {

  //   $job1 = start_job($this->mock_request);
  //   
  //   $this->assertTrue(is_dir(job_config()["fileroot"] . "/jobs/" . $job1));

  //   // two jobs should have different ID tags
  //   $job2 = start_job($this->mock_request);

  //   $this->assertNotEquals($job1, $job2);
  // }

}

?>
