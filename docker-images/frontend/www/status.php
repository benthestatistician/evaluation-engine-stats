<?php

# create our client object
$gmclient = new GearmanClient();

# add the default server (localhost)
$gmclient->addServer();

$jobid = filter_input(INPUT_GET, "id", FILTER_SANITIZE_STRING);

if ($gmclient->jobStatus($jobid)[0]) {
  echo("Job running");
} else {
  echo("Job stopped or job unknown");
}

?>
