<?php

error_reporting(E_ALL | E_STRICT);

// See file for specification of the job API
require_once(dirname(__FILE__) . "/job.php");

try {

  $api_request = api_array_to_job($_REQUEST);
  $queue = getenv("EE_ENVIRONMENT");

  $handle = start_job($api_request, $queue);
  
  $reply = array('gearman_handle' => $handle, 'queue' => $queue);

  header('Content-Type:text/plain');
  echo(json_encode($reply));

} catch (Exception $e) {
  print($e->getMessage());
}
