#!/bin/bash

BASE=/srv/stats

ID=$(R --slave -f parseJSON.R)

DIR=${BASE}/${ID}_$(date +%Y%m%d%H%M%S)
mkdir -p $DIR
cd $DIR

echo "Staring job ${ID} on $(date)" > log.txt

/usr/bin/time -v --  R -e "source('/var/eem/input.R'); processJob('$ID')" &>> log.txt

## an error code of 1 should be picked up internally
## higher level error codes need reporting as the are from OS level stuff
## (out of mem, etc.)
if [ $? -ge 2 ]; then
    echo "OS level error detected" >> log.txt
    R -e "source('/var/eem/reportError.R'); reportError('$ID')"
fi
