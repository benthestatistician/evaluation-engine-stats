#' Produce the analysis results from a problem specification and a match.
#'
#' @param job A problem specification (a list of lists)
#' @param design A design object (including covariates, block, and clustering, if any)
#' @return A list of length equal to the number of outcomes analyzed.
#' @export
analyzeMatch <- function(job, design) {

  design <- design[!is.na(design@Block)]
  observedGrades <- unique(design@Covariates$gradelevel)

  # This ensures covs and match are in the same order based on ID from outcomes
  outcomes <- getOutcomes(job, design)

  subgroup.names <- job@SubgroupAnalyses

  isBadColumn <- function(i) {
    i2 <- i[!is.na(i)]
    length(i2) == 0 || (all(i2 == i2[1]))
  }

  # OutcomeTable has the following columns in the following order
  stopifnot(colnames(job@OutcomeTable) == c("front", "main", "prof", "year"))

  doAnalysis <- function(r) {
    tryCatch({
      main <- outcomes[, r[2]]
      main.meta <- job@StudentMeta[[r[2]]]

      if (isBadColumn(main)) {
        msg <- paste0("Cannot perform outcome analysis on", r[1], "in", r[4], "as there is insufficient variation")
        logger(msg, name = paste0("analysis_", r[2]))
        return(list(error = msg))
      }

      hasGoodProf <- !is.na(r[3]) && !isBadColumn(outcomes[, r[3]])
      prof <- NULL
      if (hasGoodProf) {
        prof <- outcomes[, r[3]]
      }

      logger("Analyzing outcome:", r[2], name = "analysis")

      # load the statewide info in order to standardize and combine multigrade interventions
      files <- system.file(file.path("data", paste0(job@State, job@TreatmentYear, "_ecdfs_", observedGrades, "_", r[2], ".Rdata")), package="eem")
      if (all(nchar(files) > 0)) {
        n.means.sds <- lapply(files, function(f) {
          load(f)
          return(save.ecdf.and.means)
        })

        names(n.means.sds) <- observedGrades
      } else {
        logger("Could not find all normalizing data for all grades for ", r[2], ". Results will use raw scores, which may not be suitable for all types of outcomes.", name = "analysis")
        n.means.sds <- NULL
      }

      if (!is.null(main.meta$is_ordinal_outcome) && !is.na(main.meta$is_ordinal_outcome)) {
        isOrd <- main.meta$is_ordinal_outcome == "1"
      } else {
        isOrd <- FALSE
        ordLevels <- NULL
      }

      ordLevels <- NULL
      if (isOrd) {
        ordLevels <- main.meta$categories
      }

      performAnalysis(r[2],
                      main,
                      design,
                      subgroup.names,
                      subgroup.names,
                      proficiency = prof,
                      n.means.sds = n.means.sds,
                      isOrd = isOrd,
                      ordLevels = ordLevels)
      },
      error = function(e) {
          logger("Internal error encountered for outcome ", r[2], name = "user")
          logger("Error during outcome ", r[2], ": ", e, name = "analysis")
          list(error = "Internal Error")})
  }

  res <- apply(job@OutcomeTable, 1, doAnalysis)
  names(res) <- job@OutcomeTable$main

  # for each outcome of the same type (e.g. math), we do a simple multiplicity correction across years (subgroups are corrected within years)
  outcomeTypes <- as.factor(job@OutcomeTable$front)
  # note: if we are implementing issue #1340, this would probably be the place to group variables that need to be adjusted together.

  hadErrors <- sapply(res, function(r) { !is.null(r$error) })

  for (l in levels(outcomeTypes)) {
    idx <- which(l == outcomeTypes & !hadErrors)
    if (length(idx) > 0) {
      # #19 - P-values are one-sided; we generate both one-sided and
      # adjust with all combined, returning the adjusted p-value
      # associated with the smaller unadjusted p in each pair.
      ps <- sapply(res[idx], function(r) { r$table[1, "p.value"] })
      ps <- rbind(ps, 1 - ps) # create matrix with each column
                              # representing the pair of 1-sided p-vals.
      ps <- apply(ps, 2, sort) # sort each column

      # Apply adjustment over all entries.
      ps <- p.adjust(as.vector(t(ps)), method = "holm")
      for (j in seq_along(idx)) {
        # This will extract the first `length(idx)` entries of `ps`,
        # which is of total length `2*length(idx)`, where the second
        # half are the larger directional p-values.
        i <- idx[j]
        res[[i]]$table[1, "p.value"] <- ps[j]
      }
    }
  }

  return(res)
}

#' Generate the log name for a given outcome
#'
#' @param oname
#' @return A string that can be used to add/lookup entries in the LOGS table (using $name field).
outcomeLogName <- function(oname) {
  paste0("analyze_", oname)
}

#' Analyze an outcome for a main treatment and subgroup effects. If subgroups
#' are small, those comparisons will not be include.
#'
#' Subgroups are listed in the database metadata file. If the subgroups exist
#' in the "covariates" argument, they will be used (under certain minimum
#' sample size conditions [not yet specified])
#'
#' This function manages running the permutational analyses
#'
#' @param outcome_name The name of the outcome to be used in logging.
#' @param outcome The outcome to be analyzed. The exact analysis for this
#' outcome will depend on the data type (e.g. numeric, categorical, etc.)
#' @param design The design object with covariates, blocking, cluster, and treatment info
#' @param all.subgroups A vector of the names of all of the subgroups.
#' @param requested.subgroups The subset of all.subgroups that user requested.
#' @param minGroupSize How small are groups allowed to be before we drop them from subgroup analysis? Default is 10
#' @param omin The minimum theoretical value of the outcome. If omitted, inferred from the outcome vector.
#' @param omax The maximum theoretical value of the outcome. If omitted, inferred from the outcome vector.
#' @param proficiency Optional, proficiency scores for outcome.
#' @param outcome_name The name of the outcome being analyzed.
#' @param n.means.sds For each grade, a list with elements n, mean, and sd
#' @param isOrd Is the variable ordinal? If so we need to return additional per-grade info.
#' @param ordLevels A vector of variable labels that will be used in the ord.table. Null if this is not a ordinal outcome
#' @return A list with two elements: (1) `table`: A table with rows equal to the number of tests (one row for the
#' main effect and one row for each subgroup contrast level). Columns are:
#' variable name, the upper level, lower level, treatment effect, p-value for the
#' treatment effect. (2) `permutation`: was the permutation based analysis used for the p-values?
#' (FALSE indicates model based analysis)
#' @export
performAnalysis <- function(outcome_name,
                            outcome,
                            design,
                            all.subgroups,
                            requested.subgroups,
                            minGroupSize = 10,
                            omin = min(outcome, na.rm=TRUE),
                            omax = max(outcome, na.rm=TRUE),
                            proficiency = NULL,
                            n.means.sds = NULL,
                            isOrd = FALSE,
                            ordLevels = NULL
                            ) {

  # a little defensive programming:
  if (is.null(isOrd)) { isOrd <- FALSE }

  # gradelevel is not a choice (yet), but we include it as a subgroup
  all.subgroups <- c(all.subgroups, "gradelevel")

  olog <- function(...) {
    logger(..., name = )
  }


  # first, drop units for which there is no outcome
  missing.outcome <- is.na(outcome)
  olog("Of the ", length(design@Treatment), " total units, ", sum(missing.outcome), " are missing an outcome measure.")

  # bind up all the variables so that we can do operations on all of them
  d <- data.frame(outcome = outcome, match = design@Block, treatment = design@Treatment, design@Covariates)
  if (!is.null(proficiency)) {
    d <- data.frame(d, proficiency = proficiency)
  }

  # find units who are now in 0:k or k:0 due to missingness
  excess <- removeExcess(d$outcome, d$treatment, d$match)

  # compute attrition rates at this point, before we cull missing outcomes
  tmp <- as.numeric(!excess)
  attrition.overall <- perGradeAverageHelper(tmp, d$match, d$treatment)[1:2]
  attrition.grades  <- perGradeAverages(tmp, d$match, d$treatment, as.factor(d$gradelevel))[, 1:2, drop = FALSE]
  names(attrition.overall) <- colnames(attrition.grades) <- c("attrition: treated", "attrition: control")

  # drop NA units or units in matched sets with entirely missing treatment or control subjects
  d <- d[excess, , drop = FALSE]

  olog("After dropping students in groups missing either all treated or control units, there are ",
          sum(d$treatment), " intervention students and ", sum(!d$treatment), " control students remaining")

  d$match <- d$match[, drop = TRUE] # This drops any empty matched sets (likely had all treatments/controls removed
                                    # via missing.outcome, and remainder removed via excess)

  missing.subgroups <- requested.subgroups[!(requested.subgroups %in% colnames(design@Covariates))]

  if (length(missing.subgroups) > 0) {
    olog("Requested subgroup(s) ", paste(missing.subgroups, collapse = ", "), " not found")
    requested.subgroups <- requested.subgroups[requested.subgroups %in% colnames(design@Covariates)]
  }

  # This is really a check on non-NA outcomes because we drop missing outcomes above
  if (sum(d$treatment) < minGroupSize) {
    msg <- paste("Fewer than", minGroupSize ,"non-NA outcomes in intervention group")
    olog(msg)
    return(list(error = msg))
  }

  for (sgrp in all.subgroups) {

    sgrpvar <- as.factor(d[, sgrp])

    tbl <- table(d$treatment, sgrpvar)
    tmp <- tbl['TRUE',] < minGroupSize
    badlvls <- names(tmp)[as.logical(tmp)]
    sgrpvar[sgrpvar %in% badlvls] <- NA

    d[, sgrp] <- as.factor(as.character(sgrpvar)) # drops unused factor levels
  }

  # gradelevel is always included as a subgroup
  # if we drop grades as being being bad, then drop those units from the analysis
  # this is probably an edge case that shouldn't come up because we should drop the grade earlier, but in case we drop based on missing outcomes, etc. this will pick it up.
  if (all(is.na(d$gradelevel)))
      {
          olog(paste0("Too few non-missing values of ", outcome_name,
                      " to present comparisons on this variable."))
      }
  d <- d[!is.na(d$gradelevel), ]
  d$match <- d$match[, drop = T]
  attrition.grades <- attrition.grades[sort(unique(d$gradelevel)), , drop = FALSE]

  validSubgroups <- sapply(all.subgroups, function(sgrp) {

    if (nlevels(d[, sgrp]) < 2) {
      return(NA)
    }

    return(sgrp)
  })

  validSubgroups      <- validSubgroups[!is.na(validSubgroups)]
  all.subgroups       <- all.subgroups[all.subgroups %in% validSubgroups]
  requested.subgroups <- requested.subgroups[requested.subgroups %in% validSubgroups]

  # Create covariance adjusted outcome if possible
  trim <- 0.05
  growth <- getCovarianceAdjusted(outcome_name, d, trim = trim)

  if (!is.null(growth)) {
    olog("Used growth model adjusted outcome.")
    stopifnot(length(growth) == length(d$outcome))
    d$growth <- growth
    trim <- 0
  } else {
    olog("Growth model outcomes unavailable. Used raw outcomes.")
    d$growth <- d$outcome
  }

  type <- "NATURAL"
  normalized.outcome <- d$outcome
  # normalize outcomes based on statewide means and sds, per-grade
  # we don't replace the 0-1 outcome with the ECDF values
  if (!is.null(n.means.sds) && outcome_name != "status_inferred_p0") {
    if (isOrd) {
      type <- "ORDINAL"
      for (grade in unique(d$gradelevel)) {
        idx <- d$gradelevel == grade
        normalized.outcome[idx] <- n.means.sds[[grade]]$ecdf(normalized.outcome[idx])
      }
    } else {
      type <- "NORMALIZED"
      tmp <- sapply(n.means.sds, function(i) { c(n = i$n, mean = i$mean, sd = i$sd) })
      normalized.outcome <- (d$outcome - tmp["mean", d[, "gradelevel"]]) / tmp["sd", d[, "gradelevel"]]
    }
  }

  requested.subgroup.data <- d[, requested.subgroups, drop = F]
  permsa <- permutationAnalysis(olog, d$growth, d$match, d$treatment, requested.subgroup.data, trim)
  modelb <- modelBasedAnalysis(olog, normalized.outcome, d$match, d$treatment, d[, all.subgroups, drop = F], requested.subgroups)


  # lets do a little sanity checking here and make sure that permsa and modelb have return values of the same size
  stopifnot(all(length(permsa$stats) == length(modelb$stats), dim(permsa$tcov) == dim(modelb$tcov)))

  # prepare a table that has a column for the variable names
  # and a column for the levels

  vars <- c("Overall average", rep(colnames(requested.subgroup.data), sapply(requested.subgroup.data, nlevels)))
  level.labels <- c("---", unlist(sapply(requested.subgroup.data, levels)))

  # mAT() returns a list with the table of estimates and pvalues; and a logical for which set of p-values was used.
  tbl.used <- makeAnalysisTable(olog, permsa, modelb)

  tmp <- tbl.used$table
  rownames(tmp) <- NULL # will give errors when trying to combine with the vars and labels, later

  display.table <- data.frame(vars, level.labels, tmp)
  colnames(display.table) <- c("variable", "level", "estimate", "p.value")
  rownames(display.table) <- NULL # they look ugly
  # now compute the counts of treated and control units per subgroup
  # we use a for-loop because lapply was giviing inconsistent results when there was only 1 subgroup
  k <- dim(requested.subgroup.data)[2]
  if (k > 0) {
    nmake <- function(indicator) {
      ns <- c(sum(indicator))
      for (i in 1:k) {
        ns <- c(ns, table(requested.subgroup.data[, i][indicator]))
      }
      return(ns)
    }
    n0 <- nmake(!d$treatment)
    n1 <- nmake(d$treatment)
    ns <- matrix(c(n1,n0), ncol = 2)
    colnames(ns) <- c("treatment", "control")
    display.table <- cbind(display.table, ns)
  }

  # Computing the "averages" of the treated and control units based on matching weights

  per.grade.averages <- as.data.frame(perGradeAverages(d$outcome,  d$match, d$treatment, d$gradelevel))
  overall.average    <- perGradeAverageHelper(d$outcome, d$match, d$treatment)
  names(overall.average) <- c("mean: treated", "mean: control", "sd: treated", "sd: control")


  sensitivity <- sensitivityAnalysis(d$match, d$treatment, permsa$xbal)

  if (!is.null(proficiency)) {
    # make a copy of d that we'll destructively update
    dp <- d

    pExcess <- removeExcess(dp$proficiency, dp$treatment, dp$match)
    dp <- dp[pExcess, ]

    dp$match <- dp$match[, drop = TRUE]

    tmp <- perGradeAverages(dp$proficiency, dp$match, dp$treatment, dp$gradelevel)
    tmp <- tmp[, c(1,2), drop = FALSE]
    colnames(tmp) <- c("proficiency: treated", "proficiency: control")
    per.grade.averages <- cbind(per.grade.averages, tmp)

    tmp <- perGradeAverageHelper(dp$proficiency, dp$match, dp$treatment)
    tmp <- tmp[1:2]
    names(tmp) <- c("proficiency: treated", "proficiency: control")
    overall.average <- c(overall.average, tmp)

    prof_stats <- modelBasedAnalysis(olog, dp$proficiency, dp$match, dp$treatment, dp[, all.subgroups, drop = F], requested.subgroups, calcCov = FALSE)
    display.table$proficiency <- prof_stats$stats
  } else {
    per.grade.averages <- cbind(per.grade.averages, "proficiency: treated" = NA, "proficiency: control" = NA)
  }

  # ordingal outcomes get an extra set of columns, two per level of the outcome giving the treated and control percentages
  if (isOrd) {
    lvls <- sort(unique(d$outcome))

    tmp <- ordLevels$category_label[match(lvls, ordLevels$category_value)]
    labels <- as.vector(sapply(tmp, function(lvl) { paste0(lvl, ": ", c("treated", "control")) }))

    overall <- as.vector(sapply(lvls, function(l) { perGradeAverageHelper(as.numeric(d$outcome == l), d$match, d$treatment)[1:2] }))
    pergrade <- do.call(cbind, lapply(lvls, function(lvl) {
      x <- perGradeAverages(as.numeric(d$outcome == lvl), d$match, d$treatment, d$gradelevel)[, 1:2, drop = FALSE]
      return(x)
    }))
    colnames(pergrade) <- labels

    ord.table <- rbind(overall, pergrade)
    ord.table <- data.frame("Grade" = rownames(ord.table), as.data.frame(ord.table), stringsAsFactors = FALSE, check.names = FALSE)
    ord.table[1,1] <- "---"
  } else {
    ord.table <- NULL
  }

  # attach attrition numbers
  overall.average <- c(overall.average, attrition.overall)
  per.grade.averages <- cbind(per.grade.averages, attrition.grades)

  return(list(table              = display.table,
              permutation        = tbl.used$perm,
              overall.average    = overall.average,
              per.grade.averages = per.grade.averages,
              ord.table          = ord.table,
              range              = c(omin, omax),
              sensitivity        = sensitivity,
              permsa             = permsa,
              modelb             = modelb,
              type               = type))
}

#' Removes any 0:k or k:0 matches that would exist after NA removal
#'
#' @param outcome An outcome, possibily with NAs
#' @param treatment A treatment vector
#' @param match A matching factor
#' @return Which entries should remain (ie. not in 0:k or k:0)
removeExcess <- function(outcome, treatment, match) {
  missing <- is.na(outcome)

  tmatches <- match[as.logical(treatment) & !missing]
  cmatches <- match[as.logical(1-treatment) & !missing]

  excess.controls <- cmatches[!cmatches %in% tmatches]
  excess.treatments <- tmatches[!tmatches %in% cmatches]

  excess <- missing | match %in% excess.controls | match %in% excess.treatments

  return(!excess)
}

#' Computes per-grade averages, weighted by matched sets, on the raw outcome (not normalized)
#'
#' All parameters are vectors of the same length
#' @param outcome The outcome
#' @param match A factor indicating matched set grouping.
#' @param treatment Treatment indicator vector
#' @param grade A factor indicating gradelevel. All matches should have the same grade.
#' @return A two column table. Rows are grade. Column one is treated average. Column two is control average.
#' @export
perGradeAverages <- function(outcome, match, treatment, grade) {

  ### this how we used to compute averages. keeping around as a record
  ### if (min(outcome) >= 0 && max(outcome) <= 1) {
  ###   averages <- round(c(treated = toutcome, control = max(0, min(1, toutcome - display.table[1,3]))), 3)
  ### } else {
  ###   averages <- round(c(treated = toutcome, control = toutcome - display.table[1,3]), 1)
  ### }

  ### if (!is.null(proficiency)) {
  ###   averages <- c(averages, treated.proficiency = ptoutcome, control.proficiency = ptoutcome - display.table[1,5])
  ### }

  x <- t(sapply(levels(grade), function(g) {
    idx <- grade == g
    perGradeAverageHelper(outcome[idx], match[idx, drop = TRUE], treatment[idx])
  }))

  colnames(x) <- c("mean: treated", "mean: control", "sd: treated", "sd: control")

  return(x)
}

# does the per-grade averages and sds
perGradeAverageHelper <- function(outcome, match, treatment) {

  # convert to a sparse representation
  mcr <- factorToCSR(match, treatment)

  # it codes the treated as positive values and control as negative
  # so we set these to zero to get the treated and controls totals
  mc1 <- mcr
  mc1[mc1 < 0] <- 0

  mc0 <- mcr
  mc0[mc0 > 0] <- 0

  # generate the (weighted) outcomes for just the treated
  # and just the controls in each matched set
  y1 <- mc1 %*% outcome
  y0 <- -1 * (mc0 %*% outcome) # the controls are id'd with -1, which generates negative totals

  dim(y1) <- dim(y0) <- NULL

  mean1 <- mean(y1)
  mean0 <- mean(y0)

  sd1 <- sd(y1)
  sd0 <- sd(y0)

  return(c(mean1, mean0, sd1, sd0))
}

##' Helper function for permAnalysis
##'
##' @param x Vector to be trimmed.
##' @param trim Trim percent in [0,1)
##' @param ... Additional args to pass to quantile, mostly useful for testing.
##' @return Trimmed version of x.
#' @export
topandbottomcoder <- function(x, trim = 0.05, ...) {
  if ((trim < 0) | (trim >= 1))
    stop("Trim needs to be with [0,1)")

  lim <- quantile(abs(x), 1 - trim, na.rm = TRUE, ...)

  if (lim == 0) lim <- min(abs(x)[x != 0], na.rm = TRUE)

  x[x < -lim] <- -lim
  x[x >  lim] <-  lim

  return(x)
}

#' Perform the permutation based analysis.
#'
#' @param outlog A logging function tied to this outcome.
#' @param outcome A vector of the outcome (perhaps pre-processed)
#' @param match The result of running optmatch on the covariates
#' @param treatment A treatment indicator for the all the units
#' @param subgroup A data.frame of subgrouping variables that has been cleaned of bad variables or bad levels
#' @param trim The trim arguement to topandbottomcoder
#' @param ... Arguments to pass to topandbottomcoder.
#' @return A list with the following elements:
#'  - stats: A vector of test statistics (or estimates) with the following names: main, s1.l1, s1.l2, ...
#'  - tcov: The covariance matrix of the test statistics
#'  - alternative: The direction of the alternative you wish to test: "greater", "less", or "two.sided"
#' @export
#' @import RItools
permutationAnalysis <- function(outlog, outcome, match, treatment, subgroup, trim = 0.05, ...) {

  # as per issue #757, we start by mean centering the outcome to give more consistency with the model based outcomes:
  # use the `trim` argument to `mean`. the topandbottomcoder function is 2x that to mean
  outcome <- outcome - mean(outcome, na.rm = T, trim = trim/2) # don't expect any NAs, but being defensive

  # balanceTest will either list-wise delete or will impute the NA values and add a .NA column
  # neither of these is what we want. We just want NAs to be coded as zero in all dummy columns
  # so we have to make our own model matrix and then operate on that
  oldopt <- options()
  options(na.action = "na.pass")

  if (dim(subgroup)[2] > 0) {
    fmla <- as.formula(paste("z ~ y + strata(match) - 1 + ", paste("y:", colnames(subgroup), sep = "", collapse = "+")))
    data <- data.frame(z = treatment,
                       y = outcome,
                       match = match,
                       subgroup)
  } else {
    fmla <- z ~ y + strata(match) - 1
    data <- data.frame(z = treatment,
                       y = outcome,
                       match = match)
  }

  xb <- balanceTest(fmla,
                 data = data,
                 report = "all",
                 post.alignment.transform = function(x) { topandbottomcoder(x, trim = trim, ...) },
                 include.NA.flags = FALSE)

  # Reset options
  options(oldopt)

  xbp <- processBalanceTest(xb)


  result <- list(stats = xbp$vars,
                 tcov  = xbp$tcov,
                 alternative = "greater",
                 xbal  = xb,  # this will be used in the sensitivity analysis
                 df = NULL) # this instructs `glht` to use the multivariate normal, instead of t-distrib
}

#' Perform the model based analysis.
#'
#' @param outlog A logging function that is tied to the particular outcome.
#' @param outcome A vector of the outcome (perhaps pre-processed)
#' @param match The result of running optmatch on the covariates
#' @param treatment A treatment indicator for the all the units
#' @param subgroupdata A data.frame of subgrouping variables that has been cleaned of bad variables or bad levels
#' @param requested.subgroups Only the subgroups the user cares about.
#' @param calcCov Should the covariance be returned?
#' @return A list with the following elements:
#'  - stats: A vector of test statistics (or estimates) with the following names: main, s1.l1, s1.l2, ...
#'  - tcov: The covariance matrix of the test statistics
#'  - alternative: The direction of the alternative you wish to test: "greater", "less", or "two.sided"
#'  - df: The degrees of freedom of this model.
#' @export
#' @import Matrix
#' @import sandwich
modelBasedAnalysis <- function(outlog, outcome, match, treatment, subgroupdata = data.frame(), requested.subgroups = c(), calcCov=TRUE) {

  treatment <- as.numeric(treatment)
  # we shouldn't have any missing data at this point, but in case we do (seems to pop up in the test data), we just drop it entirely

  if (dim(subgroupdata)[2] == 0) {
    data.full <- na.omit(data.frame(z = treatment, y = outcome, m = match))
    subgrpFmla <- "1"
  } else {
    data.full <- na.omit(cbind(z = treatment, y = outcome, m = match, subgroupdata))
    subgrpFmla <- paste(colnames(subgroupdata), collapse = "+")
  }

  main.effect <- mlm(as.formula(paste("y ~ ", subgrpFmla)), data = data.full, match = data.full$m, treatment = data.full$z)

  # result should be a list with "stats", "tcov", "alternative"
  result <- list(alternative = "greater",
                 stats = coef(main.effect)["(Treatment)"])

  Amatrix <- bread(main.effect)
  Bmatrix <- meat(main.effect)
  estfuns <- estfun(main.effect)
  df <- Inf

  if (length(requested.subgroups) > 0) {

    # we include the main effects of all subgroups, not just the requested groups
    subgroupvars <- paste(colnames(subgroupdata), sep = "", collapse = "+")

    for (g in requested.subgroups) {

      groupdummies <- model.matrix(as.formula(paste("~", g, "-1")), data.full)
      zg <- data.full$z * groupdummies
      zgn <- colnames(zg) <- paste0("z.", make.names(colnames(zg))) # make.names cleans up spaces, etc.

      fmla.g <- paste("y ~ ", paste(zgn, collapse = "+"), "+", subgroupvars, " - 1")
      m <- mlm(as.formula(fmla.g), cbind(data.full, zg), match = data.full$m, treatment = data.full$z)

      idx <- grep(names(coef(m)), pattern = "^z\\.")
      result$stats <- c(result$stats, c(coef(m)[idx]))

      Amatrix <- bdiag(Amatrix, bread(m))

      estfuns <- cbind(estfuns, estfun(m))
      df <- min(df, summary(m)$df[2])
    }

    if (calcCov) {
      Bmatrix <- (1 / nlevels(match)) * crossprod(estfuns)
    }
  }

  if (calcCov) {

    # no reason to be a sparse matrix overall
    # Amatrix has no names, so we need to get them back after the multiplication
    tmp <- as.matrix(1 / nlevels(match) * (Amatrix %*% Bmatrix %*% Amatrix))
    rownames(tmp) <- rownames(Bmatrix)
    colnames(tmp) <- colnames(Bmatrix)

    result$tcov <- tmp[names(result$stats), , drop = FALSE][, names(result$stats), drop = FALSE]
  }

  result$df <- df

  return(result)
}

##' Inverts a matrix, falling back to a generalized inverse if the matrix is singular.
##'
##' @param m Matrix
##' @return Inverted (normal or generalized)
##' @import MASS
solve_with_fallback <- function(m) {
  minv <- try(solve(m), TRUE)
  if(class(minv) == "try-error") minv <- ginv(m)
  return(minv)
}


#' A little helper to generate naive p-values from normally distributed test stats.
#'
#' @param stat The statistics.
#' @param vars The variances of thes stats.
#' @param alternative "greater", "lesser", "two.sided"
#' @return A vector of p-values of the same length as stat.
#' @export
naivePvalues <- function(stat, vars, alternative) {
  greater <- pnorm(stat, sd = sqrt(vars), lower.tail = FALSE)
  lesser  <- pnorm(stat, sd = sqrt(vars), lower.tail = TRUE)
  out <- switch(pmatch(alternative, c("greater", "less", "two.sided")),
                greater,
                lesser,
                2 * pmin(greater, lesser))
  # pnorm has some oddities when sd = 0. vars will be 0 when the outcome is constant,
  # so we know that the p-value has to be .5 or 1.
  out[vars == 0] <- switch(pmatch(alternative, c("greater", "less", "two.sided")),
                           .5,
                           .5,
                           1)

  return(out)
}


#' Creates the analysis table for reporting. Handles selecting between
#' permutation and model based analysis and applying multiple comparison
#' corrections.
#' This table does not include the variable names
#'
#' @param outlog A logger tied to this outcome.
#' @param perm The result of permutationAnalysis
#' @param model The result of modelBasedAnalysis
#' @return A list of two elements: table (a matrix of the estimates and
#' p-values), perm (a boolean indicating if the permuational analysis was used
#' for all p-values, if false the model based analysis was used).
#' @export
makeAnalysisTable <- function(outlog, perm, model) {


  nests <- length(perm$stats)

  # we encountered a job where the perm estimates were all zero, as was the tcov matrix.
  # we'll short circuit if we have that issue
  if (all(perm$stats == 0) && all(perm$tcov == 0)) {
    tmp <- data.frame(adjusted_stats = rep(0, nests), allps = rep(1, nests))
    return(list(table = tmp, perm = TRUE))
  }

  mainfxp <- naivePvalues(perm$stats[1], diag(as.matrix(perm$tcov))[1], perm$alternative)


  # if the permutation based main effect test is not significant,
  # proceed to computing the pvalues based on the individual perm tests
  useperm <- mainfxp > 0.05
  if (useperm) {
    choice <- perm
  } else {
    choice <- model
  }

  if (is.matrix(choice$tcov) && dim(choice$tcov)[1] > 1) {
    if (useperm) {
      linfct <- cbind(0, diag(nests - 1))
    } else {
      linfct <- cbind(-1, diag(nests - 1))
    }
    adjps <- adjustedPvalues(choice$stats, choice$tcov, choice$alternative, df = choice$df, linfct = linfct)
	} else {
		adjps <- c() # there weren't any subgroups
	}

  # These should be identical to model$stats in most cases, see issue #273

	allps <- c(mainfxp, adjps)
  adjusted_stats <- compare_analyses(outlog, model$stats, perm$stats, allps)

  return(list(table = cbind(adjusted_stats, allps), perm = useperm))

}

##' Handles issue #273, situations where modelBasedAnalysis and permutationAnalysis give contraditory or otherwise
##' problem estimtes.
##'
##' @param modelstats $stats from modelBasedAnalysis
##' @param permstats $stats from permutationAnalysis
##' @param pvals p-values for each test statistic
##' @return An updated version of the modelstats.
##' @author Josh
#' @export
compare_analyses <- function(outlog, modelstats, permstats, pvals) {

  if (is.na(modelstats[1])) {
    outlog("Sign conflict in permutation and model based analysis:",
           "NA effect estimate from modelBasedAnalysis")

    modelstats[1] <- permstats[1]
  }

  if (modelstats[1] * permstats[1] < 0) {
    if (is.na(pvals[1]) || pvals[1] < .2) {
      outlog("Sign conflict in permutation and model based analysis:",
             "Sign conflict with p-value", pvals[1])
    }

    modelstats[1] <- ifelse(is.na(pvals[1]), 0, permstats[1]*(pvals[1] < .05))
  }

  return(modelstats)
}
##' Fit model for covariance adjusted outcomes
##'
##' Allows different models to be fit per outcome type.
##' @param oname The name of the outcome.
##' @param d Data frame with components: match and any covariates
##' @param trim Winsorize residuals at 1-trim quantile (using \code{\ref{topandbottomcoder}}
##' @return Either the covariance adjusted outcomes, or NULL
##' @import MASS SparseM
##' @export
getCovarianceAdjusted <- function(oname, d, trim) {

  if (length(oname) == 0) {
    logger("Not using covariance adjusted outcomes : No outcome name given")
    return(NULL)
  }
  stopifnot(length(oname) == 1)

  # make sure that this is a _scr_ outcome type
  if (!grepl("_scr_", oname)) {
    logger("Not using covariance adjusted outcomes for ", oname, ": No suitable method for outcome type")
    return(NULL)
  }

  # smoothed scores
  all_smhstu <- names(d)[grepl("smhstu", names(d))]

  if (length(all_smhstu) == 0) {
    logger("Not using covariance adjusted outcomes for ", oname, ": No smoothed scores present")
    return(NULL)
  }

  # plan: for each grade, update the entires of the newoutcome vector with their new outcomes
  # if a grade fails for some reason, those students just keep their raw outcomes
  newoutcome <- d$outcome
  for (g in levels(d$gradelevel)) {
    in_this_grade <- d$gradelevel == g
    dg <- d[in_this_grade, ]
    dg$match <- dg$match[, drop = TRUE] # drop record of matches from other grades

    # drop any constant columns (especially NA_***) that would make the matrix singular.
    # rlm can't handle constant columns apparently
    # (Note that by design, there are no actual NA's in covs, so shouldn't be any risks of NA's from the sapply.)
    g_all_smhstu <- all_smhstu[sapply(dg[,all_smhstu], function(x) !all(x == x[1]))]

    if (length(g_all_smhstu) == 0) {
      logger("Not using covariance adjusted outcomes for ", oname, " for grade ", g, ": All smoothed scores are constant and dropped")
      next # go to next grade
    }

    NA_smhstu <- g_all_smhstu[grepl("^NA", g_all_smhstu)]
    smhstu    <- g_all_smhstu[!grepl("^NA", g_all_smhstu)]

    scr <- gsub("smhstu", "scr", smhstu)
    scr <- scr[scr %in% names(dg)]
    NA_scr <- paste0("NA_", scr)
    NA_scr_exists <- NA_scr %in% names(dg)
    names(NA_scr_exists) <- scr

    # if scr is NA, replace with its smoothed score.
    imp_scr_data <- sapply(scr, function(x) {
      if (NA_scr_exists[x]) {
        xscr_data <- dg[,x]
        xsmh_data <- dg[,gsub("scr", "smhstu", x)]
        toReplace <- dg[,paste0("NA_", x)]

        xscr_data[as.logical(toReplace)] <- xsmh_data[as.logical(toReplace)]
        return(xscr_data)
      } else {
        return(dg[,x])
      }
    })

    data <- cbind(outcome = dg$outcome, dg[, smhstu], imp_scr_data)
    datanames <- c(oname, names(data)[-1L])

    # a check to see if all units are in the same block (e.g. an unblocked RCT)
    # if the design is blocked, we we can compute residuals per block
    # if the desgin in not blocked, we simply mean center the data
    if (length(unique(dg$match)) > 1) {
      ### To remove stratum fixed effects w/o undue memory usage in
      ### in large problems, we use a sparse design matrix for the fixed effects.
      match_as_SM_designmatrix <- SparseMMFromFactor(dg$match)
      ### Assumes columns of data are all of class "numeric"
      data <- resid(slm.fit(match_as_SM_designmatrix, as.matrix(data)))
      data <- as.data.frame(data)
    } else {
      data <- as.data.frame(lapply(data, function(col) { col - mean(col) }))
    }
    colnames(data) <- datanames

    mod <- try( rlm(x=data[,datanames!=oname,drop=FALSE], y=data[[oname]]), silent=TRUE )
    if (class(mod)[1] == "try-error") {
      logger("Not using covariance adjusted outcomes for ", oname, " for grade ", g, ": rlm fit failed")
      next #
    } else {
      newoutcome[in_this_grade] <- residuals(mod, type="response")
    }
  }
    newoutcome <- topandbottomcoder(newoutcome, trim=trim)
  return(newoutcome)
}
