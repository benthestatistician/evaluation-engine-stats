DBCONNECTION <<- NULL
JBCONNECTION <<- NULL
LOGS         <<- NULL
LOGFILE      <<- NULL

#' Sets ups the proper run-time environment for EEM.
#'
#' @param ename One of test, development, staging, production
#' @param studentdb A DBI connection object for the student database
#' @param jobd A DBI connection object for the jobs database
#' @return NULL
#' @export
#'
#' @import DBI
#' @imports db.R job.R
setup <- function(ename, studentdb, jobdb) {

  options("eem_environment" = ename)

  ### Set the global connection object

  DBCONNECTION <<- studentdb
  JBCONNECTION <<- jobdb

  LOGS <<- freshLogs()

  LOGFILE <<- NULL

}

.onLoad <- function(lib, pkg) {
  # setup()
}

freshLogs <- function() { 
  data.frame(name = "default",
             time = date(),
             message = "Initialized logging system",
             stringsAsFactors = FALSE)
}
