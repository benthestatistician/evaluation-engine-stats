################################################################################
## A place for tools dealing with large jobs
################################################################################

#' Implements several methods for turning large jobs into smaller jobs.
#'
#' Compared to methods elsewhere, these approaches take a less rigorous
#' statistical approach to save jobs that have otherwise proved difficult to
#' make small enough for the matching routines to run in a feasible amount of
#' time.
#'
#' The choice of whether a job is feasible is
#' \code{getOption("optmatch_max_problem_size")}.
#'
#' If a job is too large, the first step is to try to find a combination of
#' exact match features that will make is feasible. As of this writing the
#' function we use from optmatch is overly conservative in that it doesn't
#' consider arcs that are already removed in distm (i.e., it assumes distm is
#' dense).
#'
#' @param distm Distance object suitable for matching.
#' @param covs A covariates table for the individuals corresponding to rows and columns in the distance matrix. We expect this will hold the subgrouping variables (e.g., gender, raceth, etc).
#' @return A new distance object, with fewer comparisons.
#' @import optmatch
#' @export
largeJobFallback <- function(distm, covs) {

  maxarcs <- getOption("optmatch_max_problem_size")

  distm <- as.InfinitySparseMatrix(distm)

  ## the allowed problem size depends on both the number of finite entries and the number of rows and columns
  allowedfinite <- maxarcs - (sum(dim(distm)) + 2)
  finites <- length(distm)

  if (finites < allowedfinite) {
    return(distm)
  }

  availSubgroups <- intersect(colnames(covs), c(
        "gender",
        "specialed_now",
        "lep_now",
        "frl_2yr",
        "raceth"))

  ## minExactMatch will throw an error if it can't find a combination of factor terms that work 
  ## we catch the error so we can try other methods of mEM fails.
  minEmFactor <- tryCatch(minExactMatch(as.formula(paste("z ~", paste(availSubgroups, collapse = "+"))),
                                 data = covs,
                                 maxarcs = allowedfinite),
                          error = function(e) { return(NULL) })

  if (!is.null(minEmFactor)) {
    names(minEmFactor) <- rownames(covs)
    return(exactMatch(minEmFactor, covs[, "z"]) + distm)
  }

  ## if exact matching can't work, fall back to setting a caliper that ensures that there are precisely
  ## maxarcs comparisons to make
  srted <- sort(distm@.Data)
  calwidth <- srted[allowedfinite - 1]
  return(caliper(distm, width =  calwidth, values = TRUE))
}
