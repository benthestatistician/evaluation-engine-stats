################################################################################
# Tests for the main matching routine
################################################################################

library(testthat)

context("Matching")

## test_that("Strategies", {

##   expect_true(exists("getStrategies"))

##   set.seed(20121108)
##   # for now, just list 5 schools to use
##   schoolIDs <- paste("school-id-", 1:5, sep = "")

##   ids <- dbfetch(paste("SELECT STUDY_ID FROM nm2012_drv WHERE SCHOOL_ID_2012 IN (",
##                        paste("'", schoolIDs, "'", collapse = ",", sep = ""), ") LIMIT 100"))[,"STUDY_ID"];

##   # spec with one outcome and one subgroup
##   spec <- list(Treated = ids,
##                OutcomeMeasures = c("ELA1SCR_2012", "ELA1PRF_2012"),
##                State = "nm",
##                InterventionStartDate = "2012-09-01",
##                InterventionEndDate = "2012-12-01",
##                SubgroupAnalysis = c("RACETH"))

##   covs <- getCovariates(spec, schoolIDs)

##   strategies <- getStrategies(spec, covs)

##   expect_true(all(sapply(strategies, class) == "function"))

##   s1 <- strategies[[1]]()

##   expect_true(inherits(s1, "list"))

## })

test_that("school level model building", {
  set.seed(20130614)
  n.schools <- 100
  X1 <- rnorm(n.schools)
  X2 <- rnorm(n.schools)
  total.students <- floor(runif(n.schools, 100, 1000))
  total.treated <- floor(runif(n.schools, 0, 0.2) * total.students * ((X1 + 2 * X2) > 0.5))

  schools.data <- data.frame(z = total.treated,
                             "TOTAL_STUDENTS_2012" = total.students,
                             "X1_2012" = X1,
                             "X2_2012" = X2)

  rownames(schools.data) <- paste("school-id-", 1:n.schools, sep = "")

  #### Interface Testing ####
  # this function turns a school data frame into a model and calls the logic function
  # we start by testing this interface without caring much about the results

  # res <- comparableSchools(schools.data)

  # expect_true(length(res) > 0)
  # expect_true(all(res %in% rownames(schools.data)))

  # # should exclude at least some schools
  # expect_true(!all(rownames(schools.data) %in% res))

  # # all treated schools must be included
  # expect_true(all(rownames(schools.data[schools.data$z > 0,]) %in% res))


  ##### Logic Testing #####
  # since the specification of this function is really at the propensity score level
  # we test the logic in its own function

  # the simple case: there are not enough treated units in the schools to get to 100 controls per treated student.
  # The results should be the same even if the propensities are changed
  z <- c(100, 100, 0, 0)
  total <- c(200, 200, 100, 100)
  p1 <- rep(0, 4) # everyone has 0.5 prob of treatment

  res.easy1 <- comparableSchoolsLogic(z, total, p1)
  expect_true(all(res.easy1)) # all are selected

  p2 <- c(0.5, 1, -0.5, -1)
  res.easy2 <- comparableSchoolsLogic(z, total, p2)
  expect_true(all(res.easy2))

  z <- c(100, 100, 0, 0,0)
  total <- c(200, 200, 200, 200,200)
  logits <- qlogis(c(.5,.5,0,0,0))
  expect_true(all(comparableSchoolsLogic(z,total,logits)))

  z <- c(100, 100, 100, 0,rep(0,4))
  total <- rep(200,8)
  logits <- qlogis(c(.5,.5,.25,.25,rep(0,4)))
  expect_true(all(comparableSchoolsLogic(z,total,logits)))

  # if the median density value is too low, we also just return everyone
  z <- c(1, 1, 1, 0)
  total <- c(100, 100, 100, 10)
  res.density <- comparableSchoolsLogic(z, total, p1)
  expect_true(all(res.density))

  # now the more interesting cases
  z <- c(10, 10, 0, 0)
  total <- c(500, 500, 1000, 1000)
  p1 <- c(0, 0, -0.25, -2)

  res.some <- comparableSchoolsLogic(z, total, p1)
  expect_equal(res.some, c(T, T, T, F))

  z <- c(100,200, 0,0  )
  total <- c(200, 200, 2000,200  )
  logits <- qlogis(c(.5,.5,.5,0))
  expect_equal(sum(comparableSchoolsLogic(z,total,logits)), 3)

  # Short circuit when the treated group is over 50% of the cohort
  z <- c(100, 200, 0, 0)
  total <- c(200, 200, 50, 50)
  logits <- qlogis(c(.5,.5,.5,0))
  expect_equal(sum(comparableSchoolsLogic(z,total,logits)), 4)

})

test_that("selecting model buidling function", {

  # don't rely on  build_PS_specs for the testing
  specl <- list(formula = list("foo" = "y", "bar" = "y^2 + y"))

  # just pass false as the job and covs
  res <- make_PS_fitter(job = FALSE, specs = specl)

  z <- rep(c(0,1), 50)
  y <- rnorm(100)
  dat <- data.frame(z, y)

  # for now, we expect it to use bayeglm
  require(arm)

  # expect_equal(predict(res(z ~ y, dat)), predict(bayesglm(z ~ y, dat, family = binomial)))

  # expect_equal(predict(res("foo", dat)), predict(bayesglm(z ~ y, dat, family = binomial)))

  # expect_equal(predict(res("bar", dat)), predict(bayesglm(z ~ y^2 + y, dat, family = binomial)))

  # # bayesglm usage should be robust to missing factor levels and later use of the predict method.

  # badcovs <- data.frame(y = rep(c(0,1), 50),
  #                       x = factor(rep(c(1,2,3,4), each = 25),
  #                                  levels = 1:5,
  #                                  labels = c("A", "B", "C", "D", "E")))

  # fitter <- make_PS_fitter(NULL, badcovs, specs = NULL)
  # model <- fitter(y ~ x, badcovs)

  # safe.predict <- predict(model) # should always work
  # unsafe.predict <- predict(model, newdata = badcovs) # this fails when bayesglm doesn't have `drop.unused.levels = FALSE`

  # bayesglm has issues with perfectly correlated columns. we check for the issue and correct it.
  data.cor <- data.frame(z = rep(c(0,1), 50),
                         x1 = rnorm(100),
                         x2 = c(rep(TRUE, 1), rep(FALSE, 99)),
                         x3 = c(rep(TRUE, 1), rep(FALSE, 99)),
                         x4 = c(rep(TRUE, 1), rep(FALSE, 99)),
                         x5 = c(rep(TRUE, 1), rep(FALSE, 99)),
                         x6 = c(rep(TRUE, 1), rep(FALSE, 99)),
                         gradelevel = rep(1, 100))

  fitter.cor <- make_PS_fitter(NULL, specs = list(formulas = list("cor" = "x1 + x2 + x3 + x4 + x5 + x6")))

  expect_true(!("x3TRUE" %in% names(coef(fitter.cor("cor", data.cor)))))


})

test_that("covariate_caliper", {
  data(iris)
  iris$Species <- 1*(iris$Species == "setosa")

  # a single covariate
  m <- match_on(Species ~ Sepal.Length, data=iris, method="euclidean")
  c <- caliper(m, 2)

  smalliris <- data.frame(iris[, c("Species","Sepal.Length")])
  names(smalliris) <- c("z", "Sepal.Length")

  cov.sep <- list("Sepal.Length" = 2)

  cc <- covariate_caliper(smalliris, cov.sep)

  expect_true("InfinitySparseMatrix" %in% class(cc))
  expect_true(all(cc == 0))
  expect_equal(dim(cc), c(sum(iris$Species), dim(iris)[1] - sum(iris$Species)))


  compareCals <- function(a, b) {
    ma <- as.matrix(a)
    mb <- as.matrix(b)

    mb <- mb[, colnames(a)]
    mb <- mb[rownames(a), ]

    return(identical(ma, mb))
  }

  expect_true(compareCals(cc, c))


  # several covariates
  m1 <- match_on(Species ~ Petal.Width, data=iris, method="euclidean")
  m2 <- match_on(Species ~ Petal.Length, data=iris, method="euclidean")
  m3 <- match_on(Species ~ Sepal.Width, data=iris, method="euclidean")
  c3 <- caliper(m1, 2) + caliper(m2, 4) + caliper(m3, 1)

  cov.sep <- list( "Petal.Length"=4, "Petal.Width"=2, "Sepal.Width"=1)

  smalliris <- data.frame(iris[, c("Species", "Petal.Width", "Petal.Length", "Sepal.Width")])
  names(smalliris) <- c("z", "Petal.Width", "Petal.Length", "Sepal.Width")

  cc <- covariate_caliper(smalliris, cov.sep)
  expect_true(!"list" %in% class(cc))

  expect_true(compareCals(c3,cc))

  # order of names in data & cov.sep differ

  cov.sep <- list("Petal.Width"=2, "Sepal.Width"=1, "Petal.Length"=4)

  smalliris <- data.frame(iris[, c("Species", "Petal.Width", "Petal.Length", "Sepal.Width")])
  names(smalliris) <- c("z", "Petal.Width", "Petal.Length", "Sepal.Width")

  cc2 <- covariate_caliper(smalliris, cov.sep)

  expect_true(!"list" %in% class(cc2))
  expect_true(compareCals(c3,cc2))

  # function caliper

  sdcal <- function(x) {
    .75*(max(x) - min(x))
  }


  cov.sep.func <- replicate(3, sdcal)
  cov.sep.val <- as.list(apply(smalliris[,-1], 2, sdcal))

  names(cov.sep.func) <- names(cov.sep.val)

  m <- covariate_caliper(smalliris, cov.sep.func)
  n <- covariate_caliper(smalliris, cov.sep.val)

  expect_true(compareCals(n,m))

  # mix of numeric & function
  cov.sep.mix <- cov.sep.func
  cov.sep.mix[1] <- cov.sep.val[1]

  m2 <- covariate_caliper(smalliris, cov.sep.func)

  m2@call <- NULL

  expect_true(compareCals(n,m2))

  # using defaults

  cov.sep.1 <- list("Sepal.Width"=1)
  cov.sep.1.val <- list("Petal.Width" = diff(range(smalliris$Petal.Width)),
                        "Petal.Length" = diff(range(smalliris$Petal.Length)),
                        "Sepal.Width" = 1)
  md <- covariate_caliper(smalliris, cov.sep.1)
  nd <- covariate_caliper(smalliris, cov.sep.1.val)
  md@call <- nd@call <- NULL
  expect_true(compareCals(md,nd))

  # using different default
  ndd <- covariate_caliper(smalliris, default.separation = sdcal)
  ndd@call <- NULL

  expect_true(compareCals(m, ndd))

  # non-numeric covariates
  nonnumdata <- data.frame(z = rep(c(1,0), 10),
                       f = as.factor(rep(c("a", "b", "c", "d"), 5)),
                       ch = rep(c("a", "b", "c", "d"), 5))

  expect_error(covariate_caliper(nonnumdata, default.separation = 1))
})

test_that("global_ps_caliper", {
  data(iris)
  iris$Species <- 1*(iris$Species == "setosa")
  iris <- iris[40:60,]
  row.names(iris) <- NULL
  # add some odd observations
  iris$Species[11] <- 0
  iris$Species[12] <- 1
  iris <- iris[,c(5,1:4)]
  names(iris)[1] <- "z"

  sdcal <- function(x) {
    diff(range(x))
  }


  # this testing is from the previous version of global_PS_caliper
  # we need a better way to see if we are getting the correct value
  # and/or test over properties: always positive, somehow related to the scale of the propensity scores, etc.

  ps1 <- glm(z ~ ., family=binomial, data=iris)
  sandcov <- sandwich(ps1)[-1, -1]

  g1 <- match_on(ps1, data=iris, standardization.scale=NULL)
  dmax1 <- apply(iris[,-1], 2, sdcal)

  gc1 <- 2 * sqrt(dmax1 %*% sandcov %*% dmax1)[1,1]
  dmax1 <- as.list(dmax1)
  gc2 <- global_PS_caliper(ps1)

  ## expect_true(identical(gc1,gc2))
})

test_that("relative_density_caliperwidth", {
  library(optmatch)
  data(nuclearplants)

  ps <- glm(pr ~ . - cost, family=binomial, data=nuclearplants)
  expect_error(relative_density_caliperwidth(ps, 4, "a"))
  expect_error(relative_density_caliperwidth(ps, "a", 50))
  expect_error(relative_density_caliperwidth(ps, "4", 50))
  expect_error(relative_density_caliperwidth(ps, -1, 50))
  expect_error(relative_density_caliperwidth(ps, 4, -50))

  expect_true(class(relative_density_caliperwidth(1)) == "numeric")
  expect_true(length(relative_density_caliperwidth(1)) == 1)
  expect_true(length(relative_density_caliperwidth(1:5)) == 5)
  expect_true(length(relative_density_caliperwidth(ps)) == 10)
  expect_equal(relative_density_caliperwidth(ps), relative_density_caliperwidth(predict(ps)[as.logical(ps$y)]))
  expect_true(all(relative_density_caliperwidth(-5) == relative_density_caliperwidth(c(-5,-5))))

})

test_that("build_PS_specification", {

  expect_true(exists("build_PS_specifications"))

  set.seed(20121108)
  # for now, just list 5 schools to use
  schoolIDs <- paste("school-id-", 1:5, sep = "")

  ids <- dbfetch(paste("SELECT study_id, gradelevel FROM tn2012_drv WHERE dstschid_state_enroll_p0 IN (",
                       paste("'", schoolIDs, "'", collapse = ",", sep = ""), ") AND gradelevel in (9, 10) LIMIT 100"));

  # spec with one outcome and one subgroup
  job <- new("ObservationalJob",
             ID                  = "fakejob",
             Treated             = ids$study_id,
             State               = "tn",
             TreatmentYear       = 2011L,
             OutcomeTable        = outcomesTable("tn",
                                                 "20120101",
                                                 "reading,math",
                                                 "2011-2012"),
             SubgroupAnalyses    = c("raceth"),
             StudentMeta         = getMetaData(),
             SchoolMeta          = getMetaData("sch"),
             StudentTable        = "tn2012_drv",
             SchoolTable         = "tn2012_sch",
             StudentSchoolLink   = "dstschid_state_enroll_p0",
             StudentDistrictLink = "districtid_state_enroll_p0",
             DistrictMatch       = "NONE",
             EarlyIntervention   = TRUE,
             ObservedGrades      = unique(ids$gradelevel))

  covs <- getCovariates(job, schoolIDs)

  res <- build_PS_specifications(job, covs)

  expect_true(inherits(res, "list"))
  expect_false(is.null(res["formulas"]))

  expect_true(all(sapply(res$formulas, function(i) inherits(i, "character"))))
  expect_true(length(res$formulas) > 0)

  expect_true(all(nchar(res$formulas) > 0))

  # main should include all subgroups, inclusive should include all of main
  expect_true(all(res$raw$subgroup %in% res$raw$subgroup))
  expect_true(all(res$raw$main %in% res$raw$inclusive))

  # any column that is constant should not appear in the formula

  covs.varies <- data.frame(age = rnorm(100), gender = as.factor(rep(c("Male", "Female"), 50)))
  covs.const <- data.frame(age = rnorm(100), gender = as.factor(rep("Male", 100)))

  res.varies <- build_PS_specifications(job, covs.varies)
  res.const <- build_PS_specifications(job, covs.const)

  expect_true(length(grep(x = res.varies$formulas["main"], pattern = "gender")) > 0)
  expect_true(length(grep(x = res.const$formulas["main"], pattern = "gender")) == 0)


  # missing variables should not be included in the formulas
  covs2 <- covs
  covs2$gender <- NULL

  res.nogender <- build_PS_specifications(job, covs2)

  expect_false(any(grepl(x = res.nogender$formulas["main"],
                         pattern = "gender")))

  # NA gender

  covs3 <- covs
  covs3$gender <- NA

  res.NAgender <- build_PS_specifications(job, covs3)

  expect_false(any(grepl(x = res.NAgender$formulas["main"],
                         pattern = "gender")))

  # "" gender
  covs4 <- covs
  covs4$gender <- ""

  res.emptygender <- build_PS_specifications(job, covs4)

  expect_false(any(grepl(x = res.emptygender$formulas["main"],
                         pattern = "gender")))

  # Returns all columns, regardless of whether they are dropped
  res.allcols <- build_PS_specifications(job, covs4, dropConst = FALSE)

  expect_true(any(grepl(x = res.allcols$formulas["main"],
                         pattern = "gender")))

  # raw lists (not "+" concatenated) are also returned
  expect_true(length(res.allcols$raw$main) > 0)


})


test_that("working with meta data and database names", {

  expect_equal(metaName("gender"), "gender")
  expect_equal(metaName("NA_gender"), "gender")

})


test_that("matchEligble", {

  set.seed(20130619)
  mm <- matrix(c(rep(0, 12), Inf,Inf,Inf,Inf, rep(0, 12)), nrow = 4)
  colnames(mm) <- letters[1:7]
  rownames(mm) <- LETTERS[23:26]
  orig <- data.frame(1:11)
  rownames(orig) <- c(letters[1:7], LETTERS[23:26])

  im <- optmatch:::as.InfinitySparseMatrix(mm)

  expected <- c(T,T,T,F,T,T,T,T,T,T,T)
  names(expected) <- rownames(orig)

  expect_equal(matchEligible(im, orig), expected)

  reorg <- sample(1:11)
  expect_equal(matchEligible(im, orig[reorg, , drop = F]), expected[reorg])


})


test_that("update_globalPScal", {
  library(optmatch)
  data(nuclearplants)
  ps <- glm(pr ~ . - cost, family=binomial, data=nuclearplants)

  globalPScal <- match_on(predict(ps, data = nuclearplants), z = nuclearplants$pr, caliper = global_PS_caliper(ps))
  library(sandwich)
  globalPScal2 <- update_globalPScal(ps, globalPScal, data=nuclearplants, covariance.extractor=sandwich)

  ## test was in error, disabling until we can come up with a better test
  ## expect_true(length(globalPScal2@.Data) < prod(dim(globalPScal2)))

})

test_that("maxMatchedDistance", {
  mm <- matrix(1, nrow = 4, ncol = 5, dimnames = list(treated = c("A", "B", "C", "D"), control = c("e", "f", "g", "h", "i")))
  nms <- unlist(dimnames(mm)) ; names(nms) <- NULL
  n2 <- rep(1, length(nms)) ; names(n2) <- nms

  ism <- optmatch:::as.InfinitySparseMatrix(mm)

  match.ism <- fullmatch(ism, data = n2)

  res.max <- maxMatchedDistance(match.ism, ism)

  expect_equal(res.max, 1)
})

test_that("penalize_coefficient", {
  set.seed(352)
  x1 <- rnorm(4)
  z <- c(1,0,1,0)
  dist <- c(1,1,3,3)
  d <- as.data.frame(cbind(z,x1, dist))
  rm(list=names(d))

  m <- match_on(z~x1, data=d, caliper=1)

  m2 <- eem:::penalize_coefficient(z~dist, 10, data=d)

  expect_true(all.equal(m + m2,m + matrix(c(0,10,10,0), 2, 2)))

  # no penalties
  dist <- rep(1,4)
  d$dist <- dist
  rm(dist)

  m3 <- eem:::penalize_coefficient(z~dist, 10, data=d)
  expect_true(identical(m+m3, m))

  # all penalties
  dist <- 1:4
  d$dist <- dist
  rm(dist)
  m4 <- eem:::penalize_coefficient(z~dist, 10, data=d)
  expect_true(identical(m+m4, m+10))

  # using an existing sparse representation as a within args
  tmp <- matrix(c(1,0,Inf,4), nrow = 2)
  rownames(tmp) <- c(1,3)
  colnames(tmp) <- c(2,4)

  w <- optmatch:::as.InfinitySparseMatrix(tmp)
  expect_equal(length(eem:::penalize_coefficient(z ~ dist, 10, data = d, w)), 3)

  # bad calls
  expect_error(eem:::penalize_coefficient(z~dist+x1, 10, data=d))
  expect_error(eem:::penalize_coefficient(dist~z, 10, data=d))

  # handle missingness in the data, when the items are in the within arg
  d$dist[1] <- NA
  res.miss <- eem:::penalize_coefficient(z ~ dist, 10, data = d, w)
})


test_that("compare_worse_than_matches", {

  expect_equal(compare_worse_than_matches(c(.3, .4), c(.2, .4)), 3)
  expect_equal(compare_worse_than_matches(c(.03, .4), c(.2, .001)), 0)
  expect_equal(compare_worse_than_matches(c(.3, .4), c(.001, .4)), 1)
  expect_equal(compare_worse_than_matches(c(.3, .4), c(.2, .7)), 2)

  expect_error(compare_worse_than_matches(.4, .2))
  expect_error(compare_worse_than_matches(c(.4, .2), .2))
  expect_error(compare_worse_than_matches(.4, 2))
  expect_error(compare_worse_than_matches(c(.4, .2), c(.4, "a")))
  expect_error(compare_worse_than_matches(c(.4, .2), c(.4, .2, .2)))

})

test_that("get_best_w", {
  set.seed(3512)
  x1 <- rnorm(10)
  x2 <- rnorm(10, 4, 10)
  z <- c(rep(0,6), rep(1, 4))
  d <- as.data.frame(cbind(z,x1, x2))
  rm(list=names(d))

  m1 <- match_on(z~x1+x2, data=d)
  m2 <- match_on(z~x2, data=d)

  match1 <- fullmatch(m1, data=d)
  match2 <- fullmatch(m2, data=d)

  fm1 <- z~ x1 + strata(match2)
  fm2 <- z~ x2 + strata(match2)

  d$match2 <- match2

  bal1 <- balanceTest(fm1, data=d, report=c("all"))
  bal2 <- balanceTest(fm2, data=d, report=c("all"))

  a <- eem:::get_best_w(m2, m1, c(bal1$overall["match2", "p.value"], bal2$overall["match2", "p.value"]), fm1, fm2, data=d)

  expect_true(a$w >= 0 & a$w <=1)
  expect_true(inherits(a$match, "optmatch"))
  expect_true(inherits(a$main, "xbal"))
  expect_true(inherits(a$inc, "xbal"))
})

test_that("Adding penalties to specific (control) units", {
  m <- matrix(0, nrow = 3, ncol = 4,
              dimnames = list(treatment = c("A", "B", "C"),
                              control   = c("w", "x", "y", "z")))

  mm <- optmatch:::as.InfinitySparseMatrix(m)

  # add a penalty to some of the columns
  nm <- c("w" = 3, "x" = 9, "z" = 4)
  expect_equal(sum(addPenalty(nm, mm)), 48)

  # the rows
  nm <- c("A" = 1, "C" = 2)
  expect_equal(sum(addPenalty(nm, mm)), 12)

  # a little of both
  nm <- c("A" = 1, "x" = 2, "y" = 3)
  res <- as.matrix(addPenalty(nm, mm))

  #    w  x  y  z
  # A  1  3  4  1
  # B  0  2  3  0
  # C  0  2  3  0
  expected <- matrix(c(1, 0, 0, 3, 2, 2, 4, 3, 3, 1, 0, 0),
                     nrow = 3, ncol = 4)

  expect_equal(sum(expected), 19)
  expect_equal(sum(res), 19)
  expect_equivalent(as.matrix(res), expected)
})

test_that("adding penalties to treated school control units", {

  z <- c(0, 0, 1, 2, 0, 4)
  total <- c(1, 1, 2, 2, 5, 5)
  schools <- data.frame(z = z,
                        density = z / total)
  rownames(schools) <- 1:6

  # each school on a row
  z <- c(0,
         0,
         1,0,
         1,1,
         0,0,0,0,0,
         0,1,1,1,1)

  sids <- rep(1:6, times = total)

  names(z) <- names(sids) <- 1:length(z)

  res <- school_treated_penalty(schools, 10, z, sids)

  expect_equal(range(res), c(0,8)) # the max is for the one control unit in a 4/5 school

})

test_that("Penalties based on disability", {
  z <- c(1,1,1,0,0,0)
  dis <- factor(c(1,2,2,1,2,6))

  # 1,4; 2,5; 3,5 should have no penalty, rest should have penalty

  d <- disability_penalty(z, dis, 5)

  expect_true(is(d, "DenseMatrix"))
  expect_true(all(dim(d) == c(3,3)))

  d <- as.matrix(d)
  expect_true(d["1","4"] == 0)
  expect_true(d["2","5"] == 0)
  expect_true(d["3","5"] == 0)

  # this should capture everything else
  expect_true(sum(d) == 6 * 5 * sqrt(2))

  # passing a within

  s <- c(1,2,2,1,2,2)
  e <- exactMatch(z~s)

  d <- disability_penalty(z, dis, 5, within=e)

  expect_true(is(d, "BlockedInfinitySparseMatrix"))
  expect_true(all(dim(d) == c(3,3)))
  expect_true(all(d@rows == e@rows))
  expect_true(all(d@cols == e@cols))

  d <- as.matrix(d)
  # blocking shouldn't have affected the 0's
  expect_true(d["1","4"] == 0)
  expect_true(d["2","5"] == 0)
  expect_true(d["3","5"] == 0)

  expect_true(d["2","6"] == 5 * sqrt(2))
  expect_true(d["3","6"] == 5 * sqrt(2))

})

test_that("match summmaries", {

  ### Effect of Treatment on Treated for units and strata
  m <- as.factor(c(A = 1, b = 1, C = 2, D = 2, e = 2, F = NA, g = NA, H = 3, i = 3, j = 3, k = 3))
  w <- c(A = 1, b = 1, C = 1, D = 1, e = 2, F = 0, g = 0, H = 1, i = 1/3, j = 1/3, k = 1/3)
  z <- c(1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0)

  expect_equal(effect_treatment_on_treated(m,z), w)

  # strata version just gives back strata level values
  w2 <- c("1" = 1, "2" = 2, "3" = 1)

  expect_equal(effect_treatment_on_treated(m, z, compress = TRUE), w2)

  ### Percentage match that are the same on some value
  v <- c(A = 1, b = 1, C = 1, D = 2, e = 2, F = 0, g = 0, H = 3, i = 3, j = 3, k = 2)
  attr(m, "contrast.group") <- z
  expect_equal(percentSameHelper(m, z, v), 4/6)

})

test_that("percentSame", {
  set.seed(124)
  x <- rnorm(10)
  y <- sample(1:2,10,TRUE)
  t <- rep(0:1,5)

  # make a match with both 2:1 and 1:2
  f <- fullmatch(t ~ x, data=data.frame(x,t))

  expect_true(percentSameHelper(f, t, y) == 3/6)

  # malformed match (can happen when dropping things earlier in ee)
  f[7] <- NA

  expect_true(percentSameHelper(f, t, y) == 3/5)

  # match with an unused level
  f[6] <- NA

  expect_true(percentSameHelper(f, t, y) == 3/5)

})

test_that("fill.NAs.intervention", {
  b <- rnorm(10)
  b[3] <- NA
  b[9] <- NA
  b2 <- rexp(10)
  t <- as.logical(round(runif(10)))
  t[4] <- NA
  t[10] <- NA
  z <- c(rep(0,5), rep(1,5))
  d <- as.data.frame(cbind(z,b,b2,t))

  # make sure its the right size
  m <- fill.NAs.intervention(d)
  expect_equal(class(m), "data.frame")
  expect_equal(dim(m), c(10, 6))

  # formula and data act the same
  m2 <- fill.NAs.intervention(z~b+b2+t, data=d)
  expect_equal(m,m2)

  # NA's get filled by same value
  expect_true(m[3,2] == m[9,2])
  expect_true(m[4,4] == m[10,4])

  # NA's get filled by correct value
  expect_true(m[3,2] == mean(d$b[d$z == 1], na.rm=TRUE))
  expect_true(m[4,4] == mean(d$t[d$z == 1], na.rm=TRUE))

  # non-NA's don't get affected
  expect_true(all(m[,1:4]-d == 0, na.rm=TRUE))

  # passing a formula that doesn't include all data
  m3 <- fill.NAs.intervention(z~b, data=d)
  expect_equal(dim(m3), c(10, 3))
  expect_true(all(m3$b == m2$b))

  # everything's !na
  m4 <- fill.NAs.intervention(z~b2, data=d)
  expect_equal(m4, d[,c(1,3)])

  # fill.NAs.intervention same as fill.NAs when all treated
  z <- rep(1,10)
  d <- as.data.frame(cbind(z,b,b2,t))

  m5 <- fill.NAs(d)
  m6 <- fill.NAs.intervention(d)
  expect_true(all(m5==m6))

})

test_that("Turn NA's into Infs in an ISM", {
  set.seed(20130830)
  n <- 100
  xys <- matrix(rnorm(2 * n), nrow = n)
  z <- rep(c(1,0), n/2)
  b <- rep(c(1:4), each = n / 4)

  dist.xys <- match_on(z ~ xys, within = exactMatch(z ~ b))

  k <- length(dist.xys)
  nas <- floor(k/10)
  dist.xys[sample.int(k, nas)] <- NA

  res <- naToInf(dist.xys)
  expect_equal(length(res), length(dist.xys) - nas)
  expect_equal(res@.Data, dist.xys@.Data[!is.na(dist.xys)])
  expect_is(res, "InfinitySparseMatrix")
})


test_that("mean.c determiner", {
  data(nuclearplants)

  # Non-blocked
  m1 <- match_on(pr ~ cost, data=nuclearplants)
  # this has 2.2 controls/treatment

  expect_true(is.null(get_mean.c(m1, NULL)))
  expect_true(is.na(get_mean.c(m1,3)))
  expect_true(get_mean.c(m1,2) == 2)
  expect_true(get_mean.c(m1,2.2) == 2.2)
  expect_true(is.na(get_mean.c(m1, NA)))

  expect_error(get_mean.c(m1,c(1,2)))
  expect_error(get_mean.c(m1,c(2, NA)))
  expect_error(get_mean.c(m1,c(NA, NA)))

  # 2 subproblems
  m2 <- exactMatch(pr ~ pt, data=nuclearplants)
  # first is 19/7, second is 1.

  expect_true(is.null(get_mean.c(m2, NULL)))
  expect_true(identical(get_mean.c(m2, NA), c(NA, NA)))
  expect_true(identical(get_mean.c(m2, 3), c(NA, NA)))
  expect_true(identical(get_mean.c(m2, 2), c(2, NA)))
  expect_true(identical(get_mean.c(m2, 1), c(1, 1)))

  expect_true(identical(get_mean.c(m2, c(19/7, 1)), c(19/7, 1)))
  expect_true(identical(get_mean.c(m2, c(2, NA)), c(2, NA)))
  expect_true(identical(get_mean.c(m2, c(NA, NA)), c(NA, NA)))

  # 4 subproblems, but one is empty.
  m3 <- exactMatch(pr ~ pt + ne, data=nuclearplants)
  expect_true(length(levels(m3@groups)) == 4)
  expect_true(length(subdim(m3)) == 3)
  # Are 2.6, 1, 3 c/t

  expect_true(is.null(get_mean.c(m3, NULL)))
  expect_true(identical(get_mean.c(m3, 3), c(NA, NA, 3)))
  expect_true(identical(get_mean.c(m3, 2), c(2, NA, 2)))
  expect_true(identical(get_mean.c(m3, NA), c(NA, NA, NA)))

  expect_true(identical(get_mean.c(m3, c(3,3,3)), c(NA, NA, 3)))
  expect_true(identical(get_mean.c(m3, c(2.6,1,3)), c(2.6,1, 3)))
  expect_true(identical(get_mean.c(m3, c(3,3,NA)), c(NA, NA, NA)))
  expect_true(identical(get_mean.c(m3, c(NA,NA,NA)), c(NA, NA, NA)))

  expect_error(get_mean.c(m3, c(1,2)))
})

test_that("Create student level caliper matrix from school level model", {
  set.seed(20141217)

  nsch <- 100
  schoolZ <- rbinom(n = nsch, size = 20, prob = 0.2)
  schoolTotal <- schoolZ + rbinom(n = nsch, size = 100, prob = 0.75)
  schoolX <- cbind(rnorm(n = nsch, mean = schoolZ, sd = 10),
                   rnorm(n = nsch, mean = schoolTotal, sd = 20))
  schoolDf <- data.frame(z = schoolZ, total = schoolTotal, x = schoolX)
  rownames(schoolDf) <- paste0("sch-", 1:nsch)

  schoolMod <- glm(cbind(z, total) ~ ., data = schoolDf, family = binomial)

  nstud <- 1300
  studentz <- numeric(nstud)
  studentz[sample(300, size = nstud, replace = T)] <- 1
  nstudz <- sum(studentz)
  studentIds <- sample(rownames(schoolDf), size = nstud, replace = T)

  x <- schoolToStudentCaliper(schoolMod,  paste0("student-", 1:nstud), studentz, studentIds)

  expect_equal(dim(x), c(nstudz, nstud - nstudz))

  expect_true(all(grepl(x@rownames, patt = "student-")))
  expect_true(all(grepl(x@colnames, patt = "student-")))

  ### Handling NA's in the studentIds

  studentIds[3] <- NA

  x2 <- schoolToStudentCaliper(schoolMod,  paste0("student-", 1:nstud), studentz, studentIds)

  expect_equal(dim(x), dim(x2))
  expect_equal(tail(rownames(x2), 1), "student-3") # Since student 3 had the NA result, it should now be padded on the bottom.

  # Check that student 3 has no calipered matches,
  # but that other students aren't affected.
  m2 <- as.matrix(x2)
  expect_true(all(m2["student-3",] == 0))
  expect_false(all(m2["student-2",] == 0))

  # Check handling multiple NA-school students
  studentIds[4:6] <- NA

  x3 <- schoolToStudentCaliper(schoolMod,  paste0("student-", 1:nstud), studentz, studentIds)

  expect_equal(dim(x), dim(x3))
  lastnames <- tail(rownames(x3), 5)
  expect_equal(lastnames[1], "student-300") # last non-NA student
  expect_true(all(lastnames[-1] == paste0("student-", 3:6)))

  m3 <- as.matrix(x3)
  expect_true(all(m3["student-3",] == 0))
  expect_true(all(m3["student-4",] == 0))
  expect_true(all(m3["student-5",] == 0))
  expect_true(all(m3["student-6",] == 0))
  expect_false(all(m3["student-2",] == 0))

})


test_that("addRows", {
  schoolLevelPS <- runif(10, 0, 1)
  studentZ <- rep(0:1, times = c(8, 12))
  names(schoolLevelPS) <- letters[1:10]
  studentSchIds <- sample(letters[1:10], 20, TRUE)
  studentLevelPS <- schoolLevelPS[as.character(studentSchIds)]
  studentNames <- LETTERS[1:20]
  calWidth <- .2
  names(studentLevelPS) <- studentNames

  NAstudents <- is.na(studentLevelPS)
  studentLevelPS <- studentLevelPS[!NAstudents]
  studentZ <- studentZ[!NAstudents]

  c <- optmatch:::scoreCaliper(studentLevelPS, studentZ, calWidth)
  c2 <- addRows(c, names(NAstudents)[NAstudents])

  expect_identical(c, c2)

  studentLevelPS[1] <- NA

  NAstudents <- is.na(studentLevelPS)
  studentLevelPS <- studentLevelPS[!NAstudents]
  studentZ <- studentZ[!NAstudents]

  c <- optmatch:::scoreCaliper(studentLevelPS, studentZ, calWidth)
  c2 <- addRows(c, names(NAstudents)[NAstudents])

  expect_true(nrow(c) + 1 == nrow(c2))
  expect_true(ncol(c) == ncol(c2))
  expect_identical(as.matrix(c2)[-nrow(c2), ], as.matrix(c))
  expect_true(all(as.matrix(c2)[nrow(c2), ] == 0))

  studentLevelPS[1:2] <- NA

  NAstudents <- is.na(studentLevelPS)
  studentLevelPS <- studentLevelPS[!NAstudents]
  studentZ <- studentZ[!NAstudents]

  c <- optmatch:::scoreCaliper(studentLevelPS, studentZ, calWidth)
  c2 <- addRows(c, names(NAstudents)[NAstudents])

  expect_true(nrow(c) + 2 == nrow(c2))
  expect_true(ncol(c) == ncol(c2))
  expect_identical(as.matrix(c2)[-((nrow(c2)-1):nrow(c2)), ], as.matrix(c))
  expect_true(all(as.matrix(c2)[(nrow(c2)-1):nrow(c2), ] == 0))
})
