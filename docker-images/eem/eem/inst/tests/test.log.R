################################################################################
# Tests for logging system
################################################################################

library(testthat)

context("Logging")

test_that("Should be able to log to global LOGS object", {
  # other tests might be modifying the LOGS object, so we don't know the size in advance
  ldim <- dim(LOGS)
  expect_equal(ldim[2], 3) # name, timestamp, message

  # log should auto concatenate all arguments to form a single message (like warning does)
  logger("Logging", "test" ,"message", name = "test")

  expect_equal(dim(LOGS)[1], ldim[1] + 1)

  expect_equal(sum(LOGS$name == "test"), 1)
})
