################################################################################
# Tests for input.R
################################################################################

library(testthat)
library(eem)

context("Jobs")

test_that("Connects to job db", {

  expect_equal(options("eem_environment")[[1]], "test")
  aJob <- getJob("akdeka-kdfj-jakf-1")
  expect_is(aJob, "Job")
  expect_equal(aJob@State, "nm")

})

test_that("Sets job status", {
  guid <- "akdeka-kdfj-jakf-1"
  jobStatus(guid, "CREATED")

  expect_equal(jobStatus(guid), "CREATED")

  jobStatus(guid, "RUNNING")

  expect_equal(jobStatus(guid), "RUNNING")

  expect_error(jobStatus(guid, "FOOOO"), "status")
  expect_error(jobStatus(1191919), "job")

  # method for job objects
  job <- getJob(guid)
  jobStatus(job, "CREATED")
  expect_equal(jobStatus(job), "CREATED")
  jobStatus(job, "RUNNING")
  expect_equal(jobStatus(job), "RUNNING")

})

test_that("Puts tables, SVG in db", {
  # from the data creation script
  guid <- "akdeka-kdfj-jakf-1"

  mockJob <- list(JobGUID = guid, OutcomeMeasures = c("ELA1SCR", "ELA1PRF"))

  aTable <- data.frame(c(1,2,3,4), factor(c('fee', 'figh', 'fo', 'fum')), c("Some text", "With a single ' squote", 'With a " double dquote', 'Explicit escape \''))

  # rownames not included in YAML conversion: rownames(aTable) <- c("Multiple Words", "Add a quote 'hi'", "XXYY", "ZZWW")
  colnames(aTable) <- c("A numerical column", "a factor column", "text column") # colnames are included

  mockMatch <- list("balanceTable" = aTable,
                    "balancePlot" = plotSVG(function() { plot(rnorm(100), rnorm(100)) }))

  mockOutcomes <- list("ELA1SCR" = list(table = aTable, plot = plotSVG(function() { plot(rnorm(100), rnorm(100)) })),
                       "ELA1PRF" = list(table = aTable, plot = plotSVG(function() { plot(rnorm(100), rnorm(100)) })))

  ## res <- jobResults(mockJob, mockMatch, mockOutcomes)

  ## # this should populate the database with one line
  ## fromDb <- jobGetSql("SELECT * FROM JobResults")
  ## expect_true(dim(fromDb)[1] > 0)


})

test_that("Safety for text data to be put in the db", {
  expect_equal(jobEscape("This is a 'quote'"), "This is a ''quote''")

})

test_that("Multiple subgroups are turned into a vector", {
  # job 2 asks for GENDER and RACETH
  guid <- "akdeka-kdfj-jakf-2"

  job <- getJob(guid)

  expect_equal(as.vector(job@SubgroupAnalyses), c("gender", "raceth"))

  # job 1 has no subgroups

  guid <- "akdeka-kdfj-jakf-1"

  job <- getJob(guid)

  expect_equal(as.vector(job@SubgroupAnalyses), character(0))

})

test_that("fast fail when there are fewer than 9 units included in the spec", {

  # job 4 has 9 valid treated units
  guid <- "akdeka-kdfj-jakf-4"

  expect_error(getJob(guid), "at least 10 students")

})

test_that("translates front end outcome, years into table", {
  InterventionStartDate <- "2011-09-01"
  OutcomeMeasures <- "glmath_scr,readng_scr,status_inferred"
  OutcomeYears <- "2011-2012,2012-2013"

  # just to double check assumptions
  expect_equal(interventionYear(InterventionStartDate), 2012)

  res <- outcomesTable("nm", InterventionStartDate, OutcomeMeasures, OutcomeYears)

  # total table: 3 outcomes * 2 years
  expect_equal(dim(res), c(6, 4))
  expect_true(all(colnames(res) %in% c("front", "main", "prof", "year")))
  expect_true(all(rownames(res) %in% c("readng_scr_p0", "readng_scr_p1",
                                       "glmath_scr_p0", "glmath_scr_p1",
                                       "status_inferred_p0", "status_inferred_p1")))

  expect_true(sum(is.na(res$prof)) == 2)
  expect_true(sum(is.na(res$main)) == 0)

  expect_true(res[res$front == "readng_scr" & res$year == 2012, "main"] == "readng_scr_p0")
  expect_true(res[res$front == "readng_scr" & res$year == 2013, "main"] == "readng_scr_p1")


})

test_that("Gives useful error for bad intervention dates", {

  # right now, test dates should always be in 2009-2010 or later.
  # in the future this might depend on states involved.j

  expect_error(checkValidInterventionYear("TN", 2009), "2010-2011")
  expect_true(checkValidInterventionYear("TN", 2011))

  # test job 7 in the db should give this type of error
  expect_error(getJob("akdeka-kdfj-jakf-7"), "2010-2011")
})

test_that("Excludes outcome-year combinations that do not appear in the db (#1290)", {

  InterventionStartDate <- "2011-09-01"
  OutcomeMeasures <- "readng_scr,status_inferred"
  OutcomeYears <- "2011-2012,2012-2013,2013-2014"

  # just to double check assumptions
  expect_equal(interventionYear(InterventionStartDate), 2012)

  res <- outcomesTable("tn", InterventionStartDate, OutcomeMeasures, OutcomeYears)

  # NB: I'm cutting out status_inferred_p1 until is appears in the db for real.
  # after the metadata is pulled when this column is added, this test will fail.
  # change 4 to 5, provided status_inferred_p2 does not exist.
  expect_equal(dim(res)[1], 6) # there is no status_inferred_p2 in the db.

})


test_that("Gradelevel constrains grades loaded", {
  # job akdeka-kdfj-jakf-1 should has three grades listed in the job spec, but have ids for 2 grades.
  # we should only load two grades
  j <- getJob("akdeka-kdfj-jakf-1")
  expect_equal(j@ObservedGrades, c(7, 8))

  # uploaded grades 8, 9, and 11, requested 8 and  9
  j <- getJob("akdeka-kdfj-jakf-2")
  expect_equal(j@ObservedGrades, c(8, 9))
})
