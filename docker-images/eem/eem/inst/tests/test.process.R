################################################################################
# Tests for going from a problem description to the finished goods
################################################################################

library(testthat)
library(eem)

context("Processing sub functions")

test_that("Do not require intervention students", {

  ids <- dbfetch("SELECT study_id, gradelevel FROM nm2012_drv WHERE gradelevel = 5 LIMIT 5")


  job <- as(Class = "ObservationalJob",
            jobBasics("fake", "nm", "2011-09-01", character(0), "math", c("2011-2012", "2012-2013")))
  job@ObservedGrades <- 5

  schoolIDs <- paste("school-id-", 1:5, sep = "")

  job@Treated <- character(0)

  # this does not require treated units
  x <- getCovariates(job, schoolIDs)
})

test_that("Loading and using meta data", {

  job <- as(Class = "ObservationalJob",
            jobBasics("fake", "nm", "2011-09-01", character(0), "math", c("2011-2012", "2012-2013")))
  job@ObservedGrades <- 5
  job@Treated <- dbfetch("SELECT study_id, gradelevel FROM nm2012_drv WHERE gradelevel = 5 LIMIT 100")$study_id

  schoolIDs <- paste("school-id-", 1:5, sep = "")

  X1 <- getCovariates(job, schoolIDs)

  # all character columns should be turned into factors or dates
  expect_true(all(sapply(X1, class) != "character"))
  expect_true(sum(sapply(X1, class) == "factor") > 1)

  # lets inspect the the race column in a little more depth
  # the first implementation of the factorizer make a bunch of NAs.
  expect_true(any(!is.na(X1$raceth)))
  expect_true(length(levels(X1$raceth)) > 2) # there are 7 levels, but not all appear in this test data

#  Test Meta data doesn't include school level vars yet
#   # Some variables should be flagged as school variables
#   meta <- getMetaData()
#   X1meta <- meta[names(meta) %in% unique(substr(colnames(X1), 1, nchar(colnames(X1)) - 5))]
#
#   expect_true(sum(sapply(X1meta, function(x) {
#     x["Table"] == "School"
#   })) > 0)

  # special columns
  expect_true(!inherits(X1$gradelevel, "factor"))
})


test_that("Loading appropriate outcome variables", {

  job <- as(Class = "ObservationalJob",
            jobBasics("fake", "tn", "2011-09-01", character(0), "readng_scr,glmath_scr", c("2011-2012", "2012-2013")))
  job@ObservedGrades <- 5
  job@Treated <- dbfetch("SELECT study_id, gradelevel FROM tn2012_drv WHERE gradelevel = 5 LIMIT 100")$study_id

  # simulates a match in which some units are discarded
  match <- as.factor(c(rep(1:4, 100)))
  match[c(30, 40, 100, 300)] <- NA

  # there are about 1000 units with IDs of abcde-X format
  names(match) <- paste("abcde-", 1:400, sep = "")

  # need to make up some fake data
  covs <- data.frame(z = rep(c(T,F), each = 200),
                     raceth = rnorm(400))
  
  design <- eem:::makeDesign(job, covs, covs$z, block = match)
  res <- getOutcomes(job, design)

  expect_is(res, "data.frame")
  expect_equal(dim(res), c(396, 8))
  expect_true(all(colnames(res) %in% c("readng_scr_p0", "readng_pr2_p0", "glmath_scr_p0", "glmath_pr2_p0",
                                       "readng_scr_p1", "readng_pr2_p1", "glmath_scr_p1", "glmath_pr2_p1")))

  # single outcomes should be fetched as a 1 column matrix, not a vector

  job2 <- as(Class = "ObservationalJob",
             jobBasics("fake", "tn", "2011-09-01", character(0), "status_inferred", c("2011-2012")))
  job2@ObservedGrades <- 5
  job2@Treated <- dbfetch("SELECT study_id, gradelevel FROM tn2012_drv WHERE gradelevel = 5 LIMIT 100")$study_id

  design2 <- eem:::makeDesign(job2, covs, covs$z, block = match)

  res.single <- getOutcomes(job2, design2)

  expect_equal(dim(res.single), c(396, 1))
  expect_equal(colnames(res.single), "status_inferred_p0")

  # special casing for promotion_p0 variable

  job3 <- as(Class = "ObservationalJob",
             jobBasics("fake", "tn", "2011-09-01", character(0), "status_inferred", c("2011-2012,2012-2013")))
  job3@ObservedGrades <- 5
  job3@Treated <- dbfetch("SELECT study_id, gradelevel FROM tn2012_drv WHERE gradelevel = 5 LIMIT 100")$study_id

  design3 <- eem:::makeDesign(job3, covs, covs$z, block = match)

  res.sip1 <- getOutcomes(job3, design3)
  # expected failure right now
  expect_true("status_inferred_p1" %in% colnames(res.sip1))
})

test_that("Covariates are properly NA imputed and augmented", {

  job <- as(Class = "ObservationalJob",
            jobBasics("fake", "tn", "2011-09-01", character(0), "reading,math", c("2011-2012", "2012-2013")))
  job@ObservedGrades <- 5
  job@Treated <- dbfetch("SELECT study_id, gradelevel FROM tn2012_drv WHERE gradelevel = 5 LIMIT 100")$study_id

  schoolIDs <- paste("school-id-", 1:5, sep = "")
  covs <- getCovariates(job, schoolIDs)

  expect_false(any(is.na(covs)))
  expect_true(any(grep(x = colnames(covs), pattern = "NA_")))

  # this is to catch a weird bug where the column names were "NA_c(T,F,F,T...)"
  # as if the name was paste("NA_", is.na(some_column))

  expect_true(all(grep(x = colnames(covs), pattern = "NA_", value = T) %in% paste("NA_", colnames(covs), sep = "")))


  # any character column loaded with "" should be treated as NA
  covs$glmath_scr_m1 <- ''
  covs$age[1:10] <- '' # should make age into a character vector
  res <- processColumnsFromMeta(covs, getMetaData())

  expect_true(is.null(res$glmath_scr_m1))
  expect_true(all(!is.na(res)))
                               

})

test_that("Test balance based on match and covs", {

  # borrowed everything up balance testing from big processing test above
  set.seed(20121108)

  ids <- dbfetch("SELECT study_id, gradelevel FROM nm2012_drv WHERE gradelevel = 5 AND dstschid_state_enroll_p0 IN ('school-id-1', 'school-id-2', 'school-id-3') LIMIT 100")

  job <- as(Class = "ObservationalJob",
            jobBasics("fake", "nm", "2011-09-01", character(0), "reading,math", c("2011-2012", "2012-2013")))
  job@ObservedGrades <- 5
  job@Treated <- ids$study_id

  schoolIDs <- paste("school-id-", 1:5, sep = "")

  covs  <- getCovariates(job, schoolIDs <- paste("school-id-", 1:5, sep = ""))
  match <- fullmatch(match_on(z ~ age + gender, data = covs), data = covs)

  # after we match, we should be able to get a statement of balance
  res <- testBalance(z ~ age + gender, match, covs)

  expect_is(res, "xbal")
  expect_is(res$overall, "matrix") # where the overall p,values are stored

})


test_that("Loading school variables", {

  set.seed(20121108)
  ids <- rbind(
      dbfetch("SELECT study_id, gradelevel FROM nm2012_drv WHERE dstschid_state_enroll_p0 IS NOT NULL AND gradelevel = 5 LIMIT 100"),
      data.frame(study_id = c("bad1", "bad2", "bad3"), gradelevel = c(9, 9, 9)))

  job <- as(Class = "ObservationalJob",
            jobBasics("fake", "nm", "2011-09-01", character(0), "reading,math", c("2011-2012", "2012-2013")))
  job@ObservedGrades <- 5
  job@Treated <- ids$study_id

  schools <- eem:::getSchoolTable(job)

  meta <- getMetaData()

  schoolMeta <- meta[names(meta) %in% unique(substr(colnames(schools), 1, nchar(colnames(schools)) - 5))]

  # meta data for schools not in table yet
  # expect_true(sum(sapply(schoolMeta, function(x) {
  #   x["Table"] == "School"
  # })) > 0)

  expect_equal(length(c(grep(x = rownames(schools), pattern = "school-id"),
                        grep(x = rownames(schools), pattern = "schoolbad" ))),
               dim(schools)[1])
  expect_true(!is.null(schools[, "z"]))

  expect_equal(sum(schools[, "z"]), length(job@Treated) - 3) # expect 3 bad ids

  ## when school ids are missing, the table should getSchoolTable should note this
  ids.miss <- dbfetch("SELECT study_id, gradelevel FROM nm2012_drv WHERE dstschid_state_enroll_p0 IS NULL limit 50")$study_id
  ids.exst <- dbfetch("SELECT study_id, gradelevel FROM nm2012_drv WHERE dstschid_state_enroll_p0 IS NOT NULL limit 50")$study_id

  job.w.miss <- job
  job.w.miss@Treated <- c(ids.miss, ids.exst)

  schools.w.miss <- eem:::getSchoolTable(job.w.miss)

  expect_true(attr(schools.w.miss, "missing_school_ids"))

})

test_that("getting fixed effect (e.g. SCHOOL_ID)", {

  set.seed(20121108)
  ids <- dbfetch("SELECT study_id, gradelevel FROM nm2012_drv where gradelevel in (9, 10) LIMIT 100")

  job <- as(Class = "ObservationalJob",
            jobBasics("fake", "nm", "2011-09-01", character(0), "reading,math", c("2011-2012", "2012-2013")))
  job@ObservedGrades <- c(9,10) 
  job@Treated <- ids$study_id

  schoolIDs <- paste("school-id-", 1:5, sep = "")
  res <- getCovariates(job, schoolIDs)

  expect_true(!is.null(res[, "dstschid_state_enroll"]))
  expect_true(!is.null(res[, "districtid_state_enroll"]))
})


test_that("getCovs should include the pre-tests and smoothed tests", {

  # borrowed everything up balance testing from big processing test above
  set.seed(20121108)
  ids <- dbfetch("SELECT study_id, gradelevel FROM nm2012_drv where gradelevel in (9, 10) LIMIT 100")

  job <- as(Class = "ObservationalJob",
            jobBasics("fake", "nm", "2011-09-01", character(0), "reading,math", c("2011-2012", "2012-2013")))
  job@ObservedGrades <- c(9,10) 
  job@Treated <- ids$study_id

  covs <- getCovariates(job, paste("school-id-", 1:5, sep = ""))

  expect_true(length(covs$glmath_scr_m1) > 0)

})

test_that("getting proper intervention year", {
  dmay <- "2012-05-10"
  doct <- "2012-10-19"
  dsep <- "2012-09-01"

  expect_equal(interventionYear(dmay), 2012)
  expect_equal(interventionYear(doct), 2013)
  expect_equal(interventionYear(dsep), 2013)
})


test_that("processing raw data from db into meta-data enhanced data", {

    mym <- list(foo = list(is_categorical_variable = FALSE),
                bar = list(is_categorical_variable = FALSE))

    df <- data.frame(foo = rnorm(10), bar = rnorm(10))

    res.df <- processColumnsFromMeta(df, mym)
    expect_identical(res.df, df)

    df.missingbar <- df
    df.missingbar$bar <- NA

    res.df.missingbar <- processColumnsFromMeta(df.missingbar, mym)
    expect_equal(dim(res.df.missingbar), c(10,1))

    # now on to categorical variables
    mym$baz = list(is_categorical_variable = TRUE,
                   categories = data.frame(variable_name = "baz",
                                           category_value = c(1,2,9),
                                           category_label = c("Zippy", "Zappy", "Other")))
    df$baz <- c(9, rep(c(1,2), 4), 9)

    res.df.cat <- processColumnsFromMeta(df, mym)
    expect_equal(dim(res.df.cat), c(10, 3))
    expect_is(res.df.cat$baz, "factor")
    expect_equal(nlevels(res.df.cat$baz), 3)
    
    # now knock down the input to only have 1 value in the baz column
    df$baz <- 1
    res.const <- processColumnsFromMeta(df, mym)
    expect_equal(dim(res.const), c(10, 3))
    expect_is(res.const$baz, "factor")
    expect_equal(nlevels(res.const$baz), 3)

})

test_that("Get covs should always include all the treated units (in the grade level)", {

  set.seed(20121108)
  schoolIDs <- paste("school-id-", 1:5, sep = "")
  allids <- dbfetch(paste0("SELECT study_id, gradelevel FROM nm2012_drv WHERE gradelevel = 5 AND dstschid_state_enroll_p0 IN (",
                        paste0("'", schoolIDs, "'", collapse = ","),
                        ")"))$study_id
  ids <- sample(allids, floor(length(allids) / 2), replace = F)

  job <- as(Class = "ObservationalJob",
            jobBasics("fake", "nm", "2011-09-01", character(0), "reading,math", c("2011-2012", "2012-2013")))
  job@ObservedGrades <- 5
  job@Treated <- ids

  covs <- getCovariates(job, schoolIDs)

  expect_equal(sum(covs$z), length(ids))

  ids6 <- dbfetch(paste0("SELECT study_id, gradelevel FROM nm2012_drv WHERE gradelevel = 6 AND dstschid_state_enroll_p0 IN (",
                        paste0("'", schoolIDs, "'", collapse = ","),
                        ") LIMIT 100"))$study_id

  job2 <- job
  job2@Treated <- c(ids, ids6)

  covs6 <- getCovariates(job2, schoolIDs)
  expect_equal(sum(covs6$z), length(ids)) # only the grade 5 ids

  # fewer schools
  covs.fewer <- getCovariates(job2, schoolIDs[-1])
  expect_true(dim(covs.fewer)[1] < dim(covs)[1])
  expect_equal(sum(covs.fewer$z), sum(covs$z)) # this is the check that makes sure we fetch all treated units

})


test_that("makeMatchTable", {
  
  set.seed(20121108)
  ids <- dbfetch("SELECT study_id, gradelevel FROM nm2012_drv where gradelevel in (9, 10) LIMIT 100")

  job <- as(Class = "ObservationalJob",
            jobBasics("fake", "nm", "2011-09-01", character(0), "reading,math", c("2011-2012", "2012-2013")))
  job@ObservedGrades <- c(9,10) 
  job@Treated <- ids$study_id

  match <- as.factor(c("A"=1.1, "B"=1.2, "C"=1.1, "D"=1.2))
  attr(match, "contrast.group") <- c(TRUE, TRUE, FALSE, FALSE)
  class(match) <- c("optmatch", "factor")
  d <- data.frame(z = c(1,1, 0,0), raceth = rnorm(4), gender = rnorm(4), row.names = LETTERS[1:4])
  mps <- fitted(glm(z ~ raceth, data = d, family = binomial))
  ips <- fitted(glm(z ~ gender, data = d, family = binomial))

  design <- makeObservationalDesign(
      job = job,
      covariates = d,
      treatment = d$z,
      block = match,
      mainProp = mps,
      incProp = ips,
      schDensity = 0,
      schModel = glm(mps ~ ips))
      
  m <- makeMatchTable(job, design)
  expect_true(all.equal(dim(m), c(4,6)))
  expect_true(all(m$JobGUID == job@ID))
  expect_true(all(m$StudyId== LETTERS[1:4]))
  expect_true(all(m$MatchedSet == match))
  expect_true(all(m$TreatmentStatus == d$z))
  expect_true(all(abs(m[,c("MainPropensityScore", "InclusivePropensityScore")]) <= 1))

  ## reorder some stuff
  d <- d[c(2,3,4,1),]
  mps <- fitted(glm(z ~ raceth, data = d, family = binomial))
  ips <- fitted(glm(z ~ gender, data = d, family = binomial))

  design2 <- makeObservationalDesign(
      job = job,
      covariates = d,
      treatment = d$z,
      block = match,
      mainProp = mps,
      incProp = ips,
      schDensity = 0,
      schModel = glm(mps ~ ips))

  m2 <- makeMatchTable(job, design)
  expect_true(all.equal(m, m2))

  # real data

  data(nuclearplants)
  nuclearplants$z <- nuclearplants$pr
  nuclearplants$gender <- nuclearplants$cost
  nuclearplants$raceth <- nuclearplants$t1

  f <- fullmatch(z ~ cost, data=nuclearplants)
  mps <- fitted(glm(z ~ cost, data = nuclearplants, family = binomial))
  ips <- fitted(glm(z ~ cost + t1, data = nuclearplants, family = binomial))

  designN <- makeObservationalDesign(
      job = job,
      covariates = nuclearplants,
      treatment = nuclearplants$z,
      block = f,
      mainProp = mps,
      incProp = ips,
      schDensity = 0,
      schModel = glm(mps ~ ips))

  nukem <- makeMatchTable(job, designN)
  expect_true(all.equal(dim(nukem), c(nrow(nuclearplants),6)))

  expect_true(!all(is.na(nukem) || is.null(nukem)))



})

test_that("Fetching the appropriate tables pre/post 10/01 interventions", {

  jpre  <- getJob("akdeka-kdfj-jakf-1")
  jpost <- getJob("akdeka-kdfj-jakf-2")

  expect_true(jpre@EarlyIntervention)
  expect_false(jpost@EarlyIntervention)

  expect_equal(jpre@StudentTable, "nm2012_drv")
  expect_equal(jpost@StudentTable, "nm2012_drv")

  expect_equal(jpre@SchoolTable, "nm2011_sch")
  expect_equal(jpost@SchoolTable, "nm2012_sch")

  expect_equal(jpre@StudentSchoolLink, "dstschid_state_enroll_p0")
  expect_equal(jpost@StudentSchoolLink, "dstschid_state_enroll_m1")
})

