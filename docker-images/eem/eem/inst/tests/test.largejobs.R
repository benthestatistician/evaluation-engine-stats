################################################################################
# Tests for fixes that involve large jobs
################################################################################

library(testthat)
library(eem)

context("Large Jobs")

test_that("Fall backs to ensure reasonable job size", {

  default <- getOption("optmatch_max_problem_size")
  tryCatch({
  set.seed(20170808)
  n1 <- 20
  n0 <- 100
  testM <- matrix(rexp(n1 * n0), nrow = n1, dimnames = list(treated = paste0("T", 1:n1), control = paste0("C", 1:n0)))
  testDF <- data.frame(z = c(rep(1, n1), rep(0, n0)))
  rownames(testDF) <- c(paste0("T", 1:n1), paste0("C", 1:n0))

  ## first test with a bad covariate, in which case the result should have exactly 999 arcs (one less than max)
  options(optmatch_max_problem_size = 1000)
  res <- largeJobFallback(testM, cbind(testDF, gender = "Male") )

  ## the number of finie entries is 1000 - (20 + 100 + 2)
  ## the result will have 1 less than that
  expect_equal(dim(res), c(n1,n0))
  expect_true(length(res) == 877)

  ## now use a covariate that should allow making a small problem
  res <- largeJobFallback(testM, cbind(testDF, gender = rep(1:6, 20)))

  expect_equal(dim(res), c(20,100))
  expect_true(length(res) < 877)

  }, finally = options(optmatch_max_problem_size = default))
})
