################################################################################
# Tests for input.R
################################################################################

library(testthat)
library(eem)

context("DB")

test_that("Connects to testing db and returns results", {

  expect_equal(options("eem_environment")[[1]], "test")
  # expects the file matching/development.db to exist
  someData <- dbfetch("SELECT 1;")
  expect_equal(dim(someData)[1], 1)

  # data will be located in tables labeled "SS_YYYY" where SS is a two digit state code and YYYY is a year.
  # the test database has a state called nm and data from 2012
  students50 <- dbfetch("SELECT * FROM nm2012_drv LIMIT 5;")
  expect_equal(dim(students50)[1], 5)

})


# these tests don't validate the production server meta data, only that generated for testing purposes
# but at least, in the test environment, we know that we can load and process it appropriately
test_that("Loading meta-data", {
  meta <- getMetaData() 

  # we expect the meta data to be a list of the variables in the database, with appropriate information about each
  expect_is(meta, "list")

  # the meta-data should line up with the columns in a student table
  students <- dbfetch("SELECT * FROM nm2012_drv LIMIT 1;")
  students.columns <- colnames(students)
  
  # just to make sure our testing data is sane, 
  # any columns appearing in the students table should also appear in the meta data.
  # there will also be an alias indicating the school id (whithin district),
  # which comes from one of two columns based on intervention dates
  expect_true(all(students.columns %in% c(names(meta),
                                          "dstschid_state_enroll_p0",
                                          "dstschid_state_enroll_m1",
                                          "districtid_state_enroll_p0",
                                          "districtid_state_enroll_m1")))
  # there may be meta data variables that don't appear in the students table. so be it.

  # we should be able to find at least a few subgroup vars:
  expect_true(sum(sapply(meta, function(k) { ifelse(k$is_subgroup_variable, 1, 0) })) > 0)

  # the tags we add ourselves should be included
  expect_true(sum(sapply(meta, function(k) { ifelse(k$is_main_variable, 1, 0) })) > 0)
  expect_true(sum(sapply(meta, function(k) { ifelse(k$is_inclusive_variable, 1, 0) })) > 0)

  # for each outcome, we have a unique letter that will be used in sensitivity
  # analysis
  expect_equal(meta$glmath_scr_p0$sensitivityLetter, "M")
})


test_that("Loading school meta data", {

  imeta <- getMetaData()
  smeta <- getMetaData( "sch")

  expect_false(identical(imeta, smeta))

  schheader <- dbfetch("SELECT * FROM nm2012_sch")

  expect_true(all(colnames(schheader) %in% names(smeta)))
})

# I was hoping to implement a little helper function that made it easier to get
# meta data that met certain filtering criteria, but I couldn't quite get R's
# delayed computation to do its thing. The "Wah" test passed, but the
# "Bar_Value > 3" test failed on not finding the less than function. 
#
# #' A helper for working with the irregular meta data format.
# #'
# #' @param expr An pression that will be evaluated using the meta data as the environment. Should evaluate to a logical
# #' @param m A meta data tree, by default uses the getMetaData function, but can be substituted in for testing.
# #' @return A vector of names of the meta data items that meet the criterion of exp.
# #' @export
# filterMeta <- function(expr, m = getMetaData()) {
#   spr <- substitute(expr)
#   Filter(m, f = function(x) {
#     eval(spr, as.environment(x))  
#   })
# }
# test_that("Filtering meta data", {
#   m <- list(zip = list(Is_Foo = T, Bar_Value = 7, Wah = F), 
#             zap = list(Is_Foo = T, Bar_Value = 3, Wah = T),
#             zik = list(Bar_Value = 9, Wah = T))
# 
#   expect_equal(filterMeta(mget("Is_Foo", ifnotfound = FALSE), m), c("zip", "zap"))
#   expect_equal(names(filterMeta(Wah, m)), c("zap", "zik"))
#   expect_equal(names(filterMeta(Bar_Value > 3, m)), c("zip", "zik"))
#   
# })
