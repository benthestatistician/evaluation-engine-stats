#!/bin/bash

echo "Starting gearman worker attached to $FRONTEND_NAME"

gearman -w -f match_$EE_ENVIRONMENT -h $FRONTEND_PORT_4730_TCP_ADDR -- ./run.sh
