# Docker Builds

## R

We use the
[Rocker "Versioned"](https://github.com/rocker-org/rocker-versioned/blob/master/r-ver/Dockerfile) Dockerfile.
When a fresh copy is downloaded from the github repository, the MRAN snapshot
date should be changed to that day's date (rather than allowing *future* builds
to pick new MRAN dates). To do this, change the line

    ARG BUILD_DATE

to

    ENV BUILD_DATE YYYY-MM-DD



